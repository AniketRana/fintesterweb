﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

public class ClsCalcChart
{
    clsScenarioHeader ObjSc = new clsScenarioHeader();
    public DataTable profit = new DataTable();
    public DataTable deltatable = new DataTable();
    public DataTable gammatable = new DataTable();
    public DataTable vegatable = new DataTable();
    public DataTable thetatable = new DataTable();
    public DataTable volgatable = new DataTable();
    public DataTable vannatable = new DataTable();


    //private void init_table()
    //{
    //    if (ObjSc.UpDownStrike > 0 && ObjSc.Interval > 0 & ObjSc.MiddleStrike > 0)
    //    {
    //        profit = new DataTable();
    //        deltatable = new DataTable();
    //        gammatable = new DataTable();
    //        vegatable = new DataTable();
    //        thetatable = new DataTable();
    //        volgatable = new DataTable();
    //        vannatable = new DataTable();

    //        int i = 0;
    //        if (Convert.ToDateTime(ObjSc.StartDate) <= Convert.ToDateTime(ObjSc.EndDate))
    //        {
    //            i = Convert.ToInt32(CommonCode.DateDiff("D", dttoday.Value.Date, dtexp.Value.Date));
    //        }
    //        i += 1;
    //        int j = 0;
    //        j = 0;
    //        double start = 0;
    //        double endd = 0;
    //        start = CommonCode.Val(txtmid) - (CommonCode.Val(txtllimit.Text) * CommonCode.Val(interval));
    //        endd = CommonCode.Val(txtmid) + (CommonCode.Val(txtllimit.Text) * CommonCode.Val(interval));
    //        DataRow drow = null;

    //        //######################################################################################

    //        grdprofit.DataSource = null;

    //        //grdprofit.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grdprofit.Columns.Count > 0)
    //        {
    //            grdprofit.Columns.Clear();
    //        }
    //        DataGridViewCellStyle style1 = new DataGridViewCellStyle();
    //        style1.Format = "N2";
    //        DataGridViewTextBoxColumn acol = default(DataGridViewTextBoxColumn);

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grdprofit.Columns.Add(acol);


    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grdprofit.Columns.Add(acol);

    //        //DataGridViewRow grow = new DataGridViewRow;

    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        acol = new DataGridViewTextBoxColumn();
    //                        acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                        //gcol.HeaderText
    //                        acol.Name = acol.HeaderText;
    //                        acol.DataPropertyName = gcol.DataPropertyName;
    //                        acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                        //acol.DefaultCellStyle.Format = RoundGrossMTM
    //                        grdprofit.Columns.Add(acol);

    //                        //acol.DefaultCellStyle.Format = "N2"
    //                    }
    //                }
    //            }
    //        }

    //        var _with1 = profit.Columns;
    //        _with1.Add("SpotValue", typeof(double));
    //        _with1.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        _with1.Add(c);
    //                    }
    //                }
    //            }
    //        }
    //        //FOR MINUS LIMIT(STRI) FROM MID
    //        double inter = 0;
    //        double @int = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = profit.NewRow();
    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            if (Convert.ToInt32(drow["spotvalue"]) < 0)
    //                continue;
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                //& " %"
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            drow[gcol.DataPropertyName] = 0;
    //                        }
    //                    }
    //                }
    //            }
    //            //If drow("spotvalue") > 0 Then
    //            profit.Rows.Add(drow);
    //            //End If
    //        }
    //        //FOR PLUS LIMIT(STRI) FROM MID
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = profit.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                //& " %"
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(Convert.ToBoolean(grow.Cells[gcol.Index].Value)) == true)
    //                        {
    //                            drow[gcol.DataPropertyName] = 0;
    //                        }
    //                    }
    //                }
    //            }
    //            profit.Rows.Add(drow);
    //        }


    //        //######################################################################################

    //        grddelta.DataSource = null;

    //        //grddelta.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grddelta.Columns.Count > 0)
    //        {
    //            grddelta.Columns.Clear();
    //        }

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grddelta.Columns.Add(acol);


    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grddelta.Columns.Add(acol);


    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            acol = new DataGridViewTextBoxColumn();
    //                            acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                            //gcol.HeaderText
    //                            acol.Name = acol.HeaderText;
    //                            acol.DataPropertyName = gcol.DataPropertyName;
    //                            acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                            grddelta.Columns.Add(acol);
    //                        }
    //                    }
    //                }
    //            }
    //        }


    //        ///''''''''''''''' Detla Table Initalise

    //        var _with2 = deltatable.Columns;
    //        _with2.Add("SpotValue", typeof(double));
    //        _with2.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            _with2.Add(c);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        inter = 0;
    //        @int = 0;
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = deltatable.NewRow();
    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            drow["Percent(%)"] = 0;
    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //                //drow("Percent(%)") = Math.Round(((val(drow("SPOTVALUE")) / val(txtmid)) - 1) * 100, 0)
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            deltatable.Rows.Add(drow);
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + val(txtmid)
    //        //FOR PLUS LIMIT(STRI) FROM MID
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = deltatable.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            deltatable.Rows.Add(drow);
    //        }
    //        //######################################################################################

    //        //############# Gamma Table Init
    //        grdgamma.DataSource = null;

    //        //grdgamma.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grdgamma.Columns.Count > 0)
    //        {
    //            grdgamma.Columns.Clear();
    //        }

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grdgamma.Columns.Add(acol);

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grdgamma.Columns.Add(acol);

    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            acol = new DataGridViewTextBoxColumn();
    //                            acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                            //gcol.HeaderText
    //                            acol.Name = acol.HeaderText;
    //                            acol.DataPropertyName = gcol.DataPropertyName;
    //                            acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                            grdgamma.Columns.Add(acol);
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        var _with3 = gammatable.Columns;
    //        _with3.Add("SpotValue", typeof(double));
    //        _with3.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            _with3.Add(c);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = gammatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        inter = 0;
    //        @int = 0;
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = gammatable.NewRow();

    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            drow["Percent(%)"] = 0;
    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            gammatable.Rows.Add(drow);
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = gammatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + val(txtmid)
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = gammatable.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            gammatable.Rows.Add(drow);
    //        }

    //        // Vega Table INit ######################################################################################
    //        grdvega.DataSource = null;

    //        // grdvega.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grdvega.Columns.Count > 0)
    //        {
    //            grdvega.Columns.Clear();
    //        }

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grdvega.Columns.Add(acol);

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grdvega.Columns.Add(acol);

    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            acol = new DataGridViewTextBoxColumn();
    //                            acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                            //gcol.HeaderText
    //                            acol.Name = acol.HeaderText;
    //                            acol.DataPropertyName = gcol.DataPropertyName;
    //                            acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                            grdvega.Columns.Add(acol);
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        var _with4 = vegatable.Columns;
    //        _with4.Add("SpotValue", typeof(double));
    //        _with4.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            _with4.Add(c);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = vegatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        inter = 0;
    //        @int = 0;
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = vegatable.NewRow();

    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            drow["Percent(%)"] = 0;
    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            vegatable.Rows.Add(drow);
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = vegatable.NewRow
    //        //drow("spotvalue") = (val(interval) * j) + val(txtmid)
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = vegatable.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            vegatable.Rows.Add(drow);
    //        }
    //        //######################################################################################
    //        grdtheta.DataSource = null;

    //        //grdtheta.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grdtheta.Columns.Count > 0)
    //        {
    //            grdtheta.Columns.Clear();
    //        }

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grdtheta.Columns.Add(acol);


    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grdtheta.Columns.Add(acol);


    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            acol = new DataGridViewTextBoxColumn();
    //                            acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                            //gcol.HeaderText
    //                            acol.Name = acol.HeaderText;
    //                            acol.DataPropertyName = gcol.DataPropertyName;
    //                            acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                            grdtheta.Columns.Add(acol);
    //                        }
    //                    }
    //                }
    //            }
    //        }


    //        var _with5 = thetatable.Columns;
    //        _with5.Add("SpotValue", typeof(double));
    //        _with5.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            _with5.Add(c);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = thetatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        inter = 0;
    //        @int = 0;
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = thetatable.NewRow();

    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            drow["Percent(%)"] = 0;
    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            thetatable.Rows.Add(drow);
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = thetatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + val(txtmid)
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = thetatable.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            thetatable.Rows.Add(drow);
    //        }

    //        //------------------------------------------------------------------------------------------------------
    //        //For Volga
    //        grdvolga.DataSource = null;

    //        //grdvolga.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grdvolga.Columns.Count > 0)
    //        {
    //            grdvolga.Columns.Clear();
    //        }

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grdvolga.Columns.Add(acol);


    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grdvolga.Columns.Add(acol);


    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            acol = new DataGridViewTextBoxColumn();
    //                            acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                            //gcol.HeaderText
    //                            acol.Name = acol.HeaderText;
    //                            acol.DataPropertyName = gcol.DataPropertyName;
    //                            acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                            grdvolga.Columns.Add(acol);
    //                        }
    //                    }
    //                }
    //            }
    //        }


    //        var _with6 = volgatable.Columns;
    //        _with6.Add("SpotValue", typeof(double));
    //        _with6.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        if ((IsDate(gcol.DataPropertyName.ToString())))
    //                        {
    //                            _with6.Add(c);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = volgatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        inter = 0;
    //        @int = 0;
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = volgatable.NewRow();

    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            drow["Percent(%)"] = 0;
    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            volgatable.Rows.Add(drow);
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = volgatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + val(txtmid)
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = volgatable.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            volgatable.Rows.Add(drow);
    //        }

    //        //------------------------------------------------------------------------------------------------------
    //        //For Vanna
    //        grdvanna.DataSource = null;

    //        //grdVanna.Refresh()
    //        //grdprofit.Rows.Clear()
    //        if (grdvanna.Columns.Count > 0)
    //        {
    //            grdvanna.Columns.Clear();
    //        }

    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "SpotValue";
    //        acol.DataPropertyName = "SpotValue";
    //        acol.Frozen = true;
    //        grdvanna.Columns.Add(acol);


    //        acol = new DataGridViewTextBoxColumn();
    //        acol.HeaderText = "Percent(%)";
    //        acol.DataPropertyName = "Percent(%)";
    //        acol.Frozen = true;
    //        grdvanna.Columns.Add(acol);


    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            acol = new DataGridViewTextBoxColumn();
    //                            acol.HeaderText = DateFormat(gcol.DataPropertyName);
    //                            //gcol.HeaderText
    //                            acol.Name = acol.HeaderText;
    //                            acol.DataPropertyName = gcol.DataPropertyName;
    //                            acol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
    //                            grdvanna.Columns.Add(acol);
    //                        }
    //                    }
    //                }
    //            }
    //        }


    //        var _with7 = vannatable.Columns;
    //        _with7.Add("SpotValue", typeof(double));
    //        _with7.Add("Percent(%)", typeof(double));
    //        foreach (DataGridViewRow grow in grdact.Rows)
    //        {
    //            foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //            {
    //                if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                {
    //                    if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                    {
    //                        string c = gcol.DataPropertyName;
    //                        if ((IsDate(gcol.DataPropertyName)))
    //                        {
    //                            _with7.Add(c);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = Vannatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        inter = 0;
    //        @int = 0;
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = deltatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + start
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text) + 1; j++)
    //        {
    //            drow = vannatable.NewRow();

    //            // drow("spotvalue") = (val(interval) * j) + start
    //            drow["spotvalue"] = txtmid - (CommonCode.Val(interval * (CommonCode.Val(txtllimit.Text) - @int)));
    //            drow["Percent(%)"] = 0;
    //            if (chkint.Checked == true)
    //            {
    //                inter = ((CommonCode.Val(txtinterval.Text) * (CommonCode.Val(txtllimit.Text) - @int)) / 100);
    //                drow["Percent(%)"] = -(inter * 100);
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (-inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            @int += 1;
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            vannatable.Rows.Add(drow);
    //        }
    //        //For j = 1 To val(txtllimit.Text)
    //        //    drow = Vannatable.NewRow
    //        //    drow("spotvalue") = (val(interval) * j) + val(txtmid)
    //        inter = 0;
    //        for (j = 1; j <= CommonCode.Val(txtllimit.Text); j++)
    //        {
    //            drow = vannatable.NewRow();
    //            drow["spotvalue"] = (CommonCode.Val(interval) * j) + CommonCode.Val(txtmid);
    //            drow["Percent(%)"] = 0;

    //            if (chkint.Checked == true)
    //            {
    //                inter = (CommonCode.Val(txtinterval.Text) * CommonCode.Val(j)) / 100;
    //                drow["Percent(%)"] = inter * 100;
    //                drow["spotvalue"] = Math.Round((txtmid * (100 + (inter * 100))) / 100, 0);
    //            }
    //            else
    //            {
    //                drow["Percent(%)"] = Math.Round(((CommonCode.Val(drow["SPOTVALUE"]) / CommonCode.Val(txtmid)) - 1) * 100, 0);
    //            }
    //            foreach (DataGridViewRow grow in grdact.Rows)
    //            {
    //                foreach (DataGridViewCheckBoxColumn gcol in grdact.Columns)
    //                {
    //                    if (!Convert.IsDBNull(grow.Cells[gcol.Index].Value))
    //                    {
    //                        if (Convert.ToBoolean(grow.Cells[gcol.Index].Value) == true)
    //                        {
    //                            if ((IsDate(gcol.DataPropertyName)))
    //                            {
    //                                drow[gcol.DataPropertyName] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            vannatable.Rows.Add(drow);
    //        }

    //    }
    //    else
    //    {
    //        MessageBox.Show("enter Value.");
    //    }

    //}

    public ClsCalcChart()
    {
        //init_table();
    }
}
