﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;
using System;

namespace OptionG
{
    /// <summary>
    /// Summary description for ClsGreeks
    /// </summary>
    public class ClsGreeks
    {
        //static Microsoft.Office.Interop.Excel.Application xApp = new Microsoft.Office.Interop.Excel.Application();
        /// <summary>
        /// BSholes
        /// </summary>
        /// <param name="a">Spot</param>
        /// <param name="b">Strike</param>
        /// <param name="c">RateInt</param>
        /// <param name="d">aa</param>
        /// <param name="e">Vol</param>
        /// <param name="f">time</param>
        /// <param name="g">isCall</param>
        /// <param name="h">isFut</param>
        /// <param name="i">Divs</param>
        /// <param name="j">Result 
        /// 0 = Premium
        /// 1 = Dalta
        /// 2=gamma
        /// 3=vega
        /// 4=theta
        /// 5=Roh
        /// 6=Vol
        /// 7=Vannaa
        /// 8=Volga
        /// </param>
        /// <returns></returns>
        public static double Black_Scholes(double a, double b, double c, double d, double e, double f, bool g, bool h, int i, int j)
        //public static double Black_Scholes( double spot,  double Strike,  double rateInterest,  double aa,  double vol,  double time,  bool isCall,  bool IsFut,  int Divs,  int Result)
        {
            double spot = a;
            double Strike = b;
            double rateInterest = c;
            double aa = d;
            double vol = e;
            double time = f;
            bool isCall = g;
            bool IsFut = h;
            int Divs = i;
            int Result = j;

            double ans;
            ans = 0;
            switch (Result)
            {
                case 0:
                    //Return price
                    //if (IsFut == true)
                    //{
                    //    ans = spot;
                    //}
                    //else
                    //{
                    ans = Premium(spot, Strike, rateInterest, vol, time, isCall);
                    //}

                    break;
                case 1:
                    //If IsFut = True Then
                    //Return 1
                    //Else
                    ans = delta(spot, Strike, rateInterest, vol, time, isCall);
                    //End If
                    break;
                case 2:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = Gamma(spot, Strike, rateInterest, vol, time);
                    //End If
                    break;
                case 3:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = Vega(spot, Strike, rateInterest, vol, time);
                    //End If
                    break;
                case 4:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = Theta(spot, Strike, rateInterest, vol, time, isCall);
                    //End If
                    break;
                case 5:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = Roh(spot, Strike, rateInterest, vol, time, isCall);
                    //End If
                    break;
                case 6:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = volatility(spot, Strike, rateInterest, vol, time, isCall);
                    //ans = CVolFunctions(spot, Strike, rateInterest, vol, time, isCall);

                    //End If
                    break;
                case 7:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = Vanna(spot, Strike, rateInterest, vol, time);
                    //End If
                    break;
                case 8:
                    //If IsFut = True Then
                    //Return 0
                    //Else
                    ans = Volga(spot, Strike, rateInterest, vol, time);
                    //End If
                    break;
                case 12:
                    ans = CVolFunctions(spot, Strike, rateInterest, vol, time, isCall);
                    break;
            }

            return ans;
        }


        /// <summary>
        /// Calculate Premiun as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <param name="isCall"></param>
        /// <returns></returns>
        private static double Premium(double spot, double Strike, double rateInterest, double vol, double time, bool isCall)
        {
            //if (isCall)
            //{
            //    return Call_Premium(spot, Strike, rateInterest, vol, time);
            //}
            //else
            //{
            //    return Put_Premium(spot, Strike, rateInterest, vol, time);
            //}

            double d_D1 = DOne(spot, Strike, rateInterest, vol, time);
            double d_D2 = DTwo(spot, Strike, rateInterest, vol, time);
            return CallPut(spot, d_D1, d_D2, Strike, rateInterest, time, isCall);


        }

        /// <summary>
        /// Calculate Call Premiun as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Call_Premium(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(spot*NORMSDIST(((LN( C4/C5))+((C7+(C6*C6)/2)*(C10)))/(C6*SQRT(C10))))-(C5*EXP(C10*C7*-1)*(NORMSDIST(((LN(C4/C5))+((C7-(C6*C6)/2)*(C10)))/(C6*SQRT(C10)))))
            double result = 0;
            result = (spot * NORMSDIST(((LN(spot / Strike)) + ((rateInterest + (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time)))) - (Strike * Math.Exp(time * rateInterest * -1) * (NORMSDIST(((LN(spot / Strike)) + ((rateInterest - (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time)))));
            return result;
        }

        /// <summary>
        /// Calculate Put Premiun as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Put_Premium(double spot, double Strike, double rateInterest, double vol, double time)
        {
            double result = 0;
            //=(C5*EXP(C10*C7*-1)*(NORMSDIST(-((LN(C4/C5))+((C7-(C6*C6)/2)*(C10)))/(C6*SQRT(C10)))))-(C4*NORMSDIST(-((LN(C4/C5))+((C7+(C6*C6)/2)*(C10)))/(C6*SQRT(C10))))

            result = (Strike * Math.Exp(time * rateInterest * -1) * (NORMSDIST(-((LN(spot / Strike)) + ((rateInterest - (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time))))) - (spot * NORMSDIST(-((LN(spot / Strike)) + ((rateInterest + (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time))));
            return result;
        }

        /// <summary>
        /// Calculate Delta as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <param name="isCall"></param>
        /// <returns></returns>
        private static double delta(double spot, double Strike, double rateInterest, double vol, double time, bool isCall)
        {
            if (isCall)
            {
                return Call_delta(spot, Strike, rateInterest, vol, time);
            }
            else
            {
                return Put_delta(spot, Strike, rateInterest, vol, time);
            }
        }

        /// <summary>
        /// Calculate Call Delta as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Call_delta(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=NORMSDIST(((LN(C4/C5))+((C7+(C6*C6)/2)*(C10)))/(C6*SQRT(C10)))
            double result = 0;
            result = NORMSDIST(((LN(spot / Strike)) + ((rateInterest + (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time)));
            return result;
        }

        /// <summary>
        /// Calculate Put Delta as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Put_delta(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=C14-1
            double delta = Call_delta(spot, Strike, rateInterest, vol, time);
            double result = 0;
            result = delta - 1;
            return result;
        }

        /// <summary>
        /// Calculate Theeta as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <param name="isCall"></param>
        /// <returns></returns>
        private static double Theta(double spot, double Strike, double rateInterest, double vol, double time, bool isCall)
        {
            if (isCall)
            {
                return Call_Theta(spot, Strike, rateInterest, vol, time);
            }
            else
            {
                return Put_Theta(spot, Strike, rateInterest, vol, time);
            }
        }


        /// <summary>
        /// Calculate Call Theeta as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Call_Theta(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(-((C4*C24*C6)/(2*SQRT(C10)))-((C7*C5*EXP(-C7*C10)*NORMSDIST(C23))))/365
            double result = 0;
            double nd1 = NdOne(spot, Strike, rateInterest, vol, time);
            double d2 = DTwo(spot, Strike, rateInterest, vol, time);
            result = (-((spot * nd1 * vol) / (2 * Math.Sqrt(time))) - ((rateInterest * Strike * Math.Exp(-rateInterest * time) * NORMSDIST(d2)))) / 365;
            return result;
        }

        /// <summary>
        /// Calculate Put Theeta as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Put_Theta(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(-((C4*C24*C6)/(2*SQRT(C10)))+((C7*C5*EXP(-C7*C10)*(1-NORMSDIST(C23)))))/365
            double result = 0;
            double nd1 = NdOne(spot, Strike, rateInterest, vol, time);
            double d2 = DTwo(spot, Strike, rateInterest, vol, time);
            result = (-((spot * nd1 * vol) / (2 * Math.Sqrt(time))) + ((rateInterest * Strike * Math.Exp(-rateInterest * time) * (1 - NORMSDIST(d2))))) / 365;
            return result;
        }

        /// <summary>
        /// Calculate Gamma as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Gamma(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=C24/(C4*C6*SQRT(C10))
            double result = 0;
            double nd1 = NdOne(spot, Strike, rateInterest, vol, time);
            result = nd1 / (spot * vol * Math.Sqrt(time));
            return result;
        }

        /// <summary>
        /// Calculate Vega as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Vega(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=C4*SQRT(C10)*(EXP(-((((LN(C4/C5))+((C7+(C6*C6)/2)*(C10)))/(C6*SQRT(C10)))^2)/2)/(SQRT(2*PI())))/100
            double result = 0;
            result = spot * Math.Sqrt(time) * (Math.Exp(-(Math.Pow((((LN(spot / Strike)) + ((rateInterest + (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time))), 2)) / 2) / (Math.Sqrt(2 * Math.PI))) / 100;
            return result;
        }

        /// <summary>
        /// Calculate Roh as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <param name="isCall"></param>
        /// <returns></returns>
        private static double Roh(double spot, double Strike, double rateInterest, double vol, double time, bool isCall)
        {
            if (isCall)
            {
                return Call_Roh(spot, Strike, rateInterest, vol, time);
            }
            else
            {
                return Put_Roh(spot, Strike, rateInterest, vol, time);
            }
        }

        /// <summary>
        /// Calculate Call_Roh as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Call_Roh(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(C5*C10*EXP(-C7*C10)*NORMSDIST(C23))/100
            double result = 0;
            double d2 = DTwo(spot, Strike, rateInterest, vol, time);
            result = (Strike * time * Math.Exp(-rateInterest * time) * NORMSDIST(d2)) / 100;
            return result;
        }

        /// <summary>
        /// Calculate Put_Roh as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Put_Roh(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(-C5*C10*EXP(-C7*C10)*(1-NORMSDIST(C23)))/100
            double result = 0;
            double d2 = DTwo(spot, Strike, rateInterest, vol, time);
            result = (-Strike * time * Math.Exp(-rateInterest * time) * (1 - NORMSDIST(d2))) / 100;
            return result;
        }

        /// <summary>
        /// Calculate Vanna as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Vanna(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(C19/C4)*(1-(C22/(C6*SQRT(C10))))
            double result = 0;
            double Ve = Vega(spot, Strike, rateInterest, vol, time);
            double D1 = DOne(spot, Strike, rateInterest, vol, time);
            result = (Ve / spot) * (1 - (D1 / (vol * Math.Sqrt(time))));
            return result;
        }

        /// <summary>
        /// Calculate Volga as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double Volga(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(C19*(C22*C23)/C6)/100
            double result = 0;
            double d2 = DTwo(spot, Strike, rateInterest, vol, time);
            double d1 = DOne(spot, Strike, rateInterest, vol, time);
            double Ve = Vega(spot, Strike, rateInterest, vol, time);
            result = (Ve * (d1 * d2) / vol) / 100;
            return result;
        }

        /// <summary>
        /// Calculate ND2 as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double NdOne(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(EXP(-((((LN(C4/C5))+((C7+(C6*C6)/2)*(C10)))/(C6*SQRT(C10)))^2)/2)/(SQRT(2*PI())))
            double result = 0;
            result = (Math.Exp(-(Math.Pow((((LN(spot / Strike)) + ((rateInterest + (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time))), 2)) / 2) / (Math.Sqrt(2 * Math.PI)));
            return result;
        }

        /// <summary>
        /// Calculate D2 as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double DTwo(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=((LN(C4/C5))+((C7-(C6*C6)/2)*(C10)))/(C6*SQRT(C10))
            double result = 0;
            result = ((LN(spot / Strike)) + ((rateInterest - (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time));
            return result;
        }

        /// <summary>
        /// Calculate D1 as Describe in Option Excel Sheet
        /// </summary>
        /// <param name="spot"></param>
        /// <param name="Strike"></param>
        /// <param name="rateInterest"></param>
        /// <param name="vol"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        private static double DOne(double spot, double Strike, double rateInterest, double vol, double time)
        {
            //=(((LN(C4/C5))+((C7+(C6*C6)/2)*(C10)))/(C6*SQRT(C10)))
            double result = 0;
            result = (((LN(spot / Strike)) + ((rateInterest + (vol * vol) / 2) * (time))) / (vol * Math.Sqrt(time)));
            return result;
        }

        /// <summary>
        /// Calculate Normal Distribution -- Adopt Function From MS-Excel 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private static double NORMSDIST(double x)
        {
            //Microsoft.Office.Interop.Excel._Application appx = new Microsoft.Office.Interop.Excel._Application();
            //return xApp.WorksheetFunction.NormSDist(x);
            //If isExcel = True Then
            //return Microsoft.Win32. eNormDist(x);
            //Else
            return cNormSDist(x);
            //End If
        }

        /// <summary>
        /// Calculate Normal distribution ---- Adopt From C++ MFC (CTCL)
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private static double cNormSDist(double x)
        {
            const double b1 = 0.31938153;
            const double b2 = -0.356563782;
            const double b3 = 1.781477937;
            const double b4 = -1.821255978;
            const double b5 = 1.330274429;
            const double p = 0.2316419;
            const double c = 0.39894228;

            if ((x >= 0.0))
            {
                double t = 1.0 / (1.0 + p * x);
                return (1.0 - c * Math.Exp(-x * x / 2.0) * t * (t * (t * (t * (t * b5 + b4) + b3) + b2) + b1));

            }
            else
            {
                double t = 1.0 / (1.0 - p * x);
                return (c * Math.Exp(-x * x / 2.0) * t * (t * (t * (t * (t * b5 + b4) + b3) + b2) + b1));
            }
        }

        /// <summary>
        /// Calculate Log 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private static double LN(double x)
        {
            //If isExcel = True Then
            //    Return ExFunc.eLN(x)
            //Else
            return Math.Log(x);
            //End If
        }

        /// <summary>
        /// Calculation of Volatility as Given Option Excel Sheet
        /// </summary>
        /// <param name="s"></param>
        /// <param name="k"></param>
        /// <param name="r"></param>
        /// <param name="price"></param>
        /// <param name="t"></param>
        /// <param name="IsCall"></param>
        /// <returns></returns>
        private static double volatility(double s, double k, double r, double price, double t, bool IsCall)
        {
            double _d_Vol = 0.1;
            double MinVol = 0;
            double MaxVol = _d_Vol;
            double prevCall = 0;
            double prevVol = 0;
            double d_D1, d_D2, d_Call;
            int Iteration = 0;
            while (true)
            {
                //d_D1=DOne(s,k,r,_d_Vol,t);
                //d_D2=DTwo(s,k,r,_d_Vol,t);

                //d_Call=CallPut(s,d_D1,d_D2,k,r,t,IsCall);
                d_Call = Premium(s, k, r, _d_Vol, t, IsCall);
                if (_d_Vol > 3)
                    return 2.6;

                if (d_Call > price)
                {
                    MinVol = _d_Vol - 0.1;
                    MaxVol = _d_Vol;
                    do
                    {
                        if (Iteration > 25)
                            break;
                        prevCall = d_Call;
                        prevVol = _d_Vol;
                        _d_Vol = (MinVol + MaxVol) / 2;

                        //d_D1=DOne(s,k,r,_d_Vol,t);
                        //d_D2=DTwo(s,k,r,_d_Vol,t);
                        //d_Call=CallPut(s,d_D1,d_D2,k,r,t,IsCall);

                        d_Call = Premium(s, k, r, _d_Vol, t, IsCall);

                        if (d_Call > price)
                        {
                            MaxVol = _d_Vol - 0.00000000000001;
                        }
                        else if (d_Call < price)
                        {
                            MinVol = _d_Vol + 0.00000000000001;
                        }
                        if (d_Call == price)
                            break;
                        if (prevVol == _d_Vol && d_Call != price)
                            Iteration++;

                    } while (MinVol <= MaxVol);
                    if (prevVol > 2.6)
                        return 2.6;
                    else if (d_Call == price)
                        return _d_Vol;
                    else if (prevCall < price)
                        return prevVol;
                    else
                        return _d_Vol;
                }
                _d_Vol += 0.1;
            }
        }

        /// <summary>
        /// Calculation of volatility --- This Function adopt From C++ MFC (FinExcel)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="k"></param>
        /// <param name="r"></param>
        /// <param name="price"></param>
        /// <param name="t"></param>
        /// <param name="IsCall"></param>
        /// <returns></returns>
        private static double CVolFunctions(double s, double k, double r, double price, double t, Boolean IsCall)
        {
            //Calculates the Vol
            bool m_b_Vol;
            m_b_Vol = false;
            double m_d_Vol;
            m_d_Vol = 1.0;
            double _d_Vol = m_d_Vol;
            double d_D1, d_D2, d_Call, d_Vega;
            int Iteration = 0;
            double d_sigma = 0;
            k = Math.Abs(k);
            String str;
            //str.Format("%.02f",t);
            //t=atof(str);
            do
            {
                d_D1 = DOne(s, k, r, _d_Vol, t);
                //str.Format("%.04f",d_D1);
                //d_D1=atof(str);
                d_D2 = DTwo(s, k, r, _d_Vol, t);
                //str.Format("%.04f",d_D2);
                //d_D2=atof(str);


                d_Call = CallPut(s, d_D1, d_D2, k, r, t, IsCall);
                //str.Format("%.04f",d_Call);
                //d_Call=atof(str);

                d_Vega = Vega(s, t, d_D1);
                //str.Format("%.04f",d_Vega);
                //d_Vega=atof(str);

                //if(d_Vega==0)
                //{
                //	_d_Vol=_d_Vol+((double)10/100);
                //	//str.Format("%.04f",_d_Vol);
                //	//_d_Vol=atof(str);
                //	Iteration++;
                //	if(Iteration==26)
                //	{
                //		break;
                //	}
                //}
                //else
                //{
                //str.Format("%.06f",d_Call);
                str = Convert.ToString(Math.Round(d_Call, 6));
                if ((price - Convert.ToDouble(str)) != 0)
                {
                    d_sigma = (price - d_Call) / (d_Vega * 100 + 0.0001);
                    if (d_sigma > 0.1)
                    {
                        d_sigma = 0.1;
                    }
                    else if (d_sigma < -0.1)
                    {
                        d_sigma = -0.1;
                    }
                    _d_Vol = _d_Vol + d_sigma;
                    if (_d_Vol < 0.00001)
                    {
                        _d_Vol = 0.00001;
                    }
                }
                else
                {
                    break;
                }
                Iteration++;
                if (Iteration == 26)
                {
                    break;
                }
                //}
            } while (true);

            if (m_b_Vol == false)
            {
                m_d_Vol = _d_Vol;
                m_b_Vol = true;
            }

            return (_d_Vol);
        }

        /// <summary>
        /// Calculate Vega -- This calculation Adopt From C++ MFC (FinExcel)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="t"></param>
        /// <param name="_d1"></param>
        /// <returns></returns>
        private static double Vega(double s, double t, double _d1)
        {
            //Calculates the Vega
            return (s * Math.Sqrt(t) * NORMSDIST(_d1)) / 100;
        }

        /// <summary>
        /// Calculate Premium Of Call Or Put This calculation adopt From C++ MFC (CTCL)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="pD1"></param>
        /// <param name="pD2"></param>
        /// <param name="k"></param>
        /// <param name="r"></param>
        /// <param name="t"></param>
        /// <param name="IsCall"></param>
        /// <returns></returns>
        private static double CallPut(double s, double pD1, double pD2, double k, double r, double t, bool IsCall)
        {
            //return Premium(s, k, r, 0, t, IsCall);
            if (IsCall)
            {
                double nd1 = NORMSDIST(pD1);
                double nd2 = NORMSDIST(pD2);
                double c1 = s * nd1;
                double c2 = k * Math.Exp(r * t * (-1)) * nd2;
                return c1 - c2;//Calculates Call
            }
            else
            {
                double nd1 = NORMSDIST(-pD1);
                double nd2 = NORMSDIST(-pD2);
                double c1 = s * nd1;
                double c2 = k * Math.Exp(r * t * (-1)) * nd2;
                return c2 - c1;//Calculates Put
            }
        }

        /// <summary>
        /// Calculate Profit And loss as on Expiry Date
        /// </summary>
        /// <param name="Spot"></param>
        /// <param name="strike"></param>
        /// <param name="rate"></param>
        /// <param name="Qty"></param>
        /// <param name="CPF"></param>
        /// <returns></returns>
        public static double PNL(double Spot, double strike, double rate, double Qty, string CPF)
        {
            double result;
            double price;
            string sCPF;
            sCPF = CPF.ToUpper();
            switch (sCPF)
            {
                case "C":
                    if ((Spot - strike) > 0)
                    {
                        price = (Spot - strike);
                    }
                    else
                    {
                        price = 0;
                    }
                    result = (price - rate) * Qty;

                    break;
                case "P":
                    if ((strike - Spot) > 0)
                    {
                        price = (strike - Spot);
                    }
                    else
                    {
                        price = 0;
                    }
                    result = (price - rate) * Qty;
                    break;
                case "F":
                    price = Spot;
                    result = (price - rate) * Qty;
                    break;
                default:
                    price = Spot;
                    result = (price - rate) * Qty;
                    break;
            }
            return result;
        }
    }

}