﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

class clsScenarioHeader
{
    public double CMP;
    public double CallVol;
    public double PutVol;
    public double Vol;
    public DateTime StartDate;
    public DateTime EndDate;
    public Boolean intervalType;
    public Double Interval;
    public int UpDownStrike;
    public Double MiddleStrike;

    public clsScenarioHeader(string UserId)
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
        string sql = "Select * from tblScenarioHeader Where Email = '" + UserId + "'";
        SqlDataAdapter hdradp = new SqlDataAdapter(sql, cnnstr);
        DataSet hdrds = new DataSet();
   
        hdradp.Fill(hdrds);


        if (hdrds.Tables[0].Rows.Count >= 1)
        {
            CMP = double.Parse( hdrds.Tables[0].Rows[0]["CMP"].ToString());
            CallVol = double.Parse( hdrds.Tables[0].Rows[0]["CallVol"].ToString());
            PutVol = double.Parse(hdrds.Tables[0].Rows[0]["PutVol"].ToString());
            Vol=double.Parse(hdrds.Tables[0].Rows[0]["Vol"].ToString());
            StartDate = Convert.ToDateTime(hdrds.Tables[0].Rows[0]["StartingDate"].ToString());
            EndDate = Convert.ToDateTime(hdrds.Tables[0].Rows[0]["EndDate"].ToString());
            Interval = double.Parse(hdrds.Tables[0].Rows[0]["Interval"].ToString());
            UpDownStrike = int.Parse(hdrds.Tables[0].Rows[0]["UpDownStrike"].ToString());
            MiddleStrike = Double.Parse(hdrds.Tables[0].Rows[0]["MiddleStrike"].ToString());
            //txtScenarioName.Text = hdrds.Tables[0].Rows[0]["ScenarioName"].ToString();
        }



        

    }

    public clsScenarioHeader()
    {
        // TODO: Complete member initialization
    }
}
