﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

/// <summary>
/// Summary description for ColorSchem
/// </summary>
public class ColorSchem
{
	public ColorSchem()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private long GetR(long Color)
    {
        return Color & 0xff;
    }
    private long GetG(long Color)
    {
        return ((Color & 0xff00) / 0x100) & 0xff;
    }
    private long GetB(long Color)
    {
        return ((Color & 0xff0000) / 0x10000) & 0xff;
    }

    private static int Bind(object parav, object paramin, object paramax)
        {
            int functionReturnValue = 0;
            int v = Convert.ToInt32(parav.ToString());
            int min = Convert.ToInt32(paramin.ToString());
            int max = Convert.ToInt32(paramax.ToString());
            min = 100;
            if (v > max)
            {
                functionReturnValue = max;
            }
            else if (v < min)
            {
                functionReturnValue = min;
            }
            else
            {
                functionReturnValue = v;
            }
            return functionReturnValue;
        }

    public static Color GetColorGradient(int Rn, int Rm, int Gn, int Gm, int Bn, int Bm, double Minval, double maxval, double CurVal)
        {
            Color functionReturnValue = default(Color);

            try
            {

                dynamic Rd = null;
                dynamic Gd = null;
                dynamic Bd = null;
                int Rv = 0;
                int Gv = 0;
                int Bv = 0;



                Rd = Rn - Rm;
                Gd = Gn - Gm;
                Bd = Bn - Bm;

                dynamic Oper = null;
              //  Oper = CurVal / ((maxval - Minval) / 100);
                //if ((maxval - Minval) / 100 < 1)
                //{
                //    //  Oper = CurVal / ((maxval - Minval) / 100);
                //    // int val1 =Convert.ToInt16 ( Convert.ToDouble (CurVal - Minval) * 100)) / maxval))*100);
                //    double mm = (Convert.ToDouble((CurVal - Minval)) * 100 / Convert.ToDouble(maxval)) * 100;
                //    Oper = Convert.ToInt16(mm);
                //}
                //else
                //{


                Oper = Convert.ToInt16(CurVal / ((maxval - Minval) / 100));
                


                    
            //    }

                Rv = ((Rd / 100) * Oper) + Rm;
                Gv = ((Gd / 100) * Oper) + Gm;
                Bv = ((Bd / 100) * Oper) + Bm;

                int red = Bind(Rn + Rv, 0, 255);
                int green = Bind(Gn + Gv, 0, 255);
                int blue = Bind(Bn + Bv, 0, 255);
                functionReturnValue = Color.FromArgb(red, green, blue);
            }
            catch (Exception ex)
            {
                functionReturnValue = Color.White;
            }
            return functionReturnValue;
        }


    }
