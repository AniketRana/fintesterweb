﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for WriteLogFile
/// </summary>
public class WriteLogFile
{
    public static string file = "";

    public static int doLog = 1;

	public WriteLogFile()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void WriteLog(string strFileName, string strMessage)
    {
        try
        {
            FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", Path.GetTempPath(), strFileName), FileMode.Append, FileAccess.Write);
            StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
            objStreamWriter.WriteLine(strMessage);
            objStreamWriter.Close();
            objFilestream.Close();
            
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public static void LogException(Exception ex)
    {
        string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex.Message);
        message += Environment.NewLine;
        message += string.Format("StackTrace: {0}", ex.StackTrace);
        message += Environment.NewLine;
        message += string.Format("Source: {0}", ex.Source);
        message += Environment.NewLine;
        message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("~/ErrorLog/ErrorLog.txt");
        using (StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }

    public static void logWriter(string requestPage,string method,string error)
    {

        if (doLog == 1)
        {
            string pageName = requestPage;
            string filename = "Log_" + file + ".txt";
            string filepath = HttpContext.Current.Server.MapPath("~/logfiles/" + filename);
            if (File.Exists(filepath))
            {
                using (StreamWriter stwriter = new StreamWriter(filepath, true))
                {
                    stwriter.WriteLine(DateTime.Now.ToString() + ",             " + pageName + ",                    " + method + ",                   " + error);

                }
            }
            else
            {
                StreamWriter stwriter = File.CreateText(filepath);
                stwriter.WriteLine("-------------------START-------------" + DateTime.Now);
                stwriter.WriteLine("Page :" + pageName);
                stwriter.WriteLine(error);
                stwriter.WriteLine("-------------------END-------------" + DateTime.Now);
                stwriter.Close();
            }
        }
    }


    public static void createLogFile(string FileName)
    {
        if (doLog == 1)
        {
            file = FileName;
            string filename = "Log_" + FileName + ".txt";
            string filepath = HttpContext.Current.Server.MapPath("~/logfiles/" + filename);
            if (!File.Exists(filepath))
            {
                StreamWriter stwriter = File.CreateText(filepath);
                stwriter.WriteLine("DateTime                     Page/Class Name                     Method Name                                  error");
                stwriter.Close();
            }
        }
    }



}