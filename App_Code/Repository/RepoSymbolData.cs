﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RepoSymbolData
/// </summary>
public class RepoSymbolData
{
    SqlConnection scon;
    SqlCommand scmd;
    SqlDataAdapter sda;
    public DataSet ds;
    public DataTable dt;

	public RepoSymbolData()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable GetDataBySymbol(EntitySymbol obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("sp_MW", scon);
            scmd.Parameters.AddWithValue("@Symbol", obj.Symbol);
            scmd.Parameters.AddWithValue("@Bdate", obj.Bdate);
            scmd.Parameters.AddWithValue("@Edate", obj.Edate);
            scmd.Parameters.AddWithValue("@User", obj.User);
            scmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {
            dt = null;
        }
            return dt;
            
        

    }
    public DataTable getExpiry(EntitySymbol obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select (convert(varchar,EXPIRY_DATE,106)) as [Expiry Date] from OptionCE_" + obj.Symbol + " where BUSINESS_DATE = '" + obj.Bdate + "' group by EXPIRY_DATE Order By EXPIRY_DATE", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }

        }
        catch (Exception ex)
        {

        }
        return dt;


    }

    public string getAllExpiry(EntitySymbol obj,int index)
    {
        string Bdate;
        DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
        if (getExpiry(obj).Rows.Count == 0)
        {                                                                                     
            int skipdays = 0;
            while (getExpiry(obj).Rows.Count == 0)
            {
                skipdays += 1;
                Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
                obj.Bdate = Bdate;
                if (skipdays == 10)
                {
                    return "";
                }
            }
        }
        dt = getExpiry(obj);
        if (dt.Rows.Count > 0)
        {
            return Convert.ToDateTime(DateTime.ParseExact(dt.Rows[index][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
        }
        else
        {
            return "";
        }

    }


    //public string get2ndExpiry(EntitySymbol obj)
    //{                
    //    string Bdate;
    //    DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
    //    if (getExpiry(obj).Rows.Count == 0)
    //    {
    //        int skipdays = 0;
    //        while (getExpiry(obj).Rows.Count == 0)
    //        {
    //            skipdays += 1;
    //            Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
    //            obj.Bdate = Bdate;
    //            if (skipdays == 10)
    //            {
    //                return "";
    //            }
    //        }
    //    }
    //    dt = getExpiry(obj);

    //    if (dt.Rows.Count > 0)
    //    {
    //        return Convert.ToDateTime(DateTime.ParseExact(dt.Rows[1][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
    //    }
    //    else
    //    {
    //        return "";
    //    }

    //}

    //public string get3rdExpiry(EntitySymbol obj)
    //{
    //    string Bdate;
    //    DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
    //    if (getExpiry(obj).Rows.Count == 0)
    //    {
    //        int skipdays = 0;
    //        while (getExpiry(obj).Rows.Count == 0)
    //        {
    //            skipdays += 1;
    //            Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
    //            obj.Bdate = Bdate;
    //            if (skipdays == 10)
    //            {
    //                return "";
    //            }
    //        }
    //    }
    //    dt = getExpiry(obj);

    //    if (dt.Rows.Count > 0)
    //    {
    //        return Convert.ToDateTime(DateTime.ParseExact(dt.Rows[2][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
    //    }
    //    else
    //    {
    //        return "";
    //    }

    //}

    public string getFuturePrice(EntitySymbol obj)
    {
       
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select TOP 1 FUTURE_PRICE FROM OptionCE_" + obj.Symbol + " where BUSINESS_DATE = '" + obj.Bdate + "' AND EXPIRY_DATE = '" + obj.Edate + "'", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        
    }

    public string getBdate(string Symbol)
    {
        string s = "";

            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("SELECT TOP 1 (convert(varchar,BUSINESS_DATE,106)) FROM OptionCE_" + Symbol + " ORDER BY BUSINESS_DATE DESC", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                s = Convert.ToDateTime(DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
            }
            else
            {
                s = "";
            }
            return s;
    }

    public double getChangeFuturePrice(EntitySymbol obj)
    {
        double changeprice = 0.0; 
        try
        {
            string strCOBDate = DateTime.Parse(obj.Bdate).AddDays(-1).ToString();
           
            double futprice;
            futprice = Convert.ToDouble(getFuturePrice(obj));
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select DISTINCT EXPIRY_DATE,FUTURE_PRICE from dbo.OptionCE_" + obj.Symbol + " where SECURITY_SYMBOL = '" + obj.Symbol + "' and BUSINESS_DATE = '" + DateTime.Parse(strCOBDate).ToString("dd-MMM-yyyy") + "' ORDER BY EXPIRY_DATE ASC;", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables[0].Rows.Count > 0)
            {

                if (obj.Edate == Convert.ToDateTime(ds.Tables[0].Rows[0][0]).ToString("dd-MMM-yyyy").ToString())
                {
                    changeprice = Math.Round(futprice - Convert.ToDouble(ds.Tables[0].Rows[0][1].ToString()));
                }
                if (obj.Edate == Convert.ToDateTime(ds.Tables[0].Rows[1][0]).ToString("dd-MMM-yyyy").ToString())
                {
                    changeprice = Math.Round(futprice - Convert.ToDouble(ds.Tables[0].Rows[1][1].ToString()));
                }
                if (obj.Edate == Convert.ToDateTime(ds.Tables[0].Rows[2][0]).ToString("dd-MMM-yyyy").ToString())
                {
                    changeprice = Math.Round(futprice - Convert.ToDouble(ds.Tables[0].Rows[2][1].ToString()));
                }
            }
            else
            {
                changeprice = 0.0;
            }
           
        }
             
        catch (Exception ex)
        {

        }
        return changeprice;
    }



    public string getSymExpiry(EntitySymbol obj, int index)
    {
        string Bdate;
        DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
        if (getExpiry(obj).Rows.Count == 0)
        {
            int skipdays = 0;
            while (getExpiry(obj).Rows.Count == 0)
            {
                skipdays -= 1;
                Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
                obj.Bdate = Bdate;
                
            }
        }
        dt = getExpiry(obj);
        if (dt.Rows.Count > 0)
        {
            return Convert.ToDateTime(DateTime.ParseExact(dt.Rows[index][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
        }
        else
        {
            return "";
        }

    }


    //public string getSym2ndExpiry(EntitySymbol obj)
    //{
    //    string Bdate;
    //    DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
    //    if (getExpiry(obj).Rows.Count == 0)
    //    {
    //        int skipdays = 0;
    //        while (getExpiry(obj).Rows.Count == 0)
    //        {
    //            skipdays -= 1;
    //            Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
    //            obj.Bdate = Bdate;
                
    //        }
    //    }
    //    dt = getExpiry(obj);

    //    if (dt.Rows.Count > 0)
    //    {
    //        return Convert.ToDateTime(DateTime.ParseExact(dt.Rows[1][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
    //    }
    //    else
    //    {
    //        return "";
    //    }

    //}

    //public string getSym3rdExpiry(EntitySymbol obj)
    //{
    //    string Bdate;
    //    DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
    //    if (getExpiry(obj).Rows.Count == 0)
    //    {
    //        int skipdays = 0;
    //        while (getExpiry(obj).Rows.Count == 0)
    //        {
    //            skipdays -= 1;
    //            Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
    //            obj.Bdate = Bdate;
                
    //        }
    //    }
    //    dt = getExpiry(obj);

    //    if (dt.Rows.Count > 0)
    //    {
    //        return Convert.ToDateTime(DateTime.ParseExact(dt.Rows[2][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
    //    }
    //    else
    //    {
    //        return "";
    //    }

    //}


    public DataTable getExp(EntitySymbol obj)
    {
        try
        {
            DataTable dt_exp = new DataTable();
            string Bdate;
            DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
            if (getExpiry(obj).Rows.Count == 0)
            {
                int skipdays = 0;
                while (getExpiry(obj).Rows.Count == 0)
                {
                    skipdays += 1;
                    Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
                    obj.Bdate = Bdate;
                    if (skipdays == 6)
                    {
                        dt = null;
                        break;
                    }
                }
            }
            dt_exp = getExpiry(obj);
            if (dt_exp.Rows.Count > 0)
            {
                dt = dt_exp;
            }
            else
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {

        }

        return dt;
    }


    public double getAtm(DataTable dt, double spot)
    {


        double intLFutPrice = spot;


        try
        {
            dt.DefaultView.Sort = "Strike asc";
            double minvalue = 0;
            double maxvalue = 0;
            double m = 0;
            double m1 = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToDouble(dt.Rows[i][0].ToString()) >= intLFutPrice)
                {
                    minvalue = Convert.ToDouble(dt.Rows[i][0].ToString());
                    m = Convert.ToDouble(dt.Rows[i - 1][0].ToString());
                    break;
                }

            }

            for (int i = dt.Rows.Count - 1; i > 0; i--)
            {
                if (Convert.ToDouble(dt.Rows[i][0].ToString()) <= intLFutPrice)
                {
                    maxvalue = Convert.ToDouble(dt.Rows[i][0].ToString());
                    m1 = Convert.ToDouble(dt.Rows[i + 1][0].ToString());
                    break;
                }

            }
            double a, b;
            a = intLFutPrice - minvalue;
            b = maxvalue - intLFutPrice;
            if (a > b)
            {
                intLFutPrice = minvalue;
            }
            else if (b > a)
            {
                intLFutPrice = maxvalue;
            }
            else
            {
                intLFutPrice = maxvalue;
            }
        }
        catch
        {
        }

        return intLFutPrice;

    }


    public DataTable getSymAllExp(EntitySymbol obj)
    {
        try
        {
            string Bdate;
            DataTable dt_exp = new DataTable();
            DateTime businessDate = DateTime.ParseExact(obj.Bdate, "dd-MMM-yyyy", null);
            if (getExpiry(obj).Rows.Count == 0)
            {
                int skipdays = 0;
                while (getExpiry(obj).Rows.Count == 0)
                {
                    skipdays -= 1;
                    Bdate = Convert.ToDateTime(businessDate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
                    obj.Bdate = Bdate;

                }
            }
            dt_exp = getExpiry(obj);
            if (dt_exp.Rows.Count > 0)
            {
                dt = dt_exp;
            }
            else
            {
                return dt = null;
            }
        }
        catch (Exception ex)
        {

        }
        return dt;
    }


}