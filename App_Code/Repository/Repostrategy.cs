﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for Repostrategy
/// </summary>
public class Repostrategy
{
    SqlConnection scon;
    SqlCommand scmd;
    SqlDataAdapter sda;
    public DataSet ds;
    public DataTable dt;

	public Repostrategy()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void AddMWPosition(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("sp_InsertMWPosition", scon);
            scmd.Parameters.AddWithValue("@Symbol", obj.Symbol);
            scmd.Parameters.AddWithValue("@BusinessDate", obj.Bdate);
            scmd.Parameters.AddWithValue("@MiddleStrike", obj.Midstrike);
            scmd.Parameters.AddWithValue("@ExpDate", obj.Edate);
            scmd.Parameters.AddWithValue("@UserId", obj.Userid);
            scmd.Parameters.AddWithValue("@Strategy", obj.Strategy);
            scmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "AddMWPosition", ex.Message);
        }
    }

    public string getValues(string strat, string LS)
    {
        string value;
        switch (strat)
        {
            case "C-BF":
                {
                    value = LS + " Call Butterfly 1";
                    break;
                }
            case "C-BF2":
                {
                    value = LS + " Call Butterfly 2";
                    break;
                }
            case "C-BF3":
                {
                    value = LS + " C-BF3";
                    break;
                }
            case "C-Cal":
                {
                    value = LS + " C-CAL";
                    break;
                }
            case "PCP":
                {
                    value = LS + " PCP";
                    break;
                }
            //case "C-VR":
            //    return "Long Call Butterfly 2";
            //    break;
            case "C-DR":
                {
                    value = LS + " C-DR";
                    break;
                }
            //case "C-PR":
            //    return "Long Call Butterfly 2";
            //    break;
            case "STRADDLE":
                {
                    value = LS + " Straddle";
                    break;
                }
            case "P-BF":
                {
                    value = LS + " Put Butterfly 1";
                    break;
                }
            case "P-BF2":
                {
                    value = LS + " Put Butterfly 2";
                    break;
                }
            case "P-BF3":
                {
                    value = LS + " P-BF3";
                    break;
                }
            case "P-Cal":
                {
                    value = LS + " P-CAL";
                    break;
                }
            //case "P-PR":
            //    return "Long Call Butterfly 2";
            //    break;
            case "P-DR":
                {
                    value = LS + " P-DR";
                    break;
                }
            //case "P-VR":
            //    return "Long Call Butterfly 2";
            //    break;
            //case "P-IV%":
            //    return "Long Call Butterfly 2";
            //    break;
            default:
                value = "";
                break;

        }
        return value;

    }

    public DataTable GetMWPosition(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select * from tblMWPosition where Symbol=@Symbol and BusinessDate=@BusinessDate and ExpDate=@ExpDate and UserId=@UserId order by MiddleStrike;", scon);
            scmd.Parameters.AddWithValue("@Symbol", obj.Symbol);
            scmd.Parameters.AddWithValue("@BusinessDate", obj.Bdate);
            scmd.Parameters.AddWithValue("@ExpDate", obj.Edate);
            scmd.Parameters.AddWithValue("@UserId", obj.Userid);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "GetMWPosition", ex.Message);
        }
        return dt;
    }


    public DataTable ActionAdd(EntityStrategy obj)
    {
        try
        {
           
             DataTable dt_MW = new DataTable();
             dt_MW = GetMWPosition(obj);
             int cnt = dt_MW.Rows.Count;
             obj.Action = "Add";
             for (int i = 0; i < cnt; i++)
             {
                 obj.Midstrike = Convert.ToDouble(dt_MW.Rows[i][3]);
                 obj.Cpf = dt_MW.Rows[i][1].ToString();
                 dt = get_sp_Proc_Sim(obj);
             }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "ActionAdd", ex.Message);
        }
        return dt;
    }

    public DataTable getSimPosition(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select * from tblSimulatorPosition where Email=@Email", scon);
            scmd.Parameters.AddWithValue("@Email", obj.Userid);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getSimPosition", ex.Message);
        }
        return dt;
    }

    public DataTable ActionTrade(EntityStrategy obj)
    {
        try
        {
            
            DataTable dt_MW = new DataTable();
            dt_MW = GetMWPosition(obj);
            int cnt = dt_MW.Rows.Count;

            DataTable dtrowid = new DataTable();
            dtrowid = getSimPosition(obj);
            obj.Action = "Trade";
            for (int i = 0; i < cnt; i++)
            {
                obj.Midstrike = Convert.ToDouble(dt_MW.Rows[i][3]);
                obj.Cpf = dt_MW.Rows[i][1].ToString();
                obj.Qtycal = (Convert.ToDouble(obj.Qty) * Convert.ToDouble(dt_MW.Rows[i][4].ToString())).ToString();
                obj.Id = dtrowid.Rows[i][0].ToString();
                dt = get_sp_Proc_Sim(obj);     
            }

           
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "ActionTrade", ex.Message);
        }
        return dt;
    }

    

    public DataTable getFixVal(EntityStrategy obj)
    {
        try
        {
            obj.Action = "FixVal";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getFixVal", ex.Message);
        }
        return dt;
    }

    public DataTable getPNL(EntityStrategy obj)
    {
        try
        {
            obj.Action = "PNL";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getPNL", ex.Message);
        }
        return dt;
    }

    public DataTable getTimePlus(EntityStrategy obj, out int ch)
    {
        int change = 1;
        try
        {
            string Bdate = "";
           // string lastBdate = "";
            DataTable dt_sim = new DataTable();
            dt_sim = getSimPosition(obj);
            DateTime businessDate = DateTime.ParseExact(dt_sim.Rows[0][7].ToString(), "dd-MMM-yyyy", null);
            DateTime expiryDate = DateTime.ParseExact(dt_sim.Rows[0][8].ToString(), "dd-MMM-yyyy", null);
            Bdate = Convert.ToDateTime(businessDate).AddDays(1 * 1).ToString("dd-MMM-yyyy");
           // lastBdate = getLastAvailDate(obj);
            if (businessDate < expiryDate)
            {
                obj.Action = "TimePlus";
                change = 1;
                dt = get_sp_Proc_Sim(obj);
               
            }
            else
            {
                change = 0;
                dt.AcceptChanges();
               
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getTimePlus", ex.Message);
            dt = null;
        }
        ch = change;
        return dt;
  
    }

    public DataTable getTimeMinus(EntityStrategy obj)
    {
        try
        {
            obj.Action = "TimeMinus";
            dt = get_sp_Proc_Sim(obj);
            if (dt.Rows[0][9].ToString().Equals(""))
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getTimeMinus", ex.Message);
            dt = null;
        }
        return dt;
    }

    public DataTable getVolPlus(EntityStrategy obj)
    {
        try
        {
            obj.Action = "VolPlus";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getVolPlus", ex.Message);
            dt = null;
        }
        return dt;
    }

    public DataTable getVolMinus(EntityStrategy obj)
    {
        try
        {
            obj.Action = "VolMinus";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getVolMinus", ex.Message);
            dt = null;
        }
        return dt;
    }

    public DataTable getResetVol(EntityStrategy obj)
    {
        try
        {
            obj.Action = "ResetVol";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getResetVol", ex.Message);
            dt = null;
        }
        return dt;
    }
    public DataTable getSpotPlus(EntityStrategy obj)
    {
        try
        {
            obj.Action = "SpotPlus";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getSpotPlus", ex.Message);
            dt = null;
        }
        return dt;
    }

    public DataTable getSpotMinus(EntityStrategy obj)
    {
        try
        {
            obj.Action = "SpotMinus";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getSpotMinus", ex.Message);
            dt = null;
        }
        return dt;
    }

    public DataTable getResetSpot(EntityStrategy obj)
    {
        try
        {
            obj.Action = "ResetSpot";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getResetSpot", ex.Message);
            dt = null;
        }
        return dt;
    }

    private DataTable get_sp_Proc_Sim(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("sp_Proc_Sim", scon);
            scmd.Parameters.AddWithValue("@Action", obj.Action);
            scmd.Parameters.AddWithValue("@Symbol", obj.Symbol);
            scmd.Parameters.AddWithValue("@Email", obj.Userid);
            scmd.Parameters.AddWithValue("@Spot", obj.Spotprice);
            scmd.Parameters.AddWithValue("@Strike", obj.Midstrike);
            scmd.Parameters.AddWithValue("@CPF", obj.Cpf);
            scmd.Parameters.AddWithValue("@TradeDate", obj.Bdate);
            scmd.Parameters.AddWithValue("@ExpDate", obj.Edate);
            scmd.Parameters.AddWithValue("@Qty", obj.Qtycal);
            scmd.Parameters.AddWithValue("@Id", obj.Id);
            //scmd.Parameters.AddWithValue("@FutWeightedPrice", obj.Futwaitprice);
            scmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "get_sp_Proc_Sim", ex.Message);
            dt = null;
        }
        return dt;
    }

    public EntitySummary getSummary(DataTable objDT)
    {
        EntitySummary objSum = new EntitySummary();
        if (objDT != null)
        {
            objSum.DeltaVal = objDT.Compute("SUM([Delta Val])", string.Empty).ToString();
            objSum.GammaVal = objDT.Compute("SUM([Gamma Val])", string.Empty).ToString();
            objSum.VegaVal = objDT.Compute("SUM([Vega val])", string.Empty).ToString();
            objSum.ThetaVal = objDT.Compute("SUM([Theta Val])", string.Empty).ToString();
            objSum.VolgaVal = objDT.Compute("SUM([Volga val])", string.Empty).ToString();
            objSum.VannaVal = objDT.Compute("SUM([Vanna Val])", string.Empty).ToString();

            objSum.DeltaEffect = objDT.Compute("SUM([Delta Effect])", string.Empty).ToString();
            objSum.GammaEffect = objDT.Compute("SUM([Gamma Effect])", string.Empty).ToString();
            objSum.VegaEffect = objDT.Compute("SUM([Vega Effect])", string.Empty).ToString();
            objSum.ThetaEffect = objDT.Compute("SUM([Theta Effect])", string.Empty).ToString();
            objSum.VolgaEffect = objDT.Compute("SUM([Volga Effect])", string.Empty).ToString();
            objSum.VannaEffect = objDT.Compute("SUM([Vanna Effect])", string.Empty).ToString();

            objSum.PnlAmt = objDT.Compute("SUM([P&L])", string.Empty).ToString();
            objSum.Total = (Convert.ToDouble(objSum.DeltaEffect) + Convert.ToDouble(objSum.VegaEffect) + Convert.ToDouble(objSum.ThetaEffect)).ToString();
            return objSum;
        }
        else
        {
            objSum.DeltaVal = "0";
            objSum.GammaVal = "0";
            objSum.VegaVal = "0";
            objSum.ThetaVal = "0";
            objSum.VolgaVal = "0";
            objSum.VannaVal = "0";

            objSum.DeltaEffect = "0";
            objSum.GammaEffect = "0";
            objSum.VegaEffect = "0";
            objSum.ThetaEffect = "0";
            objSum.VolgaEffect = "0";
            objSum.VannaEffect = "0";

            objSum.PnlAmt = "0";
            objSum.Total = (Convert.ToDouble(objSum.DeltaEffect) + Convert.ToDouble(objSum.VegaEffect) + Convert.ToDouble(objSum.ThetaEffect)).ToString();
            return objSum;
        }
    }


    public DataTable getStepPrev(EntityStrategy obj)
    {
        try
        {
            obj.Action = "Prev";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getStepPrev", ex.Message);
            dt = null;
        }
        return dt;
    }

    public DataTable getStepNext(EntityStrategy obj)
    {
        try
        {
            obj.Action = "Next";
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getStepNext", ex.Message);
            dt = null;
        }
        return dt;
    }

    public string getExpiryByB_Date(string symbol,string Bdate)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select Top 1 EXPIRY_DATE from OptionCE_"+symbol+" where BUSINESS_DATE = '"+Bdate+"' order by EXPIRY_DATE ", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
               return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "getExpiryByB_Date", ex.Message);
            return "";
        }
     
    }

    public DataTable AddTrade(EntityStrategy obj)
    {
        try
        {
        
                DataTable dt_sim = new DataTable();
                dt_sim = getSimPosition(obj);

                if (dt_sim.Rows.Count > 0)
                {
                    obj.Spotprice = dt_sim.Rows[0][2].ToString();
                    //obj.Bdate = dt_sim.Rows[0][7].ToString();
                    obj.Edate = dt_sim.Rows[0][8].ToString();
                    if (check_strike(obj) == true)
                    {
                       
                        Ins_MW_Entry(obj);

                        //if (Check_SM_Entry(obj))
                        //{
                        //    update_SM_Qty(obj);
                        //}
                        //else
                        //{
                        obj.Action = "AddOne";

                            dt = get_sp_Proc_Sim(obj);
                        //}



                            obj.Action = "TradeOne";
                            obj.Id = dt.Rows[dt.Rows.Count - 1][0].ToString();
                            obj.Midstrike = Convert.ToDouble(dt.Rows[dt.Rows.Count - 1][3]);
                            obj.Cpf = dt.Rows[dt.Rows.Count - 1][5].ToString();
                            obj.Qtycal = (Convert.ToDouble(obj.Qty).ToString());
                            dt = get_sp_Proc_Sim(obj);
                    }
                    else
                    {
                        dt = null;
                    }

                }
                else
                {
                    setSpotandExpiry(obj);
                    Ins_MW_Entry(obj);
                    obj.Action = "Add";

                    dt = get_sp_Proc_Sim(obj);


                    obj.Action = "Trade";
                    obj.Id = dt.Rows[dt.Rows.Count - 1][0].ToString();
                    obj.Midstrike = Convert.ToDouble(dt.Rows[dt.Rows.Count - 1][3]);
                    obj.Cpf = dt.Rows[dt.Rows.Count - 1][5].ToString();
                    obj.Qtycal = (Convert.ToDouble(obj.Qty).ToString());
                    dt = get_sp_Proc_Sim(obj);
                    if (dt == null)
                    {
                        delete_MW_Entry_By_Email(obj.Userid);
                    }

                }
            
           

            
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "AddTrade", ex.Message);
        }
        return dt;
    }


    private bool Check_SM_Entry(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("SELECT * from tblSimulatorPosition where Strike = '" + obj.Midstrike + "' and Email = '" + obj.Userid + "' and CPF = '" + obj.Cpf + "'", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            if (ds.Tables.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "Check_SM_Entry", ex.Message);
            return false;
        }
        
    }

    private void update_SM_Qty(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("update tblSimulatorPosition set Qty = Qty" + obj.Qtycal + " where Strike = '" + obj.Midstrike + "' and Email = '" + obj.Userid + "' and CPF = '" + obj.Cpf + "'", scon);
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "update_SM_Qty", ex.Message);
        }
        
    }

    private void setSpotandExpiry(EntityStrategy obj)
    {
        try
        {
            string query = "select Top 1 FUTURE_PRICE,(convert(varchar,EXPIRY_DATE,106)) from Option" + obj.Cpf + "_" + obj.Symbol + " where STRIKE_PRICE = '" + obj.Midstrike + "' and BUSINESS_DATE = '" + obj.Bdate + "' order by FUTURE_PRICE";
            if (obj.Cpf == "FUT")
            {
                //obj.Cpf = "CE";

                query = "select Top 1 FUTURE_PRICE,(convert(varchar,EXPIRY_DATE,106)) from OptionCE_" + obj.Symbol + " where BUSINESS_DATE = '" + obj.Bdate + "' order by FUTURE_PRICE";
                
            }
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand(query, scon);
            scmd.CommandType = CommandType.Text;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            if (ds.Tables[0].Rows.Count > 0)
            {
                obj.Spotprice = ds.Tables[0].Rows[0][0].ToString();
                obj.Edate = Convert.ToDateTime(DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");

            }
            else
            {
                obj.Spotprice = "0";
                obj.Edate = "";
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "setSpotandExpiry", ex.Message);
        }
    }

    private void Ins_MW_Entry(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("sp_InsertMW_Entry", scon);
            scmd.Parameters.AddWithValue("@Symbol", obj.Symbol);
            scmd.Parameters.AddWithValue("@CPF", obj.Cpf);
            scmd.Parameters.AddWithValue("@Bdate", obj.Bdate);
            scmd.Parameters.AddWithValue("@Strike", obj.Midstrike);
            scmd.Parameters.AddWithValue("@Edate", obj.Edate);
            scmd.Parameters.AddWithValue("@UserId", obj.Userid);
           
            //scmd.Parameters.AddWithValue("@FutWeightedPrice", obj.Futwaitprice);
            scmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
            
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "Ins_MW_Entry", ex.Message);
        }
    }


    public DataTable updateTrade(EntityStrategy obj)
    {
        try
        {
            DataTable dt_simPos = new DataTable();
            dt_simPos = get_Sim_Pos_By_Id(obj);
            obj.Action = "TradeQty";
            obj.Midstrike = Convert.ToDouble(dt_simPos.Rows[0][3]);
            obj.Spotprice = dt_simPos.Rows[0][2].ToString();
            obj.Cpf = dt_simPos.Rows[0][5].ToString();
            obj.Bdate = dt_simPos.Rows[0][7].ToString();
            obj.Edate = dt_simPos.Rows[0][8].ToString();
            obj.Qtycal = (Convert.ToDouble(obj.Qty).ToString());
            
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "updateTrade", ex.Message);
        }
        return dt;
    }

    private DataTable get_Sim_Pos_By_Id(EntityStrategy objent)
    {

        scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
        scmd = new SqlCommand("select * from tblSimulatorPosition where srno='" + objent.Id + "'", scon);
        sda = new SqlDataAdapter(scmd);
        ds = new DataSet();
        sda.Fill(ds, "us");
        if (ds.Tables.Count > 0)
        {
            dt = ds.Tables[0];
        }

        return dt;
    }


    public void delete_MW_Entry_By_Email(string email)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("delete from tblMWPosition where UserId = '" + email + "' delete from tblSimulatorPosition where Email = '" + email + "' delete from tblSett_Simulator where Email = '" + email + "' delete from tbl_SimPosition_History where Email = '" + email + "'", scon);
            scmd.CommandType = CommandType.Text;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "delete_MW_Entry_By_Email", ex.Message);
        }
    }

    public DataTable Delete_Trade(int id, string email)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("delete from tblSimulatorPosition where srno = '" + id + "' and Email = '" + email + "'", scon);
            scmd.CommandType = CommandType.Text;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            scmd = new SqlCommand("select * from tblSimulatorPosition where Email = '" + email + "'", scon);
            scmd.CommandType = CommandType.Text;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            else
            {
                return dt = null;
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "Delete_Trade", ex.Message);
            dt = null;
        }
        return dt;
    }


    private bool check_strike(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select * from Option" + obj.Cpf + "_" + obj.Symbol + " where STRIKE_PRICE = " + obj.Midstrike + "", scon);
            scmd.CommandType = CommandType.Text;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            else
            {
                return false;
            }
            
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "check_strike", ex.Message);
            return false;
        }
    }


    public DataTable Add_Fut_Buy(EntityStrategy obj)
    {
        try
        {
            setSpotandExpiry(obj);
            Ins_MW_Entry(obj);
            obj.Action = "AddOne";

            dt = get_sp_Proc_Sim(obj);


            obj.Action = "FutBuy";
            obj.Id = dt.Rows[dt.Rows.Count - 1][0].ToString();
            obj.Midstrike = Convert.ToDouble(dt.Rows[dt.Rows.Count - 1][3]);
            obj.Cpf = dt.Rows[dt.Rows.Count - 1][5].ToString();
            obj.Qtycal = (Convert.ToDouble(obj.Qty).ToString());
            dt = get_sp_Proc_Sim(obj);
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "Add_Fut_Buy", ex.Message);
        }
        return dt;
    }

    public string get_Recording_Step(string email)
    {
        string step = "";
        try
        {
            
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("select Curr_Step from tbl_SimPosition_History where Email = '"+email+"'", scon);
            scmd.CommandType = CommandType.Text;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            if (ds.Tables[0].Rows.Count > 0)
            {
                 step =  ds.Tables[0].Rows[0][0].ToString();
            }
        }
        catch (Exception ex)
            {
                WriteLogFile.logWriter("Repostrategy.cs", "get_Recording_Step", ex.Message);
                return "";
            }
        return step;
       
    }


    public DataTable get_Recordings_By_Email(string Email)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("Sp_SimReport", scon);
            scmd.Parameters.AddWithValue("@Email", Email);
            scmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

            if (ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];
            }
        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "get_Recordings_By_Email", ex.Message);
            dt = null;
        }
        return dt;
    }


    public void set_Recording(EntityStrategy obj)
    {
        try
        {
            scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            scmd = new SqlCommand("sp_StepRecording", scon);
            scmd.Parameters.AddWithValue("@Action", "Set");
            scmd.Parameters.AddWithValue("@Email", obj.Userid);
            scmd.Parameters.AddWithValue("@Step", 1);

            scmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(scmd);
            ds = new DataSet();
            sda.Fill(ds, "us");

        }
        catch (Exception ex)
        {
            WriteLogFile.logWriter("Repostrategy.cs", "set_Recording", ex.Message);
            dt = null;
        }
        
    } 

    //private string getLastAvailDate(EntityStrategy obj)
    //{
    //    string date = "";
    //    try
    //    {
    //        scon = new SqlConnection(ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
    //        scmd = new SqlCommand("select top 1 BUSINESS_DATE from OptionCE_" + obj.Symbol + " order by BUSINESS_DATE desc", scon);
    //        scmd.CommandType = CommandType.Text;
    //        sda = new SqlDataAdapter(scmd);
    //        ds = new DataSet();
    //        sda.Fill(ds, "us");

    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            date = Convert.ToDateTime(DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
    //        }
    //    }
    //    catch(Exception ex)
    //    {
    //        return "";
    //    }

    //    return date;
    //}
   
}