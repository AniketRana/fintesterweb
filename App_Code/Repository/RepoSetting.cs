﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Linq;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for RepoSetting
/// </summary>
public class RepoSetting
{
    EntitySetting obj;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FinIdeas"].ConnectionString);
    DataSet ds = new DataSet();

    public EntitySetting getSettingByEmail(string email)
    {
        obj = new EntitySetting();
        var dc = new SettingDataLinqDataContext();
        var settings = dc.tblSettings.Where(s => s.EmailID == email).ToList();

        for (var i = 0; i < settings.Count; i++)
        {
            var md = settings[i].Module;
            var settingName = settings[i].SettingName;
            int roundvalue;

            if (!(settings[i].SettingValue == ""))
            {
                roundvalue = Convert.ToInt32(settings[i].SettingValue);
            }
            else
            {
                roundvalue = 0;
            }
            switch (settingName)
            {
                case "2NDGREEKS_ROUNDING":
                {
                    obj.R_2ndgreeks = roundvalue;  
                    break;
                }

                case "BF_ROUNDING":
                {
                    obj.R_Butterfly = roundvalue;
                    break;
                }
                case "BF2_ROUNDING":
                {
                    obj.R_Butterfly2 = roundvalue;
                    break;
                }
                case "CALENDER_ROUNDING":
                {
                    obj.R_Calendar = roundvalue;
                    break;
                }
                case "GREEKS_ROUNDING":
                {
                    obj.R_Greeks = roundvalue;
                    break;
                }
                case "OI_ROUNDING":
                {
                    obj.R_OpenInt = roundvalue;
                    try
                    {
                        if (obj.R_OpenInt == -3)
                            obj.R_OI = 1000;
                        else if (obj.R_OpenInt == -2)
                            obj.R_OI = 100;
                        else if (obj.R_OpenInt == -1)
                            obj.R_OI = 10;
                        if (obj.R_OpenInt == 0)
                            obj.R_OI = 1;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                }
                case "PCP_ROUNDING":
                {
                    obj.R_PCP = roundvalue;
                    break;
                }
                case "PREMIUM_ROUNDING":
                {
                    if (md == "MW")
                    {
                        obj.R_Premium = roundvalue;
                    }
                    else
                    {
                        obj.R_smPremium = roundvalue;  //rounding for simulator
                    }
                    break;
                }
                case "RATIO_ROUNDING":
                {
                    obj.R_Ratio = roundvalue;
                    break;
                }
                case "STRADDLE_ROUNDING":
                {
                    obj.R_Straddle = roundvalue;
                    break;
                }
                case "VOLSPREAD_ROUNDING":
                {
                    obj.R_VolSpreads = roundvalue;
                    break;
                }
                case "VOLATALITY_ROUNDING":
                {
                    if(md == "MW")
                    {
                    obj.R_Volatility = roundvalue;
                    }
                    else
                    {
                        obj.R_Volatality = roundvalue;   //ronding for simulator
                    }
                    break;
                }
                case "Volume":
                {
                    obj.R_Volume = roundvalue;
                    break;
                }
                case "QTY":
                {
                    obj.R_Quantity = roundvalue;
                    break;
                }
                case "2NDGREEKS_HU":
                {
                    obj.Hu_2ndgreeks = roundvalue;
                    break;
                }
                case "BF_HU":
                {
                    obj.Hu_BF = roundvalue;
                    break;
                }
                case "BF2_HU":
                {
                    obj.Hu_BF2 = roundvalue;
                    break;
                }
                case "CALENDER_HU":
                {
                    obj.Hu_Calender = roundvalue;
                    break;
                }
                case "GREEKS_HU":
                {
                    if (md == "MW")
                        obj.Hu_Greeks = roundvalue;
                    else
                        obj.Hu_Sm_Greeks = roundvalue;
                    break;
                }
                case "OI_HU":
                {
                    obj.Hu_OI = roundvalue;
                    break;
                }
                case "OTHER_HU":
                {
                    obj.Hu_Other = roundvalue;
                    break;
                }
                case "PCP_HU":
                {
                    obj.Hu_PCP = roundvalue;
                    break;
                }
                case "PREMIUM_HU":
                {
                    obj.Hu_Premium = roundvalue;
                    break;
                }
                case "RATIO_HU":
                {
                    obj.Hu_Ratio = roundvalue;
                    break;
                }
                case "STRADDLE_HU":
                {
                    obj.Hu_Straddle = roundvalue;
                    break;
                }
                case "VOL_HU":
                {
                    obj.Hu_Vol = roundvalue;
                    break;
                }
                case "VOLSPREAD_HU":
                {
                    obj.Hu_Volspread = roundvalue;
                    break;
                }
                case "VOLUME_HU":
                {
                    obj.Hu_Volume = roundvalue;
                    break;
                }





                    //ronding for simulator

                case "PRICE_ROUNDING":
                    {
                        obj.R_Price = roundvalue;
                        break;
                    }
                case "QTY_ROUNDING":
                    {
                        obj.R_Qty = roundvalue;
                        break;
                    }
                case "1GREEKSVALUE_ROUNDING":
                    {
                        obj.R_1stGreeksValue = roundvalue;
                        break;
                    }
                case "2GREEKSVALUE_ROUNDING":
                    {
                        obj.R_2ndGreeksvalue = roundvalue;
                        break;
                    }
                case "1GREEKS_ROUNDING":
                    {
                        obj.R_1stGreeks = roundvalue;
                        break;
                    }
                case "2GREEKS_ROUNDING":
                    {
                        obj.R_2ndGreeks = roundvalue;
                        break;
                    }
                case "PNL_ROUNDING":
                    {
                        obj.R_pnl = roundvalue;
                        break;
                    }
                case "DEFAULT_HU":
                    {
                        obj.Hu_Sm_Default = roundvalue;
                        break;
                    }
                case "DELTAEFFECT_HU":
                    {
                        obj.Hu_Sm_DeltaEffect = roundvalue;
                        break;
                    }
                case "GREEKSVALUE_HU":
                    {
                        obj.Hu_Sm_GreeksValue = roundvalue;
                        break;
                    }
                case "Hide_HU":
                    {
                        obj.Hu_Sm_Hide = roundvalue;
                        break;
                    }
                case "PNL_HU":
                    {
                        obj.Hu_Sm_Pnl = roundvalue;
                        break;
                    }
                case "THETAEFFECT_HU":
                    {
                        obj.Hu_Sm_ThetaEffect = roundvalue;
                        break;
                    }
                case "VEGAEFFECT_HU":
                    {
                        obj.Hu_Sm_VegaEffect = roundvalue;
                        break;
                    }
               
                default:
                   {
                       break;
                   }
            }
            
        }
        return obj;
    }

    public void SaveDefaultSetting(string Email, string Module)
    {
        cnnstr.Open();
        SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_ROUNDING','" + -1 + "'", cnnstr);
        OIcmd.ExecuteNonQuery();

        SqlCommand PCPcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_ROUNDING','" + 2 + "'", cnnstr);
        PCPcmd.ExecuteNonQuery();

        SqlCommand Greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_ROUNDING','" + 2 + "'", cnnstr);
        Greekcmd.ExecuteNonQuery();

        SqlCommand Premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_ROUNDING','" + 2 + "'", cnnstr);
        Premiumcmd.ExecuteNonQuery();

        SqlCommand Calendercmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','CALENDER_ROUNDING','" + 2 + "'", cnnstr);
        Calendercmd.ExecuteNonQuery();

        SqlCommand Ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_ROUNDING','" + 2 + "'", cnnstr);
        Ratiocmd.ExecuteNonQuery();

        SqlCommand Bfcmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','BF_ROUNDING','" + 2 + "'", cnnstr);
        Bfcmd.ExecuteNonQuery();

        SqlCommand Bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_ROUNDING','" + 2 + "'", cnnstr);
        Bf2cmd.ExecuteNonQuery();

        SqlCommand Straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_ROUNDING','" + 2 + "'", cnnstr);
        Straddlecmd.ExecuteNonQuery();

        SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_ROUNDING','" + 2 + "'", cnnstr);
        ndGreekcmd.ExecuteNonQuery();

        SqlCommand VolSpreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_ROUNDING','" + 2 + "'", cnnstr);
        VolSpreadcmd.ExecuteNonQuery();

        SqlCommand Volatalitycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLATALITY_ROUNDING','" + 2 + "'", cnnstr);
        Volatalitycmd.ExecuteNonQuery();

        SqlCommand Volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_ROUNDING','" + 2 + "'", cnnstr);
        Volumecmd.ExecuteNonQuery();

        SqlCommand Qtycmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','QTY','" + 1000 + "'", cnnstr);
        Qtycmd.ExecuteNonQuery();

        SqlCommand UpDowncmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','UD_STRIKE','" + 10 + "'", cnnstr);
        UpDowncmd.ExecuteNonQuery();

        cnnstr.Close();

        HideUndHideSetting(Email, Module);
        SaveDefaultSettingSM(Email, "SM");
    }

    private void HideUndHideSetting(string Email, string Module)
    {
        cnnstr.Open();


        SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_HU','1'", cnnstr);
        OIcmd.ExecuteNonQuery();



        SqlCommand pcpcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_HU','1'", cnnstr);
        pcpcmd.ExecuteNonQuery();



        SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_HU','1'", cnnstr);
        greekcmd.ExecuteNonQuery();



        SqlCommand premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_HU','1'", cnnstr);
        premiumcmd.ExecuteNonQuery();



        SqlCommand calandercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','CALENDER_HU','1'", cnnstr);
        calandercmd.ExecuteNonQuery();


        SqlCommand ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_HU','1'", cnnstr);
        ratiocmd.ExecuteNonQuery();



        SqlCommand bfcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF_HU','1'", cnnstr);
        bfcmd.ExecuteNonQuery();



        SqlCommand straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_HU','1'", cnnstr);
        straddlecmd.ExecuteNonQuery();





        SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','1'", cnnstr);
        ndGreekcmd.ExecuteNonQuery();


        SqlCommand volspreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_HU','1'", cnnstr);
        volspreadcmd.ExecuteNonQuery();


        SqlCommand volcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOL_HU','1'", cnnstr);
        volcmd.ExecuteNonQuery();


        SqlCommand volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_HU','1'", cnnstr);
        volumecmd.ExecuteNonQuery();


        SqlCommand bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_HU','1'", cnnstr);
        bf2cmd.ExecuteNonQuery();


        SqlCommand othercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OTHER_HU','1'", cnnstr);
        othercmd.ExecuteNonQuery();

        cnnstr.Close();
    }

    private void SaveDefaultSettingSM(string Email, string SMModule)
    {
        cnnstr.Open();
        SqlCommand pricecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PRICE_ROUNDING','" + 2 + "'", cnnstr);
        pricecmd.ExecuteNonQuery();

        SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_ROUNDING','" + 2 + "'", cnnstr);
        pnlcmd.ExecuteNonQuery();

        SqlCommand FsGrkcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','1GREEKS_ROUNDING','" + 2 + "'", cnnstr);
        FsGrkcmd.ExecuteNonQuery();

        SqlCommand FsGrkValCmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','1GREEKSVALUE_ROUNDING','" + 2 + "'", cnnstr);
        FsGrkValCmd.ExecuteNonQuery();

        SqlCommand NdGrkcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','2GREEKS_ROUNDING','" + 2 + "'", cnnstr);
        NdGrkcmd.ExecuteNonQuery();

        SqlCommand NdGrkValCmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','2GREEKSVALUE_ROUNDING','" + 2 + "'", cnnstr);
        NdGrkValCmd.ExecuteNonQuery();

        SqlCommand Premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PREMIUM_ROUNDING','" + 2 + "'", cnnstr);
        Premiumcmd.ExecuteNonQuery();

        SqlCommand sVolatalitycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VOLATALITY_ROUNDING','" + 2 + "'", cnnstr);
        sVolatalitycmd.ExecuteNonQuery();

        SqlCommand Sqtycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','QTY_ROUNDING','" + 2 + "'", cnnstr);
        Sqtycmd.ExecuteNonQuery();

        cnnstr.Close();

        SM_HU_Setting(Email, SMModule);
    }


    private void SM_HU_Setting(string Email, string SMModule)
    {
        cnnstr.Open();

            SqlCommand defcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DEFAULT_HU','1'", cnnstr);
            defcmd.ExecuteNonQuery();
        
            SqlCommand Hidecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','Hide_HU','1'", cnnstr);
            Hidecmd.ExecuteNonQuery();

            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKS_HU','1'", cnnstr);
            greekcmd.ExecuteNonQuery();
        
            SqlCommand Sgreekvalcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKSVALUE_HU','1'", cnnstr);
            Sgreekvalcmd.ExecuteNonQuery();
        
            SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_HU','1'", cnnstr);
            pnlcmd.ExecuteNonQuery();
        
            SqlCommand deltacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DELTAEFFECT_HU','1'", cnnstr);
            deltacmd.ExecuteNonQuery();
        
            SqlCommand vegacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VEGAEFFECT_HU','1'", cnnstr);
            vegacmd.ExecuteNonQuery();
        
            SqlCommand Thetacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','THETAEFFECT_HU','1'", cnnstr);
            Thetacmd.ExecuteNonQuery();

        cnnstr.Close();
    }


}

