﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsBOL
/// </summary>
public class ClsBOL
{
    String mEmail;
    String mPasswd;
    String mGenRateKey;
    String mCheckKey;
    
	public ClsBOL()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public String Email
    {
        get
        {
            return mEmail ;
        }
        set
        {
            mEmail= value;
        }
    }
    public String Password
    {
        get
        {
            return mPasswd ;
        }
        set
        {
            mPasswd= value;
        }
    }
    public String GenRateKey
    {
        get
        {
            return mGenRateKey;
        }
        set
        {
            mGenRateKey = value;
        }
    }
    public String CheckKey
    {
        get
        {
            return mCheckKey;
        }
        set
        {
            mCheckKey = value;
        }
    }

}
