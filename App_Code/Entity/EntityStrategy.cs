﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntityStrategy
/// </summary>
public class EntityStrategy
{
	public EntityStrategy()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string symbol;
    private string bdate;
    private double midstrike;
    private string edate;
    private string userid;
    private string strategy;
    private string cpf;
    private string lot;
    private string script;
    private string spotprice;
    private string action;
    private string qty;
    private string rowid;
    private string futwaitprice;
    private string qtycal;
    private string id;

    public string Id
    {
        get { return id; }
        set { id = value; }
    }

    public string Qtycal
    {
        get { return qtycal; }
        set { qtycal = value; }
    }



    public string Futwaitprice
    {
        get { return futwaitprice; }
        set { futwaitprice = value; }
    }

    public string Rowid
    {
        get { return rowid; }
        set { rowid = value; }
    }

    public string Qty
    {
        get { return qty; }
        set { qty = value; }
    }
    

    public string Action
    {
        get { return action; }
        set { action = value; }
    }

    public string Spotprice
    {
        get { return spotprice; }
        set { spotprice = value; }
    }


    public string Script
    {
        get { return script; }
        set { script = value; }
    }

    public string Lot
    {
        get { return lot; }
        set { lot = value; }
    }
   

    public string Cpf
    {
        get { return cpf; }
        set { cpf = value; }
    }
   
    
   
    public string Symbol
    {
        get { return symbol; }
        set { symbol = value; }
    }
    

    public string Bdate
    {
        get { return bdate; }
        set { bdate = value; }
    }
    

    public double Midstrike
    {
        get { return midstrike; }
        set { midstrike = value; }
    }
    

    public string Edate
    {
        get { return edate; }
        set { edate = value; }
    }
    

    public string Userid
    {
        get { return userid; }
        set { userid = value; }
    }
   

    public string Strategy
    {
        get { return strategy; }
        set { strategy = value; }
    }

}