﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntitySettingInfo
/// </summary>
public class EntitySettingInfo
{
	public EntitySettingInfo()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string email { get; set; }
    public string module { get; set; }
    public string name { get; set; }
    public string value { get; set; }
}