﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntitySummary
/// </summary>
public class EntitySummary
{
	public EntitySummary()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string deltaVal;

    public string DeltaVal
    {
        get { return deltaVal; }
        set { deltaVal = value; }
    }
    private string gammaVal;

    public string GammaVal
    {
        get { return gammaVal; }
        set { gammaVal = value; }
    }
    private string vegaVal;

    public string VegaVal
    {
        get { return vegaVal; }
        set { vegaVal = value; }
    }
    private string thetaVal;

    public string ThetaVal
    {
        get { return thetaVal; }
        set { thetaVal = value; }
    }
    private string volgaVal;

    public string VolgaVal
    {
        get { return volgaVal; }
        set { volgaVal = value; }
    }
    private string vannaVal;

    public string VannaVal
    {
        get { return vannaVal; }
        set { vannaVal = value; }
    }
    private string pnlAmt;

    public string PnlAmt
    {
        get { return pnlAmt; }
        set { pnlAmt = value; }
    }
    private string deltaEffect;

    public string DeltaEffect
    {
        get { return deltaEffect; }
        set { deltaEffect = value; }
    }
    private string gammaEffect;

    public string GammaEffect
    {
        get { return gammaEffect; }
        set { gammaEffect = value; }
    }
    private string vegaEffect;

    public string VegaEffect
    {
        get { return vegaEffect; }
        set { vegaEffect = value; }
    }
    private string thetaEffect;

    public string ThetaEffect
    {
        get { return thetaEffect; }
        set { thetaEffect = value; }
    }
    private string volgaEffect;

    public string VolgaEffect
    {
        get { return volgaEffect; }
        set { volgaEffect = value; }
    }
    private string vannaEffect;

    public string VannaEffect
    {
        get { return vannaEffect; }
        set { vannaEffect = value; }
    }
    private string total;

    public string Total
    {
        get { return total; }
        set { total = value; }
    }

    
}