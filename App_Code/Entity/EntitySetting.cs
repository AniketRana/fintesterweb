﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntitySetting
/// </summary>
public class EntitySetting
{
	

    private int hu_2ndgreeks;
    private int hu_BF;
    private int hu_BF2;
    private int hu_Calender;
    private int hu_Greeks;
    private int hu_Other;
    private int hu_OI;
    private int hu_PCP;
    private int hu_Premium;
    private int hu_Ratio;
    private int hu_Straddle;
    private int hu_Vol;
    private int hu_Volspread;
    private int hu_Volume;


    public int Hu_2ndgreeks
    {
        get { return hu_2ndgreeks; }
        set { hu_2ndgreeks = value; }
    }
    

    public int Hu_BF
    {
        get { return hu_BF; }
        set { hu_BF = value; }
    }
    

    public int Hu_BF2
    {
        get { return hu_BF2; }
        set { hu_BF2 = value; }
    }
    

    public int Hu_Calender
    {
        get { return hu_Calender; }
        set { hu_Calender = value; }
    }
    

    public int Hu_Greeks
    {
        get { return hu_Greeks; }
        set { hu_Greeks = value; }
    }
    

    public int Hu_OI
    {
        get { return hu_OI; }
        set { hu_OI = value; }
    }
    

    public int Hu_Other
    {
        get { return hu_Other; }
        set { hu_Other = value; }
    }
    

    public int Hu_PCP
    {
        get { return hu_PCP; }
        set { hu_PCP = value; }
    }
    

    public int Hu_Premium
    {
        get { return hu_Premium; }
        set { hu_Premium = value; }
    }
    

    public int Hu_Ratio
    {
        get { return hu_Ratio; }
        set { hu_Ratio = value; }
    }
    

    public int Hu_Straddle
    {
        get { return hu_Straddle; }
        set { hu_Straddle = value; }
    }
    

    public int Hu_Vol
    {
        get { return hu_Vol; }
        set { hu_Vol = value; }
    }
    

    public int Hu_Volspread
    {
        get { return hu_Volspread; }
        set { hu_Volspread = value; }
    }
    

    public int Hu_Volume
    {
        get { return hu_Volume; }
        set { hu_Volume = value; }
    }


    //declaration of runding variable 
    private int r_2ndgreeks;
    private int r_Butterfly;
    private int r_Butterfly2;
    private int r_Calendar;
    private int r_Greeks;
    private int r_OI;
    private int r_OpenInt;
    private int r_PCP;
    private int r_Premium;
    private int r_Ratio;
    private int r_Straddle;
    private int r_Volatility;
    private int r_VolSpreads;
    private int r_Volume;
    private int r_Quantity;

    public int R_Quantity
    {
        get { return r_Quantity; }
        set { r_Quantity = value; }
    }

    public int R_2ndgreeks
    {
        get { return r_2ndgreeks; }
        set { r_2ndgreeks = value; }
    }
    

    public int R_Butterfly
    {
        get { return r_Butterfly; }
        set { r_Butterfly = value; }
    }
    

    public int R_Butterfly2
    {
        get { return r_Butterfly2; }
        set { r_Butterfly2 = value; }
    }
    

    public int R_Calendar
    {
        get { return r_Calendar; }
        set { r_Calendar = value; }
    }
    

    public int R_Greeks
    {
        get { return r_Greeks; }
        set { r_Greeks = value; }
    }
    

    public int R_OI
    {
        get { return r_OI; }
        set { r_OI = value; }
    }
    

    public int R_OpenInt
    {
        get { return r_OpenInt; }
        set { r_OpenInt = value; }
    }
    

    public int R_PCP
    {
        get { return r_PCP; }
        set { r_PCP = value; }
    }
    

    public int R_Premium
    {
        get { return r_Premium; }
        set { r_Premium = value; }
    }
    

    public int R_Ratio
    {
        get { return r_Ratio; }
        set { r_Ratio = value; }
    }
    

    public int R_Straddle
    {
        get { return r_Straddle; }
        set { r_Straddle = value; }
    }
    

    public int R_Volatility
    {
        get { return r_Volatility; }
        set { r_Volatility = value; }
    }
    

    public int R_VolSpreads
    {
        get { return r_VolSpreads; }
        set { r_VolSpreads = value; }
    }
    

    public int R_Volume
    {
        get { return r_Volume; }
        set { r_Volume = value; }
    }







    //Entity Setting for Simulator
    //=====================================================================================================================
    private int r_Price;

    public int R_Price
    {
        get { return r_Price; }
        set { r_Price = value; }
    }


    private int r_1stGreeks;

    public int R_1stGreeks
    {
        get { return r_1stGreeks; }
        set { r_1stGreeks = value; }
    }


    private int r_2ndGreeks;

    public int R_2ndGreeks
    {
        get { return r_2ndGreeks; }
        set { r_2ndGreeks = value; }
    }


    private int r_pnl;

    public int R_pnl
    {
        get { return r_pnl; }
        set { r_pnl = value; }
    }


    private int r_1stGreeksValue;

    public int R_1stGreeksValue
    {
        get { return r_1stGreeksValue; }
        set { r_1stGreeksValue = value; }
    }


    private int r_2ndGreeksvalue;

    public int R_2ndGreeksvalue
    {
        get { return r_2ndGreeksvalue; }
        set { r_2ndGreeksvalue = value; }
    }


    private int r_smPremium;

    public int R_smPremium
    {
        get { return r_smPremium; }
        set { r_smPremium = value; }
    }


    private int r_Volatality;

    public int R_Volatality
    {
        get { return r_Volatality; }
        set { r_Volatality = value; }
    }


    private int r_Qty;

    public int R_Qty
    {
        get { return r_Qty; }
        set { r_Qty = value; }
    }

   

    //Hide Unhide Simulator
    //=====================================================================================================================

    private int hu_Sm_Default;

    public int Hu_Sm_Default
    {
        get { return hu_Sm_Default; }
        set { hu_Sm_Default = value; }
    }
    private int hu_Sm_Hide;

    public int Hu_Sm_Hide
    {
        get { return hu_Sm_Hide; }
        set { hu_Sm_Hide = value; }
    }
    private int hu_Sm_Greeks;

    public int Hu_Sm_Greeks
    {
        get { return hu_Sm_Greeks; }
        set { hu_Sm_Greeks = value; }
    }
    private int hu_Sm_GreeksValue;

    public int Hu_Sm_GreeksValue
    {
        get { return hu_Sm_GreeksValue; }
        set { hu_Sm_GreeksValue = value; }
    }
    private int hu_Sm_Pnl;

    public int Hu_Sm_Pnl
    {
        get { return hu_Sm_Pnl; }
        set { hu_Sm_Pnl = value; }
    }
    private int hu_Sm_DeltaEffect;

    public int Hu_Sm_DeltaEffect
    {
        get { return hu_Sm_DeltaEffect; }
        set { hu_Sm_DeltaEffect = value; }
    }
    private int hu_Sm_VegaEffect;

    public int Hu_Sm_VegaEffect
    {
        get { return hu_Sm_VegaEffect; }
        set { hu_Sm_VegaEffect = value; }
    }
    private int hu_Sm_ThetaEffect;

    public int Hu_Sm_ThetaEffect
    {
        get { return hu_Sm_ThetaEffect; }
        set { hu_Sm_ThetaEffect = value; }
    }


}