﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntitySymbol
/// </summary>
public class EntitySymbol
{
	public EntitySymbol()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string symbol;
    private string bdate;
    private string edate;
    private string user;
    
    public string Symbol
    {
        get { return symbol; }
        set { symbol = value; }
    }
  

    public string Bdate
    {
        get { return bdate; }
        set { bdate = value; }
    }
   

    public string Edate
    {
        get { return edate; }
        set { edate = value; }
    }
    

    public string User
    {
        get { return user; }
        set { user = value; }
    }
}