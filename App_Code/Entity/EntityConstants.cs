﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntityConstants
/// </summary>
public class EntityConstants
{
	public EntityConstants()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private const string symbol = "NIFTY100";
    private const string bdate = "12-jun-2017";
    private const string edate = "29-Jun-2017";



    public string Symbol
    {
        get { return symbol; }
    }
    public string Bdate
    {
        get { return bdate; }
    }
    public string Edate
    {
        get { return edate; }
    } 

}