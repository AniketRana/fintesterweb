﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <link href="dist/css/fancy-buttons.css" rel="stylesheet" type="text/css" />
    <link href="dist/css/pricingStyle.css" rel="stylesheet" type="text/css" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <%-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>--%>
  

   <style> 
#panel, #flip {
    padding: 5px;
    text-align: center;
    background-color: #e5eecc;
    border: solid 1px #c3c3c3;
    cursor: pointer;
    color:Black;
}

#panel {
    display: none;
    padding-left: 50px;
    padding-top: 15px;
    padding-bottom: 15px;
}

.pricefont
{
    font-size: inherit;
    line-height: inherit;
}

.superText
{
    font-size: large;
    right:0px;
}
</style>

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">
            <!-- Row -->
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-red">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim">
                                                    <asp:Label ID="lblNoofUser" runat="server" Text=""></asp:Label>
                                                </span></span><span class="weight-500 uppercase-font txt-light block font-13">No. Of
                                                    User</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="zmdi zmdi-male-female txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-yellow">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim">2009</span></span>
                                                <span class="weight-500 uppercase-font txt-light block">Established Years</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="zmdi zmdi-redo txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-green">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim">9</span></span>
                                                <span class="weight-500 uppercase-font txt-light block">Experience Years </span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="zmdi zmdi-file txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-blue">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim">
                                                    <asp:Label ID="lblNoOfHit" runat="server" Text=""></asp:Label>
                                                </span></span><span class="weight-500 uppercase-font txt-light block">LIVE</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 pt-25  data-wrap-right">
                                                <div id="sparkline_4" style="width: 100px; overflow: hidden; margin: 0px auto;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view panel-refresh">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <asp:Button ID="btnMarketwatch" class="btn btn-default btn-block" runat="server"
                                    Text="Marketwatch" onclick="btnMarketwatch_Click"  />
                                <hr class="light-grey-hr row mt-10 mb-15">
                                <asp:Button ID="btnsimulator" class="btn btn-default btn-block" runat="server" 
                                    Text="Simulator" onclick="btnsimulator_Click" />
                                <hr class="light-grey-hr row mt-10 mb-15">
                                <asp:Button ID="btnTrend" class="btn btn-default btn-block" runat="server" 
                                    Text="Trends" onclick="btnTrend_Click" />
                                <hr class="light-grey-hr row mt-10 mb-15">
                                <asp:Button ID="btnSetting" class="btn btn-default btn-block" runat="server" 
                                    Text="Setting" onclick="btnSetting_Click" />
                            </div>
                        </div> 
                    </div> 
                </div>
                <%--<div id="container" style="height: 400px; min-width: 700px; float:left"></div>--%>
            <%--    <div class="col-lg-9 col-md-18 col-sm-18 col-xs-18">
                    <asp:Chart ID="Chart1" runat="server" class="morris-chart tooltip" 
                        Width="900px" BackColor="Window" BackImageAlignment="Center" BorderlineWidth="10"
                        CssClass="morris-chart " Palette="SemiTransparent" 
                        DataSourceID="SqlDataSource2">
                        <Series>
                            <asp:Series Name="Series1" YValuesPerPoint="2" XValueMember="BUSINESS_DATE" YValueMembers="FUTURE_PRICE"
                                ToolTip="" ChartType="StackedArea">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:SQLDB01TConnectionString %>" 
                        SelectCommand="SELECT [BUSINESS_DATE], [FUTURE_PRICE] FROM [v_Chart]"></asp:SqlDataSource>
                </div>--%>
            </div>
            
          <%--  <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default card-view">
                        <asp:Chart ID="Chart2" runat="server" 
                            Width="557px" DataSourceID="SqlDataSource1">
                            <Series>
                                <asp:Series Name="Series2" XValueMember="EXPIRY_DATE" 
                                     YValueMembers="SETTLE_PRICE" ChartType="Pie" YValuesPerPoint="4">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1">
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>

                        <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                            ConnectionString="<%$ ConnectionStrings:SQLDB01TConnectionString2 %>" 
                            SelectCommand="select Distinct top 10 EXPIRY_DATE,LTP,SETTLE_PRICE from OptionCE_NIFTY">
                        </asp:SqlDataSource>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default card-view">
                        <iframe width="560" height="300" src="https://www.youtube.com/embed/jDD-wYzSNBg?list=PLC2D9EZOgO5ILwJi5qxiRSQO4GLpOvPYD" frameborder="0" allowFullScreen>
                        </iframe>
                    </div>
                </div>
            </div>--%>
            
         <section style="text-align: center;">

    
    <section class="third lift plan-tier" style="margin-left: 27px;" onclick="location.href='#';">

      
      <h4>Free Trial</h4>
      <h3 class = "pricefont"><sup class="superscript superText"></sup><span class="plan-price">Free</span><sub></sub></h3>

      
      <p class="early-adopter-price">Full Stock</p>
     
      

      <ul>
<li><strong>Option,Index</strong></li>

<li>7 - Days<strong> Free Demo</strong></li>
<li><strong><asp:HyperLink ID="HyperLink3" class="btn btn-success btn-outline fancy-button btn-0" runat="server">Free</asp:HyperLink></strong></li>
</ul>

    </section>
    
    <section class="third lift plan-tier callout" onclick="location.href='#';">

      <h6>Most popular</h6>
      <h4>Yearly Rent</h4>
      <h3 class = "pricefont"><sup class="superscript superText">₹​</sup><span class="plan-price">25,000</span><sub></sub></h3>

      
      <p class="early-adopter-price">Full Stock</p>
      
      

      <ul>
<li><strong>Option,Index</strong></li>
<li>Nifty Only <strong>10,000/-</strong> Per Year</li>

<li><strong><asp:HyperLink ID="HyperLink1" Target="_blank" class="btn btn-success btn-outline fancy-button btn-0" runat="server" NavigateUrl="http://finideas.com/checkout.aspx?SUQ.-PJwgwTFP5io.=Nw==-MX7rMcuKWUI=&amp;VFlQRQ..-8idGk8JSOCE.=U09GVFdBUkUtUkVOVA==-vcRyZZEsqGo=&amp;UFJJQ0U.-K51qKhMFra0.=MTE5NzA=-aPjrFg4OavE=">Buy Now</asp:HyperLink></strong></li>
</ul>

    </section>
    
    <section class="third lift plan-tier" onclick="location.href='#';">

      
      <h4>Life Time</h4>
      <h3 class = "pricefont"><sup class="superscript superText">₹​</sup><span class="plan-price">50,000</span><sub></sub></h3>

      
      <p class="early-adopter-price">Full Stock</p>
      
      

      <ul>
<li><strong>Option,Index</strong></li>
<li>Nifty Only <strong>20,000/-</strong> Per Year</li>
<li><strong><asp:HyperLink ID="HyperLink2" Target="_blank" class="btn btn-success btn-outline fancy-button btn-0" runat="server" NavigateUrl="http://finideas.com/checkout.aspx?SUQ.-PJwgwTFP5io.=Nw==-MX7rMcuKWUI=&VFlQRQ..-8idGk8JSOCE.=U09GVFdBUkUtT1VUUklHSFQ=-U4FA5UyjGWw=&UFJJQ0U.-K51qKhMFra0.=MjM5NDA=-B7ZzL55Y9WE=">Buy Now</asp:HyperLink></strong></li>


</ul>

    </section>
    
    <div style="clear: both"></div>
  </section>
 <div id="flip">* Terms & Condition</div>
<div id="panel"><ul class="normal-list" style="display: block;line-height: 1.7em;text-align: left;">

        <li>* At the time of outright purchase, 20% AMC will be charged from second year.
        </li>
        <li>* Service Tax &amp; VAT extra. </li>
        <li>* The first level of support and debugging of the software will be provided through
            internet based remote software. </li>
        <li>* All payments to be made by A/c Payee Cheque/D.D. in favour of “Finideas Management
            Solutions Pvt. Ltd.” </li>
        <li>* Bank details for fund transfer:
            <br>
           
                <b>&#65279;Account Name : </b>FinIdeas Management Solutions Pvt. Ltd.
                <br>
                <b>Account No. : </b>02512560022554
                <br>
                <b>&#65279;Banker : </b>HDFC Bank Ltd.
                <br>
                <b>Branch : </b>Udhana Darwaja, Surat
                <br>
                <b>IFSC CODE : </b>HDFC0000251
            
        </li>
    </ul></div>
        </div>
    </div>

        <script>

                 // Back Button Disable
                 window.history.forward();
                 function noBack() {
                     window.history.forward();
                 }

                 $(document).ready(function () {
                     $("#flip").click(function () {
                         $("#panel").slideToggle("slow");
                     });
                 });


                 $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?a=e&filename=aapl-ohlc.json&callback=?', function (data) {

                     // create the chart
                     Highcharts.stockChart('container', {


                         rangeSelector: {
                             selected: 1
                         },

                         title: {
                             text: 'AAPL Stock Price'
                         },

                         series: [{
                             type: 'candlestick',
                             name: 'AAPL Stock Price',
                             data: data,
                             dataGrouping: {
                                 units: [
                    [
                        'week', // unit name
                        [1] // allowed multiples
                    ], [
                        'month',
                        [1, 2, 3, 4, 6]
                    ]
                ]
                             }
                         }]
                     });
                 });

        
        </Script>

</asp:Content>

