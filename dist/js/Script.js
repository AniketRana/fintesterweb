﻿Sys.Application.add_load(function () {
    $(".context-menu-one").on("contextmenu", function (e) {
        //prevent default context menu for right click
        e.preventDefault();

        var menu = $(".menu");

        //hide menu if already shown
        menu.hide();

        //get x and y values of the click event
        var pageX = e.pageX;
        var pageY = e.pageY;

        //position menu div near mouse cliked area
        menu.css({ top: pageY, left: pageX });

        

        var mwidth = menu.width();
        var mheight = menu.height();
        var screenWidth = $(window).width();
        var screenHeight = $(window).height();

        //if window is scrolled
        var scrTop = $(window).scrollTop();

        //if the menu is close to right edge of the window
        if (pageX + mwidth > screenWidth) {
            menu.css({ left: pageX - mwidth });
        }

        //if the menu is close to bottom edge of the window
        if (pageY + mheight > screenHeight + scrTop) {
            menu.css({ top: pageY - mheight });
        }


        //finally show the menu
        menu.show();
    });

    $("html").on("click", function () {
        $(".menu").hide();
    });

    function clean_formatted_data(str) {
        return parseFloat(str.replace(/([%,$,\,])+/g, ''));
    }

    function col_to_array(tbl_col, target) {
        // Returns column `n` (zero indexed) in table id `target` as an array 

        var colArray = $('#' + target + ' td:nth-child(' + tbl_col + ')').map(function () {
            return clean_formatted_data($(this).text());
        }).get();

        return colArray;
    }

    //------ new schtuff ------------------------//

    function get_pos_of_max(col_data) { return $.inArray(Math.max.apply(Math, col_data), col_data) }

    function generate_opacities(col_data, max) {
        var opacity_array = [];
        var increment = max / (col_data.length);

        for (i = col_data.length; i >= 1; i--) {
            opacity_array.push(i * increment / 90);
        }
     
        return opacity_array;
    }


    function process_col_best_performing(tbl_col, target) {
        var col_data = col_to_array(tbl_col, target);
        var opacity_array = generate_opacities(col_data, 65);
        var row_count = col_data.length;
        for (var i = 1; i <= row_count; i++) {
            $('#' + target + ' tr:nth-child(' + (get_pos_of_max(col_data) + 1) + ') td:nth-child(' + tbl_col + ')').css('background', 'rgba(0,0,0,' + opacity_array[0] + ')');
            col_data[get_pos_of_max(col_data)] = null;
            opacity_array.splice(0, 1);

        }
    }

//    process_col_best_performing(2, 'tableID');
//    process_col_best_performing(1, 'tableID');
//    process_col_best_performing(3, 'tableID');
//    process_col_best_performing(4, 'tableID');
//    process_col_best_performing(5, 'tableID');
//    process_col_best_performing(6, 'tableID');
//    process_col_best_performing(7, 'tableID');
//    process_col_best_performing(8, 'tableID');
//    process_col_best_performing(9, 'tableID');
//    process_col_best_performing(10, 'tableID');
//    process_col_best_performing(11, 'tableID');
//    process_col_best_performing(12, 'tableID');
//    process_col_best_performing(13, 'tableID');
//    process_col_best_performing(14, 'tableID');
//    process_col_best_performing(15, 'tableID');
//    process_col_best_performing(16, 'tableID');
//    process_col_best_performing(17, 'tableID');
//    process_col_best_performing(18, 'tableID');

//    process_col_best_performing(19, 'tableID');
//    process_col_best_performing(20, 'tableID');
//    process_col_best_performing(21, 'tableID');
//    process_col_best_performing(22, 'tableID');
//    process_col_best_performing(23, 'tableID');
//    process_col_best_performing(24, 'tableID');
//    process_col_best_performing(25, 'tableID');
//    process_col_best_performing(26, 'tableID');
//    process_col_best_performing(27, 'tableID');
//    process_col_best_performing(28, 'tableID');
//    process_col_best_performing(29, 'tableID');
//    process_col_best_performing(30, 'tableID');
//    process_col_best_performing(31, 'tableID');
//    process_col_best_performing(32, 'tableID');
//    process_col_best_performing(33, 'tableID');
//    process_col_best_performing(34, 'tableID');
//    process_col_best_performing(35, 'tableID');
//    process_col_best_performing(36, 'tableID');

//    process_col_best_performing(37, 'tableID');
//    process_col_best_performing(38, 'tableID');
//    process_col_best_performing(39, 'tableID');
//    process_col_best_performing(40, 'tableID');
//    process_col_best_performing(41, 'tableID');
//    process_col_best_performing(42, 'tableID');
//    process_col_best_performing(43, 'tableID');
//    process_col_best_performing(44, 'tableID');
//    process_col_best_performing(45, 'tableID');
//    process_col_best_performing(46, 'tableID');
//    process_col_best_performing(47, 'tableID');
//    process_col_best_performing(48, 'tableID');
//    process_col_best_performing(49, 'tableID');
//    process_col_best_performing(50, 'tableID');
//    process_col_best_performing(51, 'tableID');
//    process_col_best_performing(52, 'tableID');
//    process_col_best_performing(53, 'tableID');
//    process_col_best_performing(54, 'tableID');

//    process_col_best_performing(55, 'tableID');
//    process_col_best_performing(56, 'tableID');
//    process_col_best_performing(57, 'tableID');
//    process_col_best_performing(58, 'tableID');
//    process_col_best_performing(59, 'tableID');
//    process_col_best_performing(60, 'tableID');
    
});

