﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Scenario.aspx.cs" Inherits="Scenario" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Fancy-Buttons CSS -->
    <link href="dist/css/fancy-buttons.css" rel="stylesheet" type="text/css" />
    <style>
        .selectedCell
        {
            background-color: lightblue;
        }
        .unselectedCell
        {
            background-color: white;
        }
        .heght
        {
            height: 120px;
        }
        .Calendar .ajax__calendar_body
        {
            width: 162px;
            height: 135px;
            background-color: #b5c6e0;
            color: white;
            z-index: 10;
        }
        .Calendar .ajax__calendar_header
        {
            background-color: gray;
            color: white;
        }
        .tblwidth
        {
            white-space: nowrap;
        }
        .btnApply
        {
            background-color: #516ead; /* blue */
            border: none;
            color: white;
            padding: 5px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 2px 1px;
            cursor: pointer;
            height: 35px;
            width: 80px;
            border-radius: 5px;
        }
        .btnReset
        {
            background-color: #a53b3b; /* red */
            border: none;
            color: white;
            padding: 5px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 2px 1px;
            cursor: pointer;
            height: 35px;
            width: 90px;
            border-radius: 5px;
        }
        .UpDownBtn
        {
            background-color: #516ead; /* blue */
            border: none;
            color: white;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            cursor: pointer;
            height: 30px;
            width: 20px;
            -webkit-text-fill-color: white;
            border-radius: 5px;
        }
    </style>
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <%-- <Triggers>
        <asp:PostBackTriggers ControlID="tab1" />
        <asp:PostBackTriggers ControlID="tab2" />
        <asp:PostBackTrigger ControlID ="a" />
        <asp:PostBackTrigger ControlID ="b" />

    </Triggers> --%>
        <ContentTemplate>
            <div class="page-wrapper" style="height: 915px; padding-bottom: 0px;">
                <div class="container-fluid pt-12" style="-webkit-text-fill-color: black;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pb-0" style="height: 110px;">
                                <div class="panel-wrapper collapse in">
                                    <table border="2" cellpadding="-5" height="10px" width="100%" align="center" style="margin: 0px auto;">
                                        <tr>
                                            <td align="center">
                                                CMP
                                            </td>
                                            <td align="center">
                                                <%--width="150px"--%>
                                                Call Vol
                                            </td>
                                            <td align="center">
                                                Put Vol
                                            </td>
                                            <td align="center">
                                                Vol
                                            </td>
                                            <td align="center">
                                                StartDate
                                            </td>
                                            <td align="center">
                                                ExpDate
                                            </td>
                                            <td align="center">
                                                Interval
                                            </td>
                                            <td align="center">
                                                UpDown
                                            </td>
                                            <td align="center">
                                                Middle
                                            </td>
                                            <td align="center">
                                                Name
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnApplyNow" runat="server" Text="Apply Now" class="btnApply" Style="-webkit-text-fill-color: white;"
                                                    OnClick="btnApplyNow_Click" />
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnReset" runat="server" Text="Reset Trade" class="btnReset" Style="-webkit-text-fill-color: white;"
                                                    OnClick="btnReset_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:TextBox ID="txtcmp" Height="25px" Width="55px" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <%--width="125px"--%>
                                                <asp:Button runat="server" Class="UpDownBtn" Text="-" ID="btnCallVolDown" />
                                                <asp:TextBox ID="txtCvol" Height="25px" Width="25px" runat="server"></asp:TextBox>
                                                <asp:Button runat="server" Class="UpDownBtn" Text="+" ID="btnCallVolPlus" />
                                                <cc1:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtCvol"
                                                    Width="50" RefValues="" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID="btnCallVolDown"
                                                    TargetButtonUpID="btnCallVolPlus" Minimum="1" Maximum="100">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                            <td align="center">
                                                <%--width="125px" --%>
                                                <asp:Button runat="server" Class="UpDownBtn" Text="-" ID="btnPutVolDown" />
                                                <asp:TextBox ID="txtPvol" Height="25px" Width="25px" runat="server"></asp:TextBox>
                                                <asp:Button runat="server" Class="UpDownBtn" Text="+" ID="btnPutVolPlus" />
                                                <cc1:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txtPvol"
                                                    Width="50" RefValues="" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID="btnPutVolDown"
                                                    TargetButtonUpID="btnPutVolPlus" Minimum="1" Maximum="100">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                            <td align="center">
                                                <%--width="125px"--%>
                                                <asp:Button runat="server" Class="UpDownBtn" Text="-" ID="btnVolDown" />
                                                <asp:TextBox ID="txtVol" Height="25px" Width="25px" runat="server"></asp:TextBox>
                                                <asp:Button runat="server" Class="UpDownBtn" Text="+" ID="btnVolPlus" />
                                                <cc1:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtVol"
                                                    Width="50" RefValues="" ServiceDownMethod="" ServiceUpMethod="" TargetButtonDownID="btnVolDown"
                                                    TargetButtonUpID="btnVolPlus" Minimum="1" Maximum="100">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtStartDt" Height="25px" Width="85px" runat="server"></asp:TextBox>
                                                <cc1:CalendarExtender ID="calenderStartDt" PopupButtonID="txtStartDt" runat="server"
                                                    TargetControlID="txtStartDt" Format="dd-MMM-yyyy" PopupPosition="BottomRight"
                                                    ClientIDMode="Inherit" Animated="False" CssClass="Calendar">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtExpDt" Height="25px" Width="85px" runat="server"></asp:TextBox>
                                                <cc1:CalendarExtender ID="calenderEndDt" PopupButtonID="txtExpDt" runat="server"
                                                    TargetControlID="txtExpDt" Format="dd-MMM-yyyy" PopupPosition="BottomRight" ClientIDMode="Inherit"
                                                    Animated="False" CssClass="Calendar">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td align="center">
                                                <asp:CheckBox ID="chkInterval" runat="server" />
                                                <asp:TextBox ID="txtInterval" Height="25px" Width="30px" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtUpDownStrike" Height="25px" Width="50px" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtMstrike" Height="25px" Width="60px" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="txtScenarioName" Height="25px" Width="60px" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                <%--class="btn btn-danger btn-rounded"--%>
                                                <asp:Button ID="BtnAdd" runat="server" Text="Add" class="btnApply" OnClick="BtnAdd_Click"
                                                    Style="-webkit-text-fill-color: white;" />
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnResult" class="btnApply" runat="server" Text="Result" Style="-webkit-text-fill-color: white;"
                                                    OnClick="btnResult_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="padding-left: 15px; height: 580px; padding-bottom: 0px;">
                            <div class="panel panel-default card-view pb-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" style="padding-bottom: 0px;">
                                        <div class="tab-struct custom-tab-2  mt-0">
                                            <%--In Change The Class Format Wil Be Change --%>
                                            <ul role="tablist" class="nav nav-tabs" id="Ul2">
                                                <li role="presentation" class="active"><a data-toggle="tab" id="A1" role="tab" href="#Trade"
                                                    aria-expanded="true">Trade</a></li>
                                                <li role="presentation" class=""><a data-toggle="tab" id="A2" role="tab" href="#Chart"
                                                    aria-expanded="false">Chart</a></li>
                                            </ul>
                                            <div class="tab-content" id="Div1">
                                                <div id="Trade" class="tab-pane fade active in" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-10">
                                                            <div class="panel panel-default card-view" style="height: 590px; margin-bottom: -5px;">
                                                                <%--style="box-shadow:0px 0px 10px black;"--%>
                                                                <div class="panel-wrapper collapse in" style="margin-bottom: -5px;">
                                                                    <div class="panel-body">
                                                                        <p class="text-muted">
                                                                            <%--class="btn  btn-primary btn-rounded" --%>
                                                                        </p>
                                                                        <div style="height: 550px; width: 100%; overflow: scroll;">
                                                                            <%--<div class="table-wrap mt-00">
                                                                            <div class="table-responsive tblwidth">--%>
                                                                            <%--class="table table-striped table-bordered mb-0"--%>
                                                                            <asp:GridView ID="grvScenario" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                OnRowUpdating="grvScenario_RowUpdating" class="table table-striped table-bordered mb-0"
                                                                                OnRowCancelingEdit="grvScenario_RowCancelingEdit" OnRowEditing="grvScenario_RowEditing"
                                                                                OnRowDataBound="grvScenario_RowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="#">
                                                                                        <HeaderStyle Width="50%" />
                                                                                        <ItemStyle Width="50%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="btnDel" ImageUrl="Images/delete.png" runat="server" CommandArgument='<%# Eval("ID")%>'
                                                                                                CausesValidation="false" ForeColor="Black" Height="18px" Width="18px" OnCommand="btnDel_Command" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                        <HeaderStyle Width="50%" />
                                                                                        <ItemStyle Width="50%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="btnEdit" ImageUrl="Images/edi.png" runat="server" CommandName="Edit"
                                                                                                CausesValidation="false" ForeColor="Black" Height="18px" Width="18px" />
                                                                                            <br />
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update"
                                                                                                ValidationGroup="evalid" CausesValidation="True" ForeColor="Black" />
                                                                                            <%--  <br />--%>
                                                                                            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel"
                                                                                                CausesValidation="false" ForeColor="Black" />
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ID">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Start_Date">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblStartDt" runat="server" DataFormatString="{0:dd/MMM/yyyy}" Text='<%#Eval("StartDate","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="GtxtStartDt" runat="server" DataFormatString="{0:dd/MM/yyyy}" Height="30px"
                                                                                                Width="100px" Text='<%#Eval("StartDate","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                                                            <cc1:CalendarExtender ID="calenderStartDt" PopupButtonID="GtxtStartDt" runat="server"
                                                                                                TargetControlID="GtxtStartDt" Format="dd/MMM/yyyy" PopupPosition="Right" ClientIDMode="Inherit"
                                                                                                Animated="False" CssClass="Calendar">
                                                                                            </cc1:CalendarExtender>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Expiry_Date">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblExpDt" runat="server" DataFormatString="{0:dd/MMM/yyyy}" Text='<%#Eval("ExpiryDate","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <%--<asp:TextBox ID="txtExpDt" Height="25px" Width="100px" runat="server"></asp:TextBox>--%>
                                                                                            <asp:TextBox ID="GtxtExpDt" runat="server" DataFormatString="{0:dd/MM/yyyy}" Height="30px"
                                                                                                Width="100px" Text='<%#Eval("ExpiryDate","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                                                            <cc1:CalendarExtender ID="calenderEndDt" PopupButtonID="GtxtExpDt" runat="server"
                                                                                                TargetControlID="GtxtExpDt" Format="dd/MMM/yyyy" PopupPosition="Right" ClientIDMode="Inherit"
                                                                                                Animated="False" CssClass="Calendar">
                                                                                            </cc1:CalendarExtender>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="C/P/F">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCPF" runat="server" Text='<%#Eval("CPF") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:DropDownList ID="ddlGtxtCPF" Height="30px" SelectedValue='<%# Eval("CPF") %>'
                                                                                                Width="40px" runat="server" Style="color: Black">
                                                                                                <asp:ListItem>C</asp:ListItem>
                                                                                                <asp:ListItem>P</asp:ListItem>
                                                                                                <asp:ListItem>F</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <%--<asp:TextBox ID="GtxtCPF" runat="server" Height="30px" Width="50px" Text='<%#Eval("CPF") %>'></asp:TextBox>--%>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Underlying">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblUnderlying" runat="server" Text='<%#Eval("Underlying") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="GtxtUnderlying" runat="server" Height="30px" Width="70px" Text='<%#Eval("Underlying") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Strike">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblStrike" runat="server" Text='<%#Eval("Strike") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="GtxtStrike" runat="server" Height="30px" Width="70px" Text='<%#Eval("Strike") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Qty">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblQty" runat="server" Text='<%#Eval("Qty") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="GtxtQty" runat="server" Height="30px" Width="50px" Text='<%#Eval("Qty") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Rate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblRate" runat="server" Text='<%#Eval("Rate") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="GtxtRate" runat="server" Height="30px" Width="70px" Text='<%#Eval("Rate") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Vol">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVol" runat="server" Text='<%# Eval("Vol") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="GtxtVol" runat="server" Height="30px" Width="50px" Text='<%#Eval("Vol") %>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Ltp">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblLtp" runat="server" Text='<%#Eval("Ltp","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Delta">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDelta" runat="server" Text='<%#Eval("Delta","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="DeltaValue">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDeltaValue" runat="server" Text='<%#Eval("DeltaValue","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Gamma">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGamma" runat="server" Text='<%#Eval("Gamma","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="GammaValue">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGammaValue" runat="server" Text='<%#Eval("GammaValue","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Vega">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVega" runat="server" Text='<%#Eval("Vega","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="VegaValue">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVegaValue" runat="server" Text='<%#Eval("VegaValue","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Theta">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTheta" runat="server" Text='<%#Eval("Theta","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ThetaValue">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblThetaValue" runat="server" Text='<%#Eval("ThetaValue","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Volga">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVolga" runat="server" Text='<%#Eval("Volga","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="VolgaValue">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVolgaValue" runat="server" Text='<%#Eval("VolgaValue","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Vanna">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVanna" runat="server" Text='<%#Eval("Vanna","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="VannaValue">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblVannaValue" runat="server" Text='<%#Eval("VannaValue","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Diff. Factor">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDiffFactor" runat="server" Text='<%#Eval("DiffFactor","{0:0.00000}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                            <%--  </div>
                                                                        </div>--%>
                                                                        </div>
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <div class="panel panel-default card-view" style="padding-left: 0px;">
                                                                <%--style="box-shadow:0px 0px 10px black;"--%>
                                                                <div class="panel-wrapper collapse in" style="margin-bottom: -5px;">
                                                                    <div class="panel-body">
                                                                        <p class="text-muted">
                                                                            <%--class="btn  btn-primary btn-rounded" --%>
                                                                        </p>
                                                                        <div class="table-wrap mt-00">
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        Profit&Loss
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtProfitLoss"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Delta
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtdelta"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Gamma
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtGamma"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Vega
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtVega"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Theta
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtTheta"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Volga
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtVolga"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        Vanna
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox Width="95px" Enabled="false" Style="text-align: right;" ID="txtVanna"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--   <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default card-view heght pb-0">
                                        <div class="panel-wrapper collapse in">
                                            <table id="table1" class="table mb-0">
                                                <thead class="tbl-header">
                                                    <tr align="center" style="color: Black;">
                                                        <td>
                                                            Profit & Loss
                                                        </td>
                                                        <td>
                                                            Delta
                                                        </td>
                                                        <td>
                                                            Gamma
                                                        </td>
                                                        <td>
                                                            Vega
                                                        </td>
                                                        <td>
                                                            Theta
                                                        </td>
                                                        <td>
                                                            Volga
                                                        </td>
                                                        <td>
                                                            Vanna
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbl-content" align="center">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtProfitLoss" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtdelta" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtGamma" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtVega" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtTheta" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtVolga" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox Width="100px" Enabled="false" ID="txtVanna" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                                                </div>
                                                <div id="Chart" class="tab-pane fade " role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="panel panel-default card-view" style="margin-bottom: -18px;">
                                                                <div class="panel-wrapper collapse in" style="margin-bottom: -10px; height: 54px;
                                                                    border-bottom-width: 3px; padding-bottom: 0px;">
                                                                    <div class="panel-body" style="padding-bottom: 0px;">
                                                                        <p class="text-muted">
                                                                        </p>
                                                                        <div class="table-wrap mt-00">
                                                                            <div class="table-responsive tblwidth">
                                                                                <asp:CheckBoxList ID="chklistDt" class="checkbox checkbox-success" runat="server"
                                                                                    RepeatDirection="Horizontal">
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-sm-12" style="height: 122px;">
                                                            <div class="panel panel-default card-view pb-0">
                                                                <div class="panel-wrapper collapse in">
                                                                    <div class="panel-body">
                                                                        <div class="tab-struct custom-tab-1  mt-0">
                                                                            <%--In Change The Class Format Wil Be Change --%>
                                                                            <ul role="tablist" class="nav nav-tabs" id="myTabs_7">
                                                                                <li class="active" role="presentation"><a aria-expanded="true" data-toggle="tab"
                                                                                    role="tab" id="home_tab_7" href="#ProLoss">Profit & Loss</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="3" role="tab" href="#Delta"
                                                                                    aria-expanded="false">Delta</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="5" role="tab" href="#Gamma"
                                                                                    aria-expanded="false">Gamma </a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="7" role="tab" href="#Vega"
                                                                                    aria-expanded="false">Vega </a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="9" role="tab" href="#Theta"
                                                                                    aria-expanded="false">Theta </a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="11" role="tab" href="#Volga"
                                                                                    aria-expanded="false">Volga </a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="13" role="tab" href="#Vanna"
                                                                                    aria-expanded="false">Vanna</a></li>
                                                                            </ul>
                                                                            <div class="tab-content" id="myTabContent_7">
                                                                                <div id="ProLoss" class="tab-pane fade active in" role="tabpanel">
                                                                                    <asp:GridView ID="grdprofit" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <div id="Delta" class="tab-pane fade" role="tabpanel">
                                                                                    <asp:GridView ID="grddelta" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <div id="Gamma" class="tab-pane fade" role="tabpanel">
                                                                                    <asp:GridView ID="grdgamma" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <div id="Vega" class="tab-pane fade" role="tabpanel">
                                                                                    <asp:GridView ID="grdvega" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <div id="Theta" class="tab-pane fade" role="tabpanel">
                                                                                    <asp:GridView ID="grdtheta" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <div id="Volga" class="tab-pane fade" role="tabpanel">
                                                                                    <asp:GridView ID="grdvolga" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                                <div id="Vanna" class="tab-pane fade" role="tabpanel">
                                                                                    <asp:GridView ID="grdvanna" runat="server">
                                                                                    </asp:GridView>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="panel panel-default card-view pb-0">
                                                                <div class="panel-wrapper collapse in">
                                                                    <div class="panel-body">
                                                                        <div class="tab-struct custom-tab-1  mt-0">
                                                                            <%--In Change The Class Format Wil Be Change --%>
                                                                            <ul role="tablist" class="nav nav-tabs" id="Ul1">
                                                                                <li role="presentation" class="active"><a data-toggle="tab" id="2" role="tab" href="#PLvalue"
                                                                                    aria-expanded="true">P & L Value</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="4" role="tab" href="#DeltaValue"
                                                                                    aria-expanded="false">Delta Value</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="6" role="tab" href="#GammaValue"
                                                                                    aria-expanded="false">Gamma Value</a></li>
                                                                                <li class="" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab"
                                                                                    id="A1" href="#VegaValue">Vega Value</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="10" role="tab" href="#ThetaValue"
                                                                                    aria-expanded="false">Theta Value</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="12" role="tab" href="#VolgaValue"
                                                                                    aria-expanded="false">Volga Value</a></li>
                                                                                <li role="presentation" class=""><a data-toggle="tab" id="14" role="tab" href="#VannaValue"
                                                                                    aria-expanded="false">Vanna Value</a></li>
                                                                            </ul>
                                                                            <div class="tab-content" id="Ul1">
                                                                                <div id="VegaValue" class="tab-pane fade active in" role="tabpanel">
                                                                                    <p>
                                                                                        Vega Value</p>
                                                                                </div>
                                                                                <div id="ThetaValue" class="tab-pane fade" role="tabpanel">
                                                                                    <p>
                                                                                        Theta Value</p>
                                                                                </div>
                                                                                <div id="VolgaValue" class="tab-pane fade" role="tabpanel">
                                                                                    <p>
                                                                                        Volga Value</p>
                                                                                </div>
                                                                                <div id="VannaValue" class="tab-pane fade" role="tabpanel">
                                                                                    <p>
                                                                                        Vanna Value</p>
                                                                                </div>
                                                                                <div id="PLvalue" class="tab-pane fade" role="tabpanel">
                                                                                    <p>
                                                                                        Profit & Loss Value</p>
                                                                                </div>
                                                                                <div id="DeltaValue" class="tab-pane fade" role="tabpanel">
                                                                                    <p>
                                                                                        Delta Value</p>
                                                                                </div>
                                                                                <div id="GammaValue" class="tab-pane fade" role="tabpanel">
                                                                                    <p>
                                                                                        Gamma Value</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>

        // Back Button Disable
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
        $(document).ready(function () {
            $(".tabs-menu a").click(function (event) {
                event.preventDefault();
                $(this).parent().addClass("current");
                $(this).parent().siblings().removeClass("current");
                var tab = $(this).attr("href");
                $(".tab-content").not(tab).css("display", "none");
                $(tab).fadeIn();
            });
        });

        $(document).ready(function () {
            $('#grvScenario td').hover(function () {
                $(this).addClass('selectedCell');
            }, function () { $(this).removeClass('selectedCell'); });
        });

        $(function () {
            $(document).on('click', '#grvScenario tr', function () {
                $("#grvScenario tr").removeClass('selectCell');
                $(this).addClass('selectCell');
            });
        })
    </script>
</asp:Content>
