﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;


public partial class Trends : System.Web.UI.Page
{
    DataSet ds = new DataSet();
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    string sql;
    String strEmail = "";
    string curMonth;
    string NextMonth;
    string FarMonth;
    string F1, F10;
    double atm;
    DataSet ds1 = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();
    DataTable dtField = new DataTable();

    string UserId;
    string pgNext;
    string pgPrev;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();
        ds1 = objbal.ChkKey(UserId);
        objbol.CheckKey = ds1.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }

        strEmail = Session["UserEmail"].ToString();

        if (grvTrends.Rows.Count > 0)
        {
            btnExpToExl.Visible = true;
        }
        else
        {
            btnExpToExl.Visible = false;
        }

        if (!IsPostBack)
        {
            ddlTrend.DataBind();
            ddlCompany.DataBind();
            ddlCompany.SelectedValue = "NIFTY100";
            //ddlTrend.SelectedValue = "";
            //ddCpf.SelectedValue = "";
            //ddlRounding.SelectedValue = "";

            sql = "select Convert(varchar,MAX(BUSINESS_DATE),106) as MaxDt from OptionCE_" + ddlCompany.SelectedValue + "";
            SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            txtBdate.Text = ds.Tables[0].Rows[0]["MaxDt"].ToString();
            txtBdate_TextChanged(sender, e);
            //ddlExpiry_SelectedIndexChanged(sender, e);
        }

      
    }

    private double Val(object obj)
    {
        try
        {
            return Convert.ToDouble(obj);
        }
        catch (Exception)
        {
            return 0;
            //throw;
        }
    }

    protected void grvTrends_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1)
        {

        }
        else
        {
            TableCell cell0 = e.Row.Cells[0];
            TableCell cell1 = e.Row.Cells[1];
            TableCell cell2 = e.Row.Cells[2];
            TableCell cell3 = e.Row.Cells[3];
            TableCell cell4 = e.Row.Cells[4];
            TableCell cell5 = e.Row.Cells[5];
            TableCell cell6 = e.Row.Cells[6];
            TableCell cell7 = e.Row.Cells[7];
            TableCell cell8 = e.Row.Cells[8];
            TableCell cell9 = e.Row.Cells[9];
            TableCell cell10 = e.Row.Cells[10];
            Int16 r = Convert.ToInt16(ddlRounding.SelectedValue);

            #region Rounding
            // 0 Column Rounding // Strike Column
            double c0 = Math.Round(Convert.ToDouble(cell0.Text), 2);
            cell0.Text = ((c0)).ToString("N" + 2 + "");

            // 1 Column Rounding
            if (cell1.Text == null || cell1.Text == "0" || cell1.Text == "0.0" || cell1.Text == "&nbsp;")
            {
                cell1.Text = Convert.ToDouble("0").ToString();
                if (cell1.Text == Convert.ToString(0))
                {
                    cell1.Text = "0";
                }
            }
            else
            {
                double c1 = Math.Round(Convert.ToDouble(cell1.Text), r);
                cell1.Text = ((c1)).ToString("N" + r + "");
            }

            // 2 Column Rounding
            if (cell2.Text == null || cell2.Text == "0" || cell2.Text == "0.0" || cell2.Text == "&nbsp;")
            {
                cell2.Text = Convert.ToDouble("0").ToString();
                if (cell2.Text == Convert.ToString(0))
                {
                    cell2.Text = "0";
                }
            }
            else
            {
                double c2 = Math.Round(Convert.ToDouble(cell2.Text), r);
                cell2.Text = ((c2)).ToString("N" + r + "");
            }

            // 3 Column Rounding
            if (cell3.Text == null || cell3.Text == "0" || cell3.Text == "0.0" || cell3.Text == "&nbsp;")
            {
                cell3.Text = Convert.ToDouble("0").ToString();
                if (cell3.Text == Convert.ToString(0))
                {
                    cell3.Text = "0";
                }
            }
            else
            {
                double c3 = Math.Round(Convert.ToDouble(cell3.Text), 1);
                cell3.Text = ((c3)).ToString("N" + r + "");
            }

            // 4 Column Rounding
            if (cell4.Text == null || cell4.Text == "0" || cell4.Text == "0.0" || cell4.Text == "&nbsp;")
            {
                cell4.Text = Convert.ToDouble("0").ToString();
                if (cell4.Text == Convert.ToString(0))
                {
                    cell4.Text = "0";
                }
            }
            else
            {
                double c4 = Math.Round(Convert.ToDouble(cell4.Text), r);
                cell4.Text = ((c4)).ToString("N" + r + "");
            }

            // 5 Column Rounding
            if (cell5.Text == null || cell5.Text == "0" || cell5.Text == "0.0" || cell5.Text == "&nbsp;")
            {
                cell5.Text = Convert.ToDouble("0").ToString();
                if (cell5.Text == Convert.ToString(0))
                {
                    cell5.Text = "0";
                }
            }
            else
            {
                double c5 = Math.Round(Convert.ToDouble(cell5.Text), r);
                cell5.Text = ((c5)).ToString("N" + r + "");
            }

            // 6 Column Rounding
            if (cell6.Text == null || cell6.Text == "0" || cell6.Text == "0.0" || cell6.Text == "&nbsp;")
            {
                cell6.Text = Convert.ToDouble("0").ToString();
                if (cell6.Text == Convert.ToString(0))
                {
                    cell6.Text = "0";
                }
            }
            else
            {
                double c6 = Math.Round(Convert.ToDouble(cell6.Text), r);
                cell6.Text = ((c6)).ToString("N" + r + "");
            }

            // 7 Column Rounding
            if (cell7.Text == null || cell7.Text == "0" || cell7.Text == "0.0" || cell7.Text == "&nbsp;")
            {
                cell7.Text = Convert.ToDouble("0").ToString();
                if (cell7.Text == Convert.ToString(0))
                {
                    cell7.Text = "0";
                }
            }
            else
            {
                double c7 = Math.Round(Convert.ToDouble(cell7.Text), r);
                cell7.Text = ((c7)).ToString("N" + r + "");
            }

            // 8 Column Rounding
            if (cell8.Text == null || cell8.Text == "0" || cell8.Text == "0.0" || cell8.Text == "&nbsp;")
            {
                cell8.Text = Convert.ToDouble("0").ToString();
                if (cell8.Text == Convert.ToString(0))
                {
                    cell8.Text = "0";
                }
            }
            else
            {
                double c8 = Math.Round(Convert.ToDouble(cell8.Text), r);
                cell8.Text = ((c8)).ToString("N" + r + "");
            }

            // 9 Column Rounding
            if (cell9.Text == null || cell9.Text == "0" || cell9.Text == "0.0" || cell9.Text == "&nbsp;")
            {
                cell9.Text = Convert.ToDouble("0").ToString();
                if (cell9.Text == Convert.ToString(0))
                {
                    cell9.Text = "0";
                }
            }
            else
            {
                double c9 = Math.Round(Convert.ToDouble(cell9.Text), r);
                cell9.Text = ((c9)).ToString("N" + r + "");
            }

            // 10 Column Rounding
            if (cell10.Text == null || cell10.Text == "0" || cell10.Text == "0.0" || cell10.Text == "&nbsp;")
            {
                cell10.Text = Convert.ToDouble("0").ToString();
                if (cell10.Text == Convert.ToString(0))
                {
                    cell10.Text = "0";
                }
            }
            else
            {
                double c10 = Math.Round(Convert.ToDouble(cell10.Text), r);
                cell10.Text = ((c10)).ToString("N" + r + "");
            }

            #endregion

            #region Color

            double Strike = Convert.ToDouble (e.Row.Cells[0].Text);

            // Dynamic Add Color Value Wise Code 
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Color colGreen;

                colGreen = Color.FromArgb(178, 255, 228);
                Color ColRed;
                ColRed = Color.FromArgb(242, 205, 206);
                
                //1
                cell0.Font.Bold = true;
                cell0.ForeColor = Color.Black;

                //2
                if (Strike == atm)
                {
                    e.Row.BackColor = Color.FromArgb(186, 210, 244);
                    e.Row.ForeColor = Color.Black;
                    e.Row.Font.Bold = true;
                   
                    //e.Row.CssClass = "ATM";
                }
                else
                {
                    cell1.BackColor = colGreen;
                    cell1.ForeColor = Color.Black;

                    if (Val(cell3.Text) > Val(cell2.Text))
                    {
                        cell2.BackColor = colGreen;  // Chartreuse , GreenYellow , LawnGreen
                        cell2.ForeColor = Color.Black;

                    }
                    else 
                    {
                        cell2.BackColor = ColRed;  // Tomato
                        cell2.ForeColor = Color.Black;
                    }

                    //3
                    if (Val(cell3.Text) > Val(cell2.Text))
                    {
                        cell3.BackColor = colGreen;
                        cell3.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell3.BackColor = ColRed;
                        cell3.ForeColor = Color.Black;
                    }

                    //4
                    if (Val(cell4.Text) > Val(cell3.Text))
                    {
                        cell4.BackColor = colGreen;
                        cell4.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell4.BackColor = ColRed;
                        cell4.ForeColor = Color.Black;
                    }


                    //5
                    if (Val(cell5.Text) > Val(cell4.Text))
                    {
                        cell5.BackColor = colGreen;
                        cell5.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell5.BackColor = ColRed;
                        cell5.ForeColor = Color.Black;
                    }

                    //6
                    if (Val(cell6.Text) > Val(cell5.Text))
                    {
                        cell6.BackColor = colGreen;
                        cell6.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell6.BackColor = ColRed;
                        cell6.ForeColor = Color.Black;
                    }


                    //7
                    if (Val(cell7.Text) > Val(cell6.Text))
                    {
                        cell7.BackColor = colGreen;
                        cell7.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell7.BackColor = ColRed;
                        cell7.ForeColor = Color.Black;
                    }
                    //8
                    if (Val(cell8.Text) > Val(cell7.Text))
                    {
                        cell8.BackColor = colGreen;
                        cell8.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell8.BackColor = ColRed;
                        cell8.ForeColor = Color.Black;
                    }
                    //9
                    if (Val(cell9.Text) > Val(cell8.Text))
                    {
                        cell9.BackColor = colGreen;
                        cell9.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell9.BackColor = ColRed;
                        cell9.ForeColor = Color.Black;
                    }

                    //10
                    if (Val(cell10.Text) > Val(cell9.Text))
                    {
                        cell10.BackColor = colGreen;
                        cell10.ForeColor = Color.Black;
                    }
                    else 
                    {
                        cell10.BackColor = ColRed;
                        cell10.ForeColor = Color.Black;
                    }
                }
            }
            #endregion

        }
    }

    protected void grvTrends_DataBound(object sender, EventArgs e)
    {

    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        if (cnnstr.State == ConnectionState.Closed)
        {
            cnnstr.Open();
        }
        DataSet ds = new DataSet();
        sql = "select top 3 (convert(varchar,EXPIRY_DATE,106)) from [Option" + ddCpf.SelectedValue + "_" + ddlCompany.SelectedValue + "] where BUSINESS_DATE = '" + txtBdate.Text + "' group by EXPIRY_DATE";
        SqlDataAdapter adep = new SqlDataAdapter(sql, cnnstr);
        adep.Fill(ds);
        cnnstr.Close();

        if (ds.Tables[0].Rows.Count > 0)
        {
            curMonth = Convert.ToDateTime(ds.Tables[0].Rows[0][0].ToString()).ToString("dd MMM yyyy");
            NextMonth = Convert.ToDateTime(ds.Tables[0].Rows[1][0].ToString()).ToString("dd MMM yyyy");
            FarMonth = Convert.ToDateTime(ds.Tables[0].Rows[2][0].ToString()).ToString("dd MMM yyyy");
        }
        else
        {
            lblError.Text = "Selected Date Not Found Please Select Another ...!";
        }

        DataSet ds1 = new DataSet();
        SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
        sadp.Fill(ds1);

        if (lblPg.Text == "Page No : 1")
        {
            return;
        }
        //delete Current page Data
        cnnstr.Open();
        sql = "delete from tblPage where PgNo =(Select MAX(PgNo) from tblPage where Email = '" + UserId + "') and Email = '" + UserId + "'";
        SqlCommand cmDelPg = new SqlCommand(sql, cnnstr);
        cmDelPg.ExecuteNonQuery();
        cnnstr.Close();

        //Get Prev Date From The tblPage
        cnnstr.Open();
        sql = "select * from tblPage where PgNo =(Select Max(PgNo) as MaxPg from tblPage  where Email = '" + UserId + "')and Email = '" + UserId + "'";
        DataSet dsprev = new DataSet();
        SqlDataAdapter adpprev = new SqlDataAdapter(sql, cnnstr);
        adpprev.Fill(dsprev);
        cnnstr.Close();

        if (dsprev.Tables[0].Rows.Count >= 1)
        {
            lblPg.Text = "Page No : " + dsprev.Tables[0].Rows[0]["PgNo"].ToString();
            pgPrev = dsprev.Tables[0].Rows[0]["PrevDate"].ToString();
            pgPrev = pgPrev.Replace("[", "");
            pgPrev = pgPrev.Replace("]", "");
        }
        if (pgPrev == null)
        {
            return;
        }

        cnnstr.Open();
        SqlCommand sqlcmd = new SqlCommand();
        sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + pgPrev + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "'", cnnstr);
        sqlcmd.ExecuteNonQuery();
        DataSet dsa = new DataSet();
        SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
        sda.Fill(dsa);

        GetAtmStrike();

        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
        sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' order by strike ";  //strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10

        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(dt);

        if (dt.Rows.Count >= 1)
        {
            lblError.Visible = false;
        }
        else
        {
            lblError.Visible = true;
        }

        grvTrends.DataSource = dt;
        grvTrends.DataBind();

        SqlDataAdapter dadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
        DataSet dsheader = new DataSet();
        dadp.Fill(dsheader);

        grvTrendsHeader.DataSource = dsheader;
        grvTrendsHeader.DataBind();
        cnnstr.Close();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (cnnstr.State == ConnectionState.Closed)
        {
            cnnstr.Open();
        }
        DataSet ds = new DataSet();

        sql = "select top 3 (convert(varchar,EXPIRY_DATE,106)) from [Option" + ddCpf.SelectedValue + "_" + ddlCompany.SelectedValue + "] where BUSINESS_DATE = '" + txtBdate.Text + "' group by EXPIRY_DATE";
        SqlDataAdapter adep = new SqlDataAdapter(sql, cnnstr);
        adep.Fill(ds);

        if (ds.Tables[0].Rows.Count > 0)
        {
            curMonth = Convert.ToDateTime(ds.Tables[0].Rows[0][0].ToString()).ToString("dd MMM yyyy");
            NextMonth = Convert.ToDateTime(ds.Tables[0].Rows[1][0].ToString()).ToString("dd MMM yyyy");
            FarMonth = Convert.ToDateTime(ds.Tables[0].Rows[2][0].ToString()).ToString("dd MMM yyyy");
        }
        else
        {
            lblError.Text = "Selected Date Not Found Please Select Another Date ...!";
        }

        DataSet ds1 = new DataSet();
        SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
        sadp.Fill(ds1);
        if (ds1.Tables[0].Rows.Count >= 1)
        {
            pgNext = ds1.Tables[0].Rows[0]["F10"].ToString();
        }
        if (pgNext == "0")
        {
            return;
        }
        pgNext = pgNext.Replace("[", "");
        pgNext = pgNext.Replace("]", "");

        SqlCommand sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + pgNext + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "'", cnnstr);
        sqlcmd.ExecuteNonQuery();

        GetAtmStrike();

        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
        sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' order by strike ";  
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);

        adp.Fill(dt);

        if (dt.Rows.Count >= 1)
        {
            lblError.Visible = false;
        }
        else
        {
            lblError.Visible = true;
        }
        grvTrends.DataSource = dt;
        grvTrends.DataBind();

        SqlDataAdapter dadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
        DataSet dsheader = new DataSet();
        dadp.Fill(dsheader);
        grvTrendsHeader.DataSource = dsheader;
        grvTrendsHeader.DataBind();
        cnnstr.Close();

        F1 = dsheader.Tables[0].Rows[0]["F1"].ToString();
        F10 = dsheader.Tables[0].Rows[0]["F10"].ToString();

        //First Get Max PageNo From The tblPage 
        cnnstr.Open();
        sql = "Select Max(PgNo) + 1 as MaxPg from tblPage where Email = '" + UserId + "'";
        DataSet dsMaxPg = new DataSet();
        SqlDataAdapter adpMaxPg = new SqlDataAdapter(sql, cnnstr);
        adpMaxPg.Fill(dsMaxPg);
        int MaxPg = Convert.ToInt16(dsMaxPg.Tables[0].Rows[0]["MaxPg"].ToString());

        //now Insert Time F1 And F10 
        sql = "insert into tblPage values(" + MaxPg + ",'" + F1 + "','" + F10 + "','" + UserId + "')";
        SqlCommand cmdIns = new SqlCommand(sql, cnnstr);
        cmdIns.ExecuteNonQuery();
        cnnstr.Close();

        //For Page Number Display 
        sql = "select * from tblPage where Email = '" + UserId + "' and PrevDate = '" + F1 + "' and NextDate ='" + F10 + "'";
        DataSet dspaging = new DataSet();
        SqlDataAdapter adpPaging = new SqlDataAdapter(sql, cnnstr);
        adpPaging.Fill(dspaging);
        lblPg.Text = "Page No : " + dspaging.Tables[0].Rows[0]["PgNo"].ToString();

    }

    protected void btnExpToExl_Click(object sender, ImageClickEventArgs e)
    {
        ExportGridToExcel();
    }

    private void ExportGridToExcel()
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string symbol = ddlCompany.SelectedValue;
        string FileName = "Trends _" + symbol + "_" + DateTime.Now + ".xls";
        StringWriter strwritter = new StringWriter();
        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        grvTrendsHeader.GridLines = GridLines.Both;
        grvTrends.GridLines = GridLines.Both;
        grvTrendsHeader.HeaderStyle.Font.Bold = true;
        grvTrends.HeaderStyle.Font.Bold = true;
        grvTrendsHeader.RenderControl(htmltextwrtter);
        grvTrends.RenderControl(htmltextwrtter);
        Response.Write(strwritter.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!(String.IsNullOrEmpty(txtBdate.Text) && String.IsNullOrEmpty(ddlExpiry.SelectedValue)))
        {
            if (cnnstr.State == ConnectionState.Closed)
            {
                cnnstr.Open();
            }

            SqlCommand sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + txtBdate.Text + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "','" + ddCpf.SelectedValue + "'", cnnstr);
            //sqlcmd.ExecuteNonQuery();
            SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
            sda.Fill(dtField);

            //Add Column For Display Data  
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
            sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' order by strike ";
            SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
            adp.Fill(dt);

            if (dt.Rows.Count < 1)
            {
                lblError.Text = "Selected Data Not Found Please Select Another ...!";
            }

            if (dt.Rows.Count >= 1)
            {
                lblError.Visible = false;
                btnExpToExl.Visible = true;
            }
            else
            {
                lblError.Visible = true;
                btnExpToExl.Visible = false;
            }

            GetAtmStrike();

            grvTrends.DataSource = dt;
            grvTrends.DataBind();

            SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
            DataSet dsheader = new DataSet();
            sadp.Fill(dsheader);

            if (dt.Rows.Count < 1)
            {
                dsheader = null;
            }

            grvTrendsHeader.DataSource = dsheader;
            grvTrendsHeader.DataBind();
            Session["state"] = "First";
            cnnstr.Close();

            txtBdate_TextChanged(sender, e);
        }
    }

    protected void ddlTrend_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!(String.IsNullOrEmpty(txtBdate.Text) && String.IsNullOrEmpty(ddlExpiry.SelectedValue)))
        {
            if (cnnstr.State == ConnectionState.Closed)
            {
                cnnstr.Open();
            }

            //curMonth = txtExpDate.Text;

            SqlCommand sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + txtBdate.Text + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "','" + ddCpf.SelectedValue + "'", cnnstr);
            //sqlcmd.ExecuteNonQuery();
            SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
            sda.Fill(dtField);

            //Display Data From Gridview 
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
            sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' order by strike ";
            SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);

            adp.Fill(dt);

            if (dt.Rows.Count < 1)
            {
                lblError.Text = "Selected Data Not Found Please Select Another ...!";
            }

            if (dt.Rows.Count >= 1)
            {
                lblError.Visible = false;
                btnExpToExl.Visible = true;
            }
            else
            {
                lblError.Visible = true;
                btnExpToExl.Visible = false;
            }

            GetAtmStrike();

            grvTrends.DataSource = dt;
            grvTrends.DataBind();

            SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
            DataSet dsheader = new DataSet();
            sadp.Fill(dsheader);

            if (dt.Rows.Count < 1)
            {
                dsheader = null;
            }

            grvTrendsHeader.DataSource = dsheader;
            grvTrendsHeader.DataBind();
            // txtExpDate.Text = curMonth;
            Session["state"] = "First";

            cnnstr.Close();
        }
    }

    protected void ddCpf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!(String.IsNullOrEmpty(txtBdate.Text) && String.IsNullOrEmpty(ddlExpiry.SelectedValue)))
        {
            if (cnnstr.State == ConnectionState.Closed)
            {
                cnnstr.Open();
            }

            // curMonth = txtExpDate.Text;

            SqlCommand sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + txtBdate.Text + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "','" + ddCpf.SelectedValue + "'", cnnstr);
            //sqlcmd.ExecuteNonQuery();
            SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
            sda.Fill(dtField);
            DataTable dt = new DataTable(); //Display Data From Gridview 
            dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
            sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' order by strike ";
            SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);

            adp.Fill(dt);

            if (dt.Rows.Count < 1)
            {
                lblError.Text = "Selected Data Not Found Please Select Another ...!";
            }

            if (dt.Rows.Count >= 1)
            {
                lblError.Visible = false;
                btnExpToExl.Visible = true;
            }
            else
            {
                lblError.Visible = true;
                btnExpToExl.Visible = false;
            }

            grvTrends.DataSource = dt;
            grvTrends.DataBind();

            SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
            DataSet dsheader = new DataSet();
            sadp.Fill(dsheader);

            if (dt.Rows.Count < 1)
            {
                dsheader = null;
            }

            grvTrendsHeader.DataSource = dsheader;
            grvTrendsHeader.DataBind();
            //  txtExpDate.Text = curMonth;
            Session["state"] = "First";

            cnnstr.Close();

        }
    }

    protected void ddlRounding_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!(String.IsNullOrEmpty(txtBdate.Text) && String.IsNullOrEmpty(ddlExpiry.SelectedValue)))
        {
            if (cnnstr.State == ConnectionState.Closed)
            {
                cnnstr.Open();
            }

            //  curMonth = txtExpDate.Text;

            SqlCommand sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + txtBdate.Text + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "','" + ddCpf.SelectedValue + "'", cnnstr);
            //sqlcmd.ExecuteNonQuery();
            SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
            sda.Fill(dtField);


            //Display Data From Gridview 
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
            sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' ";
            SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);

            adp.Fill(dt);

            if (dt.Rows.Count < 1)
            {
                lblError.Text = "Selected Data Not Found Please Select Another ...!";
            }
            if (dt.Rows.Count >= 1)
            {
                lblError.Visible = false;
                btnExpToExl.Visible = true;
            }
            else
            {
                lblError.Visible = true;
                btnExpToExl.Visible = false;
            }

            grvTrends.DataSource = dt;
            grvTrends.DataBind();

            SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
            DataSet dsheader = new DataSet();
            sadp.Fill(dsheader);

            if (dt.Rows.Count < 1)
            {
                dsheader = null;
            }

            grvTrendsHeader.DataSource = dsheader;
            grvTrendsHeader.DataBind();
            //  txtExpDate.Text = curMonth;
            Session["state"] = "First";

            cnnstr.Close();
        }
    }

    protected void txtBdate_TextChanged(object sender, EventArgs e)
    {
        cnnstr.Open();
        sql = "select Convert(varchar,Convert(datetime,EXPIRY_DATE,106),106) as EXPIRY_DATE  from OptionCE_" + ddlCompany.SelectedValue + " where BUSINESS_DATE = '" + txtBdate.Text + "' group by  EXPIRY_DATE order by Convert(Datetime,EXPIRY_DATE ) ";

        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        cnnstr.Close();

        if (ds.Tables[0].Rows.Count < 1)
        {
            lblError.Text = "Selected Data Not Found Please Select Another ...!";
        }

        ddlExpiry.DataTextField = ds.Tables[0].Columns["Expiry_Date"].ToString();
        ddlExpiry.DataSource = ds.Tables[0];
        ddlExpiry.DataBind();

        GetAtmStrike();
        ddlExpiry_SelectedIndexChanged(sender, e);
    }

    private void GetAtmStrike()
    {
        //Get Atm Strike 
        if (cnnstr.State == ConnectionState.Closed)
        {
            cnnstr.Open();
        }
        SqlDataAdapter adpAtm = new SqlDataAdapter("GetAtmStrike '" + ddlCompany.SelectedValue + "','" + txtBdate.Text + "','" + ddlExpiry.SelectedValue + "'", cnnstr);
        DataSet dsAtm = new DataSet();
        adpAtm.Fill(dsAtm);
        if (dsAtm != null)
        {
            atm = Convert.ToDouble(dsAtm.Tables[0].Rows[0]["STRIKE_PRICE"].ToString());
        }
        
        cnnstr.Close();
    }

    protected void ddlExpiry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cnnstr.State == ConnectionState.Closed)
        {
            cnnstr.Open();
        }
        SqlCommand sqlcmd = new SqlCommand("SP_Trends '" + ddlCompany.SelectedValue + "','" + txtBdate.Text + "','" + ddlExpiry.SelectedValue + "','" + ddlTrend.SelectedValue + "','" + strEmail + "','" + ddCpf.SelectedValue + "'", cnnstr);
        SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
        sda.Fill(dtField);


        //Display Data From Gridview 
        DataTable dt = new DataTable();
        dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Strike"), new DataColumn("F1"), new DataColumn("F2"), new DataColumn("F3"), new DataColumn("F4"), new DataColumn("F5"), new DataColumn("F6"), new DataColumn("F7"), new DataColumn("F8"), new DataColumn("F9"), new DataColumn("F10") });
        sql = "select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrends where Email='" + strEmail + "' order by strike";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);

        adp.Fill(dt);
        if (dt.Rows.Count < 1)
        {
            lblError.Text = "Selected Data Not Found Please Select Another ...!";
        }

        if (dt.Rows.Count >= 1)
        {
            lblError.Visible = false;
            btnExpToExl.Visible = true;
        }
        else
        {
            lblError.Visible = true;
            btnExpToExl.Visible = false;
        }

        GetAtmStrike();

        grvTrends.DataSource = dt;
        grvTrends.DataBind();

        SqlDataAdapter sadp = new SqlDataAdapter("select strike,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10 from TmpTrendsH where Email = '" + strEmail + "' ", cnnstr);
        DataSet dsheader = new DataSet();
        sadp.Fill(dsheader);

        if (dt.Rows.Count < 1)
        {
            dsheader = null;
        }

        grvTrendsHeader.DataSource = dsheader;
        grvTrendsHeader.DataBind();
        //txtExpDate.Text = curMonth;
        Session["state"] = "First";

        cnnstr.Close();

        //if(dsheader.Tables[0].Rows.Count > 1)
        if (dsheader != null)
        {
            F1 = dsheader.Tables[0].Rows[0]["F1"].ToString();
            F10 = dsheader.Tables[0].Rows[0]["F10"].ToString();

            //Delete TmpTable And Insert New F1 And F10 In TmpTable 
            cnnstr.Open();
            sql = "delete from tblPage where Email = '" + UserId + "'";
            SqlCommand cmdTmp = new SqlCommand(sql, cnnstr);
            cmdTmp.ExecuteNonQuery();

            //Now Insert F1 And F10
            sql = "insert into tblPage values(1,'" + F1 + "','" + F10 + "','" + UserId + "')";
            cmdTmp = new SqlCommand(sql, cnnstr);
            cmdTmp.ExecuteNonQuery();
            cnnstr.Close();
            lblPg.Text = "Page No : 1";
        }
    }

    protected void grvTrendsHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowIndex == -1)
        {

        }
        else
        {
            TableCell cell0 = e.Row.Cells[0];
            TableCell cell1 = e.Row.Cells[1];
            TableCell cell2 = e.Row.Cells[2];
            TableCell cell3 = e.Row.Cells[3];
            TableCell cell4 = e.Row.Cells[4];
            TableCell cell5 = e.Row.Cells[5];
            TableCell cell6 = e.Row.Cells[6];
            TableCell cell7 = e.Row.Cells[7];
            TableCell cell8 = e.Row.Cells[8];
            TableCell cell9 = e.Row.Cells[9];
            TableCell cell10 = e.Row.Cells[10];

            #region DisplayDayInHeader
            if (cell1.Text == "0")
            {

            }
            else
            {
                //Column 1
                string D1 = Convert.ToString(cell1.Text);
                string Day1, OldDt1;
                OldDt1 = cell1.Text;
                D1 = D1.TrimStart('[');
                D1 = D1.TrimEnd(']');
                DateTime dt1 = Convert.ToDateTime(D1);
                Day1 = Convert.ToString(dt1.DayOfWeek);
                cell1.Text = OldDt1 + " " + Day1;
            }
            if (cell2.Text == "0")
            {

            }
            else
            {
                //Column 2
                string D2 = Convert.ToString(cell2.Text);
                string Day2, OldDt2;
                OldDt2 = cell2.Text;
                D2 = D2.TrimStart('[');
                D2 = D2.TrimEnd(']');
                DateTime dt2 = Convert.ToDateTime(D2);
                Day2 = Convert.ToString(dt2.DayOfWeek);
                cell2.Text = OldDt2 + " " + Day2;

            }
            if (cell3.Text == "0")
            {

            }
            else
            {
                //Column 3
                string D3 = Convert.ToString(cell3.Text);
                string Day3, OldDt3;
                OldDt3 = cell3.Text;
                D3 = D3.TrimStart('[');
                D3 = D3.TrimEnd(']');
                DateTime dt3 = Convert.ToDateTime(D3);
                Day3 = Convert.ToString(dt3.DayOfWeek);
                cell3.Text = OldDt3 + " " + Day3;
            }
            if (cell4.Text == "0")
            {

            }
            else
            {
                //Column 4
                string D4 = Convert.ToString(cell4.Text);
                string Day4, OldDt4;
                OldDt4 = cell4.Text;
                D4 = D4.TrimStart('[');
                D4 = D4.TrimEnd(']');
                DateTime dt4 = Convert.ToDateTime(D4);
                Day4 = Convert.ToString(dt4.DayOfWeek);
                cell4.Text = OldDt4 + " " + Day4;
            }

            if (cell5.Text == "0")
            {

            }
            else
            {
                //Column 5
                string D5 = Convert.ToString(cell5.Text);
                string Day5, OldDt5;
                OldDt5 = cell5.Text;
                D5 = D5.TrimStart('[');
                D5 = D5.TrimEnd(']');
                DateTime dt5 = Convert.ToDateTime(D5);
                Day5 = Convert.ToString(dt5.DayOfWeek);
                cell5.Text = OldDt5 + " " + Day5;
            }
            if (cell6.Text == "0")
            {

            }
            else
            {
                //Column 6
                string D6 = Convert.ToString(cell6.Text);
                string Day6, OldDt6;
                OldDt6 = cell6.Text;
                D6 = D6.TrimStart('[');
                D6 = D6.TrimEnd(']');
                DateTime dt6 = Convert.ToDateTime(D6);
                Day6 = Convert.ToString(dt6.DayOfWeek);
                cell6.Text = OldDt6 + " " + Day6;
            }
            if (cell7.Text == "0")
            {

            }
            else
            {
                //Column 7
                string D7 = Convert.ToString(cell7.Text);
                string Day7, OldDt7;
                OldDt7 = cell7.Text;
                D7 = D7.TrimStart('[');
                D7 = D7.TrimEnd(']');
                DateTime dt7 = Convert.ToDateTime(D7);
                Day7 = Convert.ToString(dt7.DayOfWeek);
                cell7.Text = OldDt7 + " " + Day7;
            }
            if (cell8.Text == "0")
            {

            }
            else
            {
                //Column 8
                string D8 = Convert.ToString(cell8.Text);
                string Day8, OldDt8;
                OldDt8 = cell8.Text;
                D8 = D8.TrimStart('[');
                D8 = D8.TrimEnd(']');
                DateTime dt8 = Convert.ToDateTime(D8);
                Day8 = Convert.ToString(dt8.DayOfWeek);
                cell8.Text = OldDt8 + " " + Day8;
            }
            if (cell9.Text == "0")
            {

            }
            else
            {
                //Column 9
                string D9 = Convert.ToString(cell9.Text);
                string Day9, OldDt9;
                OldDt9 = cell9.Text;
                D9 = D9.TrimStart('[');
                D9 = D9.TrimEnd(']');
                DateTime dt9 = Convert.ToDateTime(D9);
                Day9 = Convert.ToString(dt9.DayOfWeek);
                cell9.Text = OldDt9 + " " + Day9;
            }
            if (cell10.Text == "0")
            {

            }
            else
            {
                //Column 10
                string D10 = Convert.ToString(cell10.Text);
                string Day10, OldDt10;
                OldDt10 = cell10.Text;
                D10 = D10.TrimStart('[');
                D10 = D10.TrimEnd(']');
                DateTime dt10 = Convert.ToDateTime(D10);
                Day10 = Convert.ToString(dt10.DayOfWeek);
                cell10.Text = OldDt10 + " " + Day10;
            }
            #endregion
        }
    }

}
