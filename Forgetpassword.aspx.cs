﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mail;

public partial class Forgetpassword : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    string password = null;
    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
            sqlcon.Open();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Login where Email='" + txtEmail.Text + "' and Expdate >= '" + DateTime.Today.ToString("dd/MMM/yyyy") + "'", sqlcon);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            sqlcon.Close();
            password = ds.Tables[0].Rows[0]["Password"].ToString();

        }
        catch (Exception)
        {
        }
        string Str = null;
        Str = "<!DOCTYPE HTML>" + "\n";
        Str = Str + "<html lang='en-US'>" + "\n";
        Str = Str + "<head>" + "\n";
        Str = Str + "<meta charset='UTF-8'>" + "\n";
        Str = Str + "<title></title>" + "\n";
        Str = Str + "</head>" + "\n";
        Str = Str + "<body>" + "\n";
        Str = Str + "<p>Dear Sir, </p>" + "\n";
        Str = Str + "<p>Greetings from FinIdeas !!!</p>" + "\n";
        Str = Str + "<p>We welcome you to the world of FinIdeas Softwares.'</p>" + "\n";
        Str = Str + "<p>Thanks for showing your keen interest in FinTester Software.</p>" + "\n";
        Str = Str + "<p>Please feel free to contact our Support Team.</p>" + "\n";
        Str = Str + "<p><B>Your User ID :</B>" + txtEmail.Text + " </p>" + "\n";
        Str = Str + "<p><B>Password :</B>" + password + " </p>" + "\n";
        Str = Str + "</body>" + "\n";
        Str = Str + "</html>" + "\n";
        
        //DemoDays

        send_email("Software@finideas.com", "Finideas123", txtEmail.Text, "FinTester Registration Confirmation", Str);
        
    }
    public void send_email(string senderemail, string senderpassword, string receiveremail, string subject, string message)
    {
        //  If MessageBox.Show((Convert.ToString("This will send an email to ") & receiveremail) + " are you sure ?", "Confirm", MessageBoxButtons.YesNo) = DialogResult.Yes Then
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress(senderemail);
            mail.To.Add(receiveremail);
            mail.Subject = subject;
            mail.IsBodyHtml = true;
            mail.Body = message;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(senderemail, senderpassword);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            Response.Redirect("Login.aspx");
            //MessageBox.Show(Convert.ToString("Email Sent to ") & receiveremail)
        }
        catch (Exception ex)
        {
            //MessageBox.Show(ex.ToString());
            //MessageBox.Show("Email Is Invalid..");
        }
        //  End If
    }
}