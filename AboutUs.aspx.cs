﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class More : System.Web.UI.Page
{
    string sql;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();
    string UserId;
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        } 
     
        //SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
     
        DataSet ds1 = new DataSet();
        cnnstr.Open();
        sql = "select * from tblFaq";
        SqlDataAdapter da = new SqlDataAdapter(sql, cnnstr);
        da.Fill(ds1);
        RepeaterFaq.DataSource = ds1;
        RepeaterFaq.DataBind();
        cnnstr.Close();

    }

    protected void btnAboutus_Click(object sender, EventArgs e)
    {
        multiviewMore.ActiveViewIndex = 0;
    }
    protected void btnFaq_Click(object sender, EventArgs e)
    {
        //Response.Redirect("FAQ.aspx");
        multiviewMore.ActiveViewIndex = 1;
    }
    protected void btnAddmoreque_Click(object sender, EventArgs e)
    {
        Response.Redirect("https://docs.google.com/forms/d/e/1FAIpQLScYEI3KXd17Bluw6WoRY2cTovD0K7lyS5Qai0KdJH-uvPxnGQ/viewform");
    }
}