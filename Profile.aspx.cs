﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ProfileFinal : System.Web.UI.Page
{
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    string sql;
    string UserId;
    string oldPasswd;

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();

        if (!IsPostBack)
        {
            fillprofile();
        }

    }
    public void fillprofile()
    {
        cnnstr.Open();
        sql = "select * from Login Where Email= '" + UserId + "'";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet dsa = new DataSet();
        adp.Fill(dsa);
        txtFname.Text = ds.Tables[0].Rows[0]["Firstname"].ToString();
        txtLname.Text = ds.Tables[0].Rows[0]["Lastname"].ToString();
        txtmno.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();

        // Profile Only Display
        lblFname.Text = ds.Tables[0].Rows[0]["firstname"].ToString();
        lblLname.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
        lblMno.Text = ds.Tables[0].Rows[0]["MobileNo"].ToString();
        lblEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();

        lblExpDt.Text = ds.Tables[0].Rows[0]["Expdate"].ToString();
        lblExpDt.Text = lblExpDt.Text.Remove(lblExpDt.Text.Length - 11, 11);

        lblRegistration.Text = ds.Tables[0].Rows[0]["Registerdate"].ToString();
        lblRegistration.Text = lblRegistration.Text.Remove(lblRegistration.Text.Length - 11, 11);

        //calculate Day 
        string ExpDt;
        string Todate;
        int LeftDays;
        ExpDt = Convert.ToString(ds.Tables[0].Rows[0]["Expdate"]);
        Todate = System.DateTime.Now.ToString("dd-MM-yyyy");
        LeftDays = Convert.ToDateTime(ExpDt).Subtract(Convert.ToDateTime(Todate)).Days;
        lblDayLeft.Text = Convert.ToString(LeftDays) + " - Days" + "";

        //Calculate Data Limit
        string FromDate;
        string Today;
        FromDate = Convert.ToString(ds.Tables[0].Rows[0]["Fromdate"]);
        Today = Convert.ToString(ds.Tables[0].Rows[0]["Todate"]);
        FromDate = FromDate.Remove(FromDate.Length - 11, 11);
        lblDataLimit.Text = "Your Data Limit Is This Date " + FromDate + " To This Date " + Convert.ToDateTime(ExpDt).ToString("dd-MM-yyyy") + " ";
       
        cnnstr.Close();
    }

    protected void btnUpdateProfile_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void btnChangePasswd_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void btnprofile_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }

    protected void btnpasswdChange_Click(object sender, EventArgs e)
    {
        if (txtOldPasswd.Text == "")
        {
            lblmsg.Text = "Do Not Enter Blank Old Password";
            return;
        }
        if (txtNewPasswd.Text == "")
        {
            lblmsg.Text = "Do Not Enter Blank New Password";
            return;
        }
        if (txtConfirmPasswd.Text == "")
        {
            lblmsg.Text = "Do Not Enter Blank Confirm Password";
            return;
        }

        cnnstr.Open();
        sql = "Select * from Login where Email = '" + UserId + "'";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet ds1 = new DataSet();
        adp.Fill(ds1);
        cnnstr.Close();
        oldPasswd = ds1.Tables[0].Rows[0]["Password"].ToString().Trim();

        if (txtOldPasswd.Text == oldPasswd)
        {
            if (txtNewPasswd.Text == txtConfirmPasswd.Text)
            {
                cnnstr.Open();
                sql = "Update Login set Password= '" + txtConfirmPasswd.Text + "' where Email = '" + UserId + "' ";
                SqlCommand cmd = new SqlCommand(sql, cnnstr);
                cmd.ExecuteNonQuery();
                cnnstr.Close();
                Response.Redirect("Profile.aspx");
            }
            else
            {
                lblmsg.Text = "Enter New Password And Confirm Password Must Be Same";
            }
        }
        else
        {
            lblmsg.Text = "Does not Match Old Password";
            return;
        }


    }

    protected void btnprofileUpdate_Click(object sender, EventArgs e)
    {
        if (txtFname.Text == "")
        {
            lblmsg.Text = "Do Not Enter Blank First Name";
            return;
        }
        else if (txtLname.Text == "")
        {
            lblmsg.Text = "Do Not Enter Blank Last Name";
            return;
        }
        else if (txtmno.Text == "")
        {
            lblmsg.Text = "Do Not Enter Blank Mobile No";
            return;
        }
        else
        {
            cnnstr.Open();
            sql = "Update Login set Firstname = '" + txtFname.Text + "',Lastname = '" + txtLname.Text + "',MobileNo = '" + txtmno.Text + "' where Email = '" + UserId + "' ";
            SqlCommand cmd = new SqlCommand(sql, cnnstr);
            cmd.ExecuteNonQuery();
            cnnstr.Close();
            Response.Redirect("Profile.aspx");
        }
    }

}