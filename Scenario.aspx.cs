﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;


//using MSChart20Lib;

public partial class Scenario : System.Web.UI.Page
{
    //string blanck = " ";  

    string sql;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();
    string UserId;
                                            
    // Variable Of Chart And Graph
    String str = "";
    double interval;
    double txtmid;
    long div = 1;

    public static DataTable profit = new DataTable();
    public static DataTable deltatable = new DataTable();
    public static DataTable gammatable = new DataTable();
    public static DataTable vegatable = new DataTable();
    public static DataTable thetatable = new DataTable();
    public static DataTable volgatable = new DataTable();
    public static DataTable vannatable = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }

        //After compelet Edit this Will Be Inside The Comment Tag
        if (!IsPostBack)
        {
            bindgrid();

            sql = "Select * from tblScenarioHeader Where Email = '" + UserId + "'";
            SqlDataAdapter hdradp = new SqlDataAdapter(sql, cnnstr);
            DataSet hdrds = new DataSet();
   
            hdradp.Fill(hdrds);

            if (hdrds.Tables[0].Rows.Count >= 1)
            {
                txtcmp.Text = hdrds.Tables[0].Rows[0]["CMP"].ToString();
                txtCvol.Text = hdrds.Tables[0].Rows[0]["CallVol"].ToString();
                txtPvol.Text = hdrds.Tables[0].Rows[0]["PutVol"].ToString();
                txtVol.Text = hdrds.Tables[0].Rows[0]["Vol"].ToString();
                txtStartDt.Text = Convert.ToDateTime(hdrds.Tables[0].Rows[0]["StartingDate"].ToString()).ToString("dd/MMM/yyyy");
                txtExpDt.Text = Convert.ToDateTime(hdrds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("dd/MMM/yyyy"); ;
                txtInterval.Text = hdrds.Tables[0].Rows[0]["Interval"].ToString();
                txtUpDownStrike.Text = hdrds.Tables[0].Rows[0]["UpDownStrike"].ToString();
                txtMstrike.Text = hdrds.Tables[0].Rows[0]["MiddleStrike"].ToString();
                txtScenarioName.Text = hdrds.Tables[0].Rows[0]["ScenarioName"].ToString();
            }
        }

        fetchGreeksTotal();

        // Get Min And Max Date From The Trade table tblscenario 
        sql = "select MIN(startingdate) as MinDt,Max (EndDate)  as MaxDt from tblScenarioHeader Where Email = '"+ UserId +"'";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet das = new DataSet();
        adp.Fill(das);

        if (das.Tables[0].Rows[0]["MinDt"] == DBNull.Value) 
        {
            return;
        }
        string min, max;
        DateTime dtmin, dtmax;
        min = das.Tables[0].Rows[0]["MinDt"].ToString();
        min = min.Replace("12:00:00 AM", "");
        dtmin = Convert.ToDateTime(min);

        max = das.Tables[0].Rows[0]["MaxDt"].ToString();
        max = max.Replace("12:00:00 AM", "");
        dtmax = Convert.ToDateTime(max).AddDays(1);

        // Check Saturday and Sunday If it is there then not display 
        while (dtmin != dtmax)
        {
            if (dtmin.DayOfWeek != DayOfWeek.Saturday && dtmin.DayOfWeek != DayOfWeek.Sunday)
            {
                ListItem lst = new ListItem(dtmin.ToString("dd-MMM-yy"));
                chklistDt.Items.Add(dtmin.ToString("dd-MMM-yy"));
            }
            dtmin = dtmin.AddDays(1);
        }

    }

    public void fetchGreeksTotal()
    {
        if (cnnstr.State == ConnectionState.Open)
        {
            cnnstr.Close();
        }
        cnnstr.Open();

        sql = "select isnull(SUM (Qty),0)* isnull(SUM(Rate),0)- isnull(SUM(Qty),0)*isnull(SUM(Ltp),0) as TotalProfitLoss ,isnull(SUM (DeltaValue),0) as TotalDeltaValue,isnull(SUM (GammaValue),0) as TotalGammaValue ";
        sql += " ,isnull(SUM (VegaValue),0) as TotalVegaValue,isnull(SUM (ThetaValue),0) as TotalThetaValue  ,isnull(SUM (VolgaValue),0) as TotalVolgaValue,isnull(SUM (VannaValue),0) as TotalVannaValue ";
        sql += " from  tblscenario where Email = '" + UserId + "'";

        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet dstot = new DataSet();
        adp.Fill(dstot);
        cnnstr.Close();

        txtProfitLoss.Text = dstot.Tables[0].Rows[0]["TotalProfitLoss"].ToString();
        float ProfitLoss = float.Parse(txtProfitLoss.Text);
        txtProfitLoss.Text = Convert.ToString(Math.Round(ProfitLoss, 5));

        txtdelta.Text = dstot.Tables[0].Rows[0]["TotalDeltaValue"].ToString();
        float Delta = float.Parse(txtdelta.Text);
        txtdelta.Text = Convert.ToString(Math.Round(Delta, 5));

        txtGamma.Text = dstot.Tables[0].Rows[0]["TotalGammaValue"].ToString();
        float Gamma = float.Parse(txtGamma.Text);
        txtGamma.Text = Convert.ToString(Math.Round(Gamma, 5));

        txtVega.Text = dstot.Tables[0].Rows[0]["TotalVegaValue"].ToString();
        float vega = float.Parse(txtVega.Text);
        txtVega.Text = Convert.ToString(Math.Round(vega, 5));

        txtTheta.Text = dstot.Tables[0].Rows[0]["TotalThetaValue"].ToString();
        float Theta = float.Parse(txtTheta.Text);
        txtTheta.Text = Convert.ToString(Math.Round(Theta, 5));

        txtVolga.Text = dstot.Tables[0].Rows[0]["TotalVolgaValue"].ToString();
        float Volga = float.Parse(txtVolga.Text);
        txtVolga.Text = Convert.ToString(Math.Round(Volga, 5));

        txtVanna.Text = dstot.Tables[0].Rows[0]["TotalVannaValue"].ToString();
        float vanna = float.Parse(txtVanna.Text);
        txtVanna.Text = Convert.ToString(Math.Round(vanna, 5));
    }

    public void bindgrid()
    {
        if (cnnstr.State == ConnectionState.Open)
        {
            cnnstr.Close();
        }
        cnnstr.Open();
        sql = "select ID,StartDate,ExpiryDate,CPF,Underlying,Strike,Qty,Rate,Vol,Ltp,Delta,DeltaValue,Gamma,GammaValue,Vega,VegaValue,Theta,ThetaValue,Volga,VolgaValue,Vanna,VannaValue,DiffFactor from tblscenario where Email = '" + UserId + "'";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        grvScenario.DataSource = ds;
        grvScenario.DataBind();
        cnnstr.Close();
        fetchGreeksTotal();
    }

    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        if (cnnstr.State == ConnectionState.Closed)
        {
            cnnstr.Open();
        }

        //Get User Details 
        sql = "Select * from tblScenarioHeader Where Email = '" + UserId + "'";
        SqlDataAdapter gadp = new SqlDataAdapter(sql, cnnstr);
        DataSet gds = new DataSet();
        gadp.Fill(gds);

        //New Entry for Trade
        sql = " 0,'" + UserId + "','" + txtStartDt.Text + "','" + txtExpDt.Text + "','C'," + txtcmp.Text + ",0,0,0," + txtCvol.Text + "";
        SqlCommand cmd = new SqlCommand("[Sp_InsUpdScenarioDetails]" + sql + "", cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();

        // For Display Trade 
        bindgrid();
    }

    protected void grvScenario_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Label lblID = (Label)grvScenario.Rows[e.RowIndex].FindControl("lblID");
        TextBox GtxtStartDt = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtStartDt");
        TextBox GtxtExpDt = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtExpDt");
        DropDownList ddlGtxtCPF = (DropDownList)grvScenario.Rows[e.RowIndex].FindControl("ddlGtxtCPF");
        TextBox GtxtUnderlying = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtUnderlying");
        TextBox GtxtStrike = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtStrike");
        TextBox GtxtQty = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtQty");
        TextBox GtxtRate = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtRate");
        TextBox GtxtVol = (TextBox)grvScenario.Rows[e.RowIndex].FindControl("GtxtVol");

        cnnstr.Open();
        //sql = "Update tblscenario set ExpiryDate = '" + GtxtExpDt.Text + "' ,CPF = '" + ddlGtxtCPF.SelectedValue + "' , Underlying = '" + GtxtUnderlying.Text + "' , Strike= '" + GtxtStrike.Text + "' , Qty = '" + GtxtQty.Text + "' , Rate = '" + GtxtRate.Text + "' ,Vol = '" + GtxtVol.Text + "' Where Email = '" + UserId + "' and ID = '" + lblID.Text + "'";

        sql = " " + lblID.Text + ",'" + UserId + "','" + GtxtStartDt.Text + "','" + GtxtExpDt.Text + "','" + ddlGtxtCPF.Text + "'," + GtxtUnderlying.Text + "," + GtxtStrike.Text + "," + GtxtQty.Text + "," + GtxtRate.Text + "," + GtxtVol.Text + "";
        SqlCommand cmd = new SqlCommand("[Sp_InsUpdScenarioDetails]" + sql + "", cnnstr);
        cmd.ExecuteNonQuery();

        cnnstr.Close();
        grvScenario.EditIndex = -1;
        bindgrid();
    }

    protected void grvScenario_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grvScenario.EditIndex = -1;
        bindgrid();
    }

    protected void grvScenario_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grvScenario.EditIndex = e.NewEditIndex;
        bindgrid();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        cnnstr.Open();
        UserId.Trim();
        sql = "Delete from tblScenario where Email = '" + UserId + "'";
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        txtcmp.Text = "";
        txtCvol.Text = "";
        txtPvol.Text = "";
        txtVol.Text = "";
        txtStartDt.Text = "";
        txtExpDt.Text = "";
        txtInterval.Text = "";
        txtUpDownStrike.Text = "";
        txtMstrike.Text = "";
        txtScenarioName.Text = "";

        Response.Redirect("Scenario.aspx");
    }

    public void headerUpdate()
    {
        if (Session["UserEmail"] == null)
        {
            return;
        }
        else
        {
            if (cnnstr.State == ConnectionState.Closed)
            {
                cnnstr.Open();
            }

            if (chkInterval.Checked == false)
            {
                //Interval Type 0 It's called IntervalType Is Point wise 
                sql = "'" + txtScenarioName.Text + "','" + UserId + "','" + txtcmp.Text + "','" + txtCvol.Text + "','" + txtPvol.Text + "','" + txtVol.Text + "','" + txtStartDt.Text + "','" + txtExpDt.Text + "','0','" + txtInterval.Text + "','" + txtUpDownStrike.Text + "','" + txtMstrike.Text + "'";
                SqlCommand hcmd = new SqlCommand("[Sp_InsUpdScenarioHeader]" + sql + "", cnnstr);
                hcmd.ExecuteNonQuery();
            }
            else
            {
                //Interval Type 1 It's called IntervalType Is Percentage wise  
                sql = "'" + txtScenarioName.Text + "','" + UserId + "','" + txtcmp.Text + "','" + txtCvol.Text + "','" + txtPvol.Text + "','" + txtVol.Text + "','" + txtStartDt.Text + "','" + txtExpDt.Text + "','1','" + txtInterval.Text + "','" + txtUpDownStrike.Text + "','" + txtMstrike.Text + "'";
                SqlCommand hcmd = new SqlCommand("[Sp_InsUpdScenarioHeader]" + sql + "", cnnstr);
                hcmd.ExecuteNonQuery();
            }
            cnnstr.Close();
        }
    }

    protected void btnApplyNow_Click(object sender, EventArgs e)
    {
        headerUpdate();
        bindgrid();
    }

    protected void grvScenario_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void btnGrid_Click(object sender, EventArgs e)
    {
        //  multiviewScenario.ActiveViewIndex = 0;
    }

    protected void btnDel_Command(object sender, CommandEventArgs e)
    {
        string ID =Convert.ToString(e.CommandArgument);
        cnnstr.Open();
        sql = "delete from tblscenario where ID = '" + ID + "' and Email = '" + UserId + "'";
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        bindgrid();
    }

    protected void btnChart_Click(object sender, EventArgs e)
    {
        //  multiviewScenario.ActiveViewIndex = 1;
    }

    protected void btnResult_Click(object sender, EventArgs e)
    {
       // CalcResult();
    }

    private void CalcResult()
    {
        ClsCalcChart objCalc = new ClsCalcChart();

    }

}
