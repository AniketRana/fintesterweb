﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Net.Mail;


public partial class Login : System.Web.UI.Page
{
    SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    ClsBAL objbal = new ClsBAL ();
    ClsBOL objbol= new ClsBOL ();

    public String characters = "0123456789";

    public string UniqueNumber()
    {
        Random unique1 = new Random();
        string s = "IN";
        int unique;
        int n = 0;
        while (n < 10)
        {
            if (n % 2 == 0)
            {
                s += unique1.Next(10).ToString();
            }
            else
            {
                unique = unique1.Next(52);
                if (unique < this.characters.Length)
                    s = String.Concat(s, this.characters[unique]);
            }
            Session["Key"] = s.ToString();
            n++;
        }
        return s;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
     
        Session["AuthUser"] = null;
        //txtEmail.Text = "";
        //txtLogPwd.Text = "";
    }
    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        try
        {
            if (txtEmail.Text != "" || txtLogPwd.Text != "")
            {
                WriteLogFile.createLogFile(txtEmail.Text);
                sqlcon.Open();    
                objbol .Email = txtEmail .Text;
                objbol .Password  = txtLogPwd.Text;

                SqlCommand cmd = new SqlCommand("SELECT * FROM Login where Email='" + txtEmail.Text + "' and password='" + txtLogPwd.Text + "'", sqlcon);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sda.Fill(ds);

                sqlcon.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime myDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["Expdate"].ToString());
                    //DateTime myDate = DateTime.ParseExact(ds.Tables[0].Rows[0]["Expdate"].ToString(), "dd/MM/yyyy",
                    //System.Globalization.CultureInfo.InvariantCulture);
                    if (myDate >= DateTime.Today)
                    {
                        try
                        {
                            sqlcon.Open();
     
                            objbol.CheckKey = Convert.ToString(ds.Tables[0].Rows[0]["KeyID"].ToString().Trim());
                            objbol.GenRateKey = UniqueNumber();
                            //update login set KeyID = '123' where Email = 'aniket.rana@gmail.com'
                            SqlCommand KeyInsert = new SqlCommand("update Login set KeyID =  '" + objbol.GenRateKey + "' where Email = '" + objbol.Email + "'", sqlcon);
                            KeyInsert.ExecuteNonQuery();
                            sqlcon.Close();
                            Session["AuthUser"] = ds.Tables[0].Rows[0]["Firstname"].ToString();
                            Session["UserEmail"] = ds.Tables[0].Rows[0]["Email"].ToString();   
                            Response.Redirect("Dashboard.aspx");
                        }
                        catch (Exception)
                        {
                        }
                    }
                    else
                    {
                        Response.Write(@"<script>alert('licence has been expired please contact administrator.')</script>");
                        send_Mail_To_support(txtEmail.Text);
                        Panel1.Visible = true;
                        return;
                    }
                }
                else
                {
                    Response.Write(@"<script>alert('Username or password Invalid')</script>");
                    return;
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void send_Mail_To_support(string email)
    {
        string Str = null;
        Str = "<!DOCTYPE HTML>" + "\n";
        Str = Str + "<html lang='en-US'>" + "\n";
        Str = Str + "<head>" + "\n";
        Str = Str + "<meta charset='UTF-8'>" + "\n";
        Str = Str + "<title></title>" + "\n";
        Str = Str + "</head>" + "\n";
        Str = Str + "<body>" + "\n";
        Str = Str + "<p>License Expired</p>" + "\n";
        Str = Str + "<p>License Expired of " + email + "</p>" + "\n";
        Str = Str + "</body>" + "\n";
        Str = Str + "</html>" + "\n";

        send_email("Software@finideas.com", "Finideas123", "sarang.gadkari@finideas.com", "FinTester License Expired.", Str);
    }



    public void send_email(string senderemail, string senderpassword, string receiveremail, string subject, string message)
    {
        //  If MessageBox.Show((Convert.ToString("This will send an email to ") & receiveremail) + " are you sure ?", "Confirm", MessageBoxButtons.YesNo) = DialogResult.Yes Then
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress(senderemail);
            mail.To.Add(receiveremail);
            mail.Subject = subject;
            mail.IsBodyHtml = true;
            mail.Body = message;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(senderemail, senderpassword);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }
        catch (Exception ex)
        {
            //MessageBox.Show(ex.ToString());
            //MessageBox.Show("Email Is Invalid..");
        }
        //  End If
    }

    private void send_Exp_Mail(string email)
    {

    }

}