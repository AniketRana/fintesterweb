﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>FinTester</title>
		<meta name="description" content="Hound is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Hound Admin, Houndadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
         <link href="dist/css/pricingStyle.css" rel="stylesheet" type="text/css" />
    <link href="dist/css/fancy-buttons.css" rel="stylesheet" type="text/css" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
		<!-- vector map CSS -->
		<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Custom CSS -->
         <link href="dist/css/pricingStyle.css" rel="stylesheet" type="text/css" />
		<link href="dist/css/style.css" rel="stylesheet" type="text/css">
       
    <link href="dist/css/fancy-buttons.css" rel="stylesheet" type="text/css" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <style>
        .invalid
        {
            color:Red;
        }
        
        .plan-price1
        {
            font-size: 1em;
            font-weight: 300;
           /* letter-spacing: -3px;*/
        }
       
        </style>
        
</head>
<body">
<form runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
</asp:ScriptManager>
    	<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="Login.aspx">
						<img class="brand-img mr-10" src="Images/F-1.jpg" alt="brand"/>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Don't have an account?</span>
                    <a class="inline-block btn btn-info btn-rounded btn-outline" href="SignUp.aspx">Sign Up</a>
                    <%--<asp:Button class="inline-block btn btn-info btn-rounded btn-outline" 
                        ID="btnsingup" runat="server" Text="Sign Up" onclick="btnsingup_Click"></asp:Button>--%>
				</div>
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in to FinIdeas</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>	
										<div class="form-wrap">
											<form action="#">
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputEmail_2">Email address</label>
													
                                                    <asp:TextBox class="form-control"  ID="txtEmail" placeholder="Enter Email" 
                                                        runat="server" TabIndex="1"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                            runat="server" ErrorMessage="RequiredFieldValidator" 
                                                        ControlToValidate="txtEmail" CssClass="invalid">Please Enter Your Email..</asp:RequiredFieldValidator>
                                                    <%--<input type="email" class="form-control" required="" id="exampleInputEmail_2" placeholder="Enter email">--%>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
													
                                                        <%--<asp:Button ID="btnResetPwd" runat="server" Text="Reset Password" />--%>
                                                    <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="Forgetpassword.aspx">forgot password ?</a>
													<div class="clearfix"></div>
                                                        <asp:TextBox class="form-control"  ID="txtLogPwd" TextMode="Password" 
                                                        placeholder="Enter password" runat="server" TabIndex="2"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                            runat="server" ErrorMessage="RequiredFieldValidator" 
                                                        ControlToValidate="txtLogPwd" CssClass="invalid">Please Enter Your Password..</asp:RequiredFieldValidator>
												</div>
											<%--	
												<div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
                                                            <asp:CheckBox ID="checkbox_2"  type="checkbox" runat="server" />
														<label for="checkbox_2"> Keep me logged in</label>
													</div>
													<div class="clearfix"></div>
												</div>--%>
												<div class="form-group text-center">
                                                            <asp:Button type="submit" class="btn btn-info btn-rounded" ID="btnSubmit" 
                                                                runat="server" Text="Sign in" onclick="btnSubmit_Click1" TabIndex="3" />
												</div>
                                                <div class="form-group text-center">

                                                <asp:Label ID="lblMsg" Runat="server" Text="" />
                                                
                                                </div> 
											</form>
										</div>
									</div>	
								</div>
							</div>
                            
                            <asp:Panel ID="Panel1" runat="server" Visible="False">
                            <section style="text-align: center;">

    
    <section class="third lift plan-tier" onclick="location.href='#';">

      
      <h4>Free Trial</h4>
      <h3><sup class="superscript"></sup><span class="plan-price1">Free</span><sub></sub></h3>

      
      <p class="early-adopter-price">Full Stock</p><br>
      <del></del>
      

      <ul>
<li><strong>Option,Index,Currency</strong></li>

<li>7 - Days<strong> Free Demo</strong></li>
<li class="btn btn-success btn-outline fancy-button btn-0"><strong><a>Buy Now</a></strong></li>
</ul>

    </section>
    
    <section class="third lift plan-tier callout" onclick="location.href='#';">

      <h6>Most popular</h6>
      <h4>Yearly Rent</h4>
      <h3><sup class="superscript" style="right: 0px;">₹​</sup><span class="plan-price1">25,000</span><sub></sub></h3>

      
      <p class="early-adopter-price">Full Stock</p><br>
      <del></del>
      

      <ul>
<li><strong>Option,Index,Currency</strong></li>
<li>Nifty Only <strong>10,000/-</strong> Per Year</li>

<li class="btn btn-success btn-outline fancy-button btn-0"><strong><a href="http://finideas.com/checkout.aspx?SUQ.-PJwgwTFP5io.=Nw==-MX7rMcuKWUI=&amp;VFlQRQ..-8idGk8JSOCE.=U09GVFdBUkUtUkVOVA==-vcRyZZEsqGo=&amp;UFJJQ0U.-K51qKhMFra0.=MTE5NzA=-aPjrFg4OavE=">Buy Now</a></strong></li>
</ul>

    </section>
    
    <section class="third lift plan-tier" onclick="location.href='#';">

      
      <h4>Life Time</h4>
      <h3><sup class="superscript" style="right: 0px;">₹​</sup><span class="plan-price1">50,000</span><sub></sub></h3>

      
      <p class="early-adopter-price">Full Stock</p><br>
      <del></del>
      

      <ul>
<li><strong>Option,Index,Currency</strong></li>
<li>Nifty Only <strong>20,000/-</strong> Per Year</li>

<li class="btn btn-success btn-outline fancy-button btn-0"><strong><a href="http://finideas.com/checkout.aspx?SUQ.-PJwgwTFP5io.=Nw==-MX7rMcuKWUI=&VFlQRQ..-8idGk8JSOCE.=U09GVFdBUkUtT1VUUklHSFQ=-U4FA5UyjGWw=&UFJJQ0U.-K51qKhMFra0.=MjM5NDA=-B7ZzL55Y9WE=">Buy Now</a></strong></li>

</ul>

    </section>
    
    <div style="clear: both"></div>
  </section>
                            </asp:Panel>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="dist/js/init.js"></script></form>
         
         <script>

             // Back Button Disable
             window.history.forward();
             function noBack() {
                 window.history.forward();
             }
             
        </Script>

</body>
</html>
