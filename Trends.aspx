﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Trends.aspx.cs" Inherits="Trends" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ddl
        {
            color: white;
            background-color: #516ead;
            height: 30px;
            width: 150px;
        }
        .dtddl
        {
            color: black;
            background-color: white;
            height: 25px;
            width: 150px;
        }
        .Calendar .ajax__calendar_body
        {
            width: 162px;
            height: 135px;
            background-color: #c0ddea;
            color: white;
            z-index: 10;
        }
        .Calendar .ajax__calendar_header
        {
            background-color: LightGray;
            color: black;
        }
        .tblwidth
        {
          /*  white-space:nowrap;*/
        }
        .template 
        {
            height:550px;
            overflow-x:scroll;
        }
        .btnPN 
        {
             width: 90px;
             height: 30px;
             font-family: 'Roboto', sans-serif;
             font-size: 15px;
             /*letter-spacing: 2.5px;
             font-weight: 500;*/
             color: white;
             background-color: #0783ef;
             border: none;
             border-radius:5px;
             box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1); /* rgb(4, 31, 232); */
             transition: all 0.3s ease 0s;
             cursor: pointer;
             outline:none;
        }
        .AMT
        {
            background-color:Black !important;    
            color:White !important:
        }
        /* Define the hover highlight color for the table row */
        
        
        .PTChildPivotTable td:hover
        {
              background-color: #ffff99 !important;
        }

      .PTChildPivotTable tr:hover 
        {
              background-color: #ff0000 !important;
        }
        /*.btnPN:hover 
        {
          background-color: #51adef;
          box-shadow: 0px 15px 20px
          color: white ;
          font-weight:bold;
          
        }*/
        
        /*.hover_row
        {
            background-color: #A1DCF2 !important;

        }*/
        
    </style>
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers>  
    <asp:PostBackTrigger ControlID="btnExpToExl" />   
</Triggers> 
        <ContentTemplate>        
            <div class="page-wrapper" style="min-height: 664px;padding-top: 30px;padding-bottom: 0px;">
                <div class="container-fluid pt-25">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pb-0" style="height:60px;" >
                                <div class="panel-wrapper collapse in">
                                    <%--<center>
                                <span class="panel-price"><font size="5"><b>Trends</b> </font></span>
                            </center>--%>
                                    <b>Symbol :</b>
                                    <asp:DropDownList ID="ddlCompany" class="ddl" runat="server" 
                                        style="width: 132px;" DataTextField="Security_Symbol"
                                        DataValueField="Security_Symbol" DataSourceID="SqlDataSource1" 
                                        AutoPostBack="True" 
                                        onselectedindexchanged="ddlCompany_SelectedIndexChanged" TabIndex="1">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SQLDB01TConnectionString3 %>"
                                        SelectCommand="select * from tblsymbol order  by Security_Symbol"></asp:SqlDataSource>
                                    <b> Trends :</b>
                                    <asp:DropDownList ID="ddlTrend" class="ddl" runat="server" TabIndex="2"
                                        style="width: 120px;" DataTextField="Trends"
                                        DataValueField="Trends" DataSourceID="SqlDataSource3" AutoPostBack="True" 
                                        onselectedindexchanged="ddlTrend_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <b>CPF :</b>
                                    <asp:DropDownList ID="ddCpf" class="ddl" style="width: 44px;" runat="server" TabIndex="3"
                                        AutoPostBack="True" onselectedindexchanged="ddCpf_SelectedIndexChanged">
                                        <asp:ListItem>CE</asp:ListItem>
                                        <asp:ListItem>PE</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:SQLDB01TConnectionString4 %>"
                                        SelectCommand="select * from Trends order by Trends"></asp:SqlDataSource>
                                     Rounding :
                                    <asp:DropDownList ID="ddlRounding" AutoPostBack="True" TabIndex="4" Style="height: 30px; width: 40px; 
                                        background-color: #516ead; color: white;" runat="server" 
                                        onselectedindexchanged="ddlRounding_SelectedIndexChanged">
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                    </asp:DropDownList>
                                    <b>Date : </b>
                                    <asp:TextBox ID="txtBdate" Width="85px" required runat="server" AutoPostBack="true"  ViewStateMode="Enabled" TabIndex="5"
                                        ontextchanged="txtBdate_TextChanged">
                                    </asp:TextBox>

                                    <cc1:CalendarExtender ID="calenderBusinessDt" PopupButtonID="txtBdate" runat="server"
                                        TargetControlID="txtBdate" Format="dd MMM yyyy" PopupPosition="BottomRight" ClientIDMode="Inherit"
                                        Animated="False" CssClass="Calendar">
                                    </cc1:CalendarExtender>

                                    <asp:Label ID="lblExpdt" runat="server" Text=""></asp:Label>
                                    <%--<asp:TextBox ID="txtExpDate" Width="130px" required runat="server" placeholder="ex : 30 Jan 2017">
                            </asp:TextBox>--%>
                                    <b style="margin-left: 4px;">Expiry Date : </b>
                                    <asp:DropDownList ID="ddlExpiry" class="dtddl" runat="server" Width="120px" AutoPostBack="true" TabIndex="6" 
                                        onselectedindexchanged="ddlExpiry_SelectedIndexChanged">
                                    </asp:DropDownList>

                                    <%--<asp:TextBox ID="txtExpDate" style="width:85px;" runat="server" ReadOnly="True"></asp:TextBox>--%>
                                </div>
                             
                           </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">                                                         
                            <div class="panel panel-default card-view" style="height: 775px;">
                                <div class="panel-heading" style="padding-top: 10px;padding-bottom: 0px;">
                                    <div class="clearfix">
                                        <%--btn btn-default btn-outline width="10%" --%>
                                          <center>
                                          <div style="height:40px;">
                                          <asp:Button class="btnPN" role="button"  
                                                      ID="btnPrev" runat="server" Text="Previous" onclick="btnPrev_Click" />
                                                <asp:Button class="btnPN" role="button"  
                                                      ID="btnNext" runat="server" Text="Next" onclick="btnNext_Click" />
                                              
                                               <asp:Label ID="lblPg" style="float:Left; font-weight:bold;"
                                                runat="server" Text=""></asp:Label>

                                              <asp:ImageButton ID="btnExpToExl" runat="server" ImageUrl="~/Images/IcoExcel.png" 
                                                  style="width: 40px; float:right" ImageAlign="AbsBottom" 
                                                  onclick="btnExpToExl_Click" Visible="False" ToolTip="Export To Excel Download" />
                                             </div> 
                                          </center>
                                           <asp:GridView Width="98.5%" ID="grvTrendsHeader" class="table table-striped table-bordered mb-0" 
                                                      runat="server" ShowHeader="False" Style="table-layout:fixed;" 
                                                      onrowdatabound="grvTrendsHeader_RowDataBound">
                                                      <RowStyle HorizontalAlign="left" ForeColor="Black" />
                                                </asp:GridView>    
                                   </div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" style="padding-top: 0px;" >
                                        <p class="text-muted">
                                        </p>
                                        <div class="table-wrap mt-0 template" style="height:650px;">
                                       
                                             <div class="table-responsive tblwidth"; >                                            
                                             
                                            
                                               

                                                <asp:GridView  Width="100%"  Style="table-layout: fixed;" 
                                                    ID="grvTrends" class="table table-striped table-bordered mb-0 "
                                                    runat="server" OnRowDataBound="grvTrends_RowDataBound" OnDataBound="grvTrends_DataBound"
                                                    ShowHeader="False">
                                                    <RowStyle HorizontalAlign="Right" ForeColor="Black" />
                                                </asp:GridView>

                                                 <font color="red" size="5">
                                                    <center>
                                                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                                    </center>
                                                </font>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--<asp:Button ID="btnNext" align=right Style="background-color: #0783ef; color: White;" Height="35px"
                                     Width="80px" runat="server" Text="Next" />--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <script>

        // Back Button Disable
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    
    </script>

    <script type = "text/javascript">
        // F5 Key Disable [Browser]    
        window.onload = function () {
            document.onkeydown = function (e) {
                return (e.which || e.keyCode) != 116;
            };
        }
         </script>

<%--  script for hover Effect ........
    <script type="text/javascript">
      $(function () {
          $("[id*=grvTrends] td").hover(function () {
              $("td", $(this).closest("tr")).addClass("hover_row",);
          }, function () {
              $("td", $(this).closest("tr")).removeClass("hover_row",);
          });
      });
</script>--%>
    
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>

    $("td").on("hover", function () {
        $(this).css({
            "background-color": "#0093e0",
            "border": "10px"
        });
    });

    </script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

</asp:Content>
