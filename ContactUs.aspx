﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
   
 <div class="page-wrapper">
 <div class="container-fluid pt-25">
     <asp:Label ID="lblOffice" runat="server" Text="Office" Font-Size="Large" Font-Bold="true" ForeColor="Red" Font-Underline="true"></asp:Label>
     &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <i class="fa fa-phone fa-call"  style="text-decoration: underline; color: #008000; font-size: large; "></i>
  <a href="https://docs.google.com/forms/d/e/1FAIpQLScwPCfUBsZZXnr_LtsIKOEtQyHE2s6g3oGa8e3FTQIe1FTZww/viewform" style="font-family: Arial; font-size: large; font-weight: bold; font-style: normal; font-variant: normal; text-transform: capitalize; color: #FF0000; ">&nbsp;  <font color="black"> | </font>&nbsp;  Call Me Back</a>
      <hr width="100%"  style="border: medium double #0000FF"/>
      <h2 style="font-family: Arial; font-size: large; font-weight: bold; font-style: normal; font-variant: normal; text-transform: capitalize; color: #000000;">
     FinIdeas Management Solutions Private Limited
      </h2>

      
      <p style="padding: 40px; margin: 2px; font-family: Arial; font-size: large; font-weight: normal; font-style: normal; font-variant: normal; text-transform: capitalize; color: #000000; table-layout: auto; border-collapse: separate; border-spacing: inherit; empty-cells: hide; position: static;"><i class=" zmdi zmdi-arrow-right mr-10 " > </i>
      803-City Centre, Near Sosyo Circle, Udhna Magdalla Road, Surat-395004
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.499676640367!2d72.82506021532997!3d21.17230038592085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04e1592bbe131%3A0xe1599e88c39af6da!2sCity+Center!5e0!3m2!1sen!2sin!4v1501307920702" frameborder="0" 
       style="border-style: none; border-color: inherit; border-width: 0; padding: 1px; margin: 1px; table-layout: auto; empty-cells: hide; position: relative; overflow: hidden; top: 0px; left: 26px;  
          width: 355px; height: 142px;" 
        align="right"></iframe><br />
       </p>
      <br />

      <p style="padding: 40px; margin: 2px; font-family: Arial; font-size: large; font-weight: normal; font-style: normal; font-variant: normal; text-transform: capitalize; color: #000000; table-layout: auto; border-collapse: separate; border-spacing: inherit; empty-cells: hide; position: static;"><i class=" zmdi zmdi-arrow-right mr-10 " > </i>
      1002,Luxuria Business Hub, Nr. V R Mall, Gaurav Path Road, Surat-395007
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.139435178217!2d72.75750251493457!3d21.146848585934535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04d80624e2ebf%3A0x92dea4d5e6f6574b!2sLuxuria+Business+Hub!5e0!3m2!1sen!2sin!4v1501307580639" frameborder="0" 
     style="border-style: none; border-color: inherit; border-width: 0; padding: 1px; margin: 1px; table-layout: auto; empty-cells: hide; position: relative; overflow: hidden; top: 0px; left: 27px;  
          width: 355px; height: 142px;" 
        align="right"></iframe><br />
        <br />
      </p> 
     <br />

      <h2 style="font-family: Arial; font-size: large; font-weight: bold; font-style: normal; font-variant: normal; text-transform: capitalize; color: #000000;">
     Email
      </h2>
      <p style="padding: 40px; margin: 2px; font-family: Arial; font-size: large; font-weight: normal; font-style: normal; font-variant: normal; text-transform: capitalize; color: #000000; table-layout: auto; border-collapse: separate; border-spacing: inherit; empty-cells: hide; position: static;"><i class=" zmdi zmdi-arrow-right mr-10 " > </i>
      info@finideas.com
      </p>

      <hr width="100%"  style="border: medium double #0000FF"/>
      <h1 style="font-family: Arial; font-size: large; font-weight: bold; font-style: normal; font-variant: normal; text-transform: capitalize; color: #FF0000; text-decoration: underline;">
     Support team
      </h1>
      <hr width="100%"  style="border: medium double #0000FF"/>
      <br /><br />
       <div class ="SupportContent">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
							<div class="panel panel-danger contact-card card-view" >
								<div class="panel-heading" style="background-color: #0066FF">
									<div class="pull-left">
										<div class="pull-left user-img-wrap mr-15">
											<img class="card-user-img img-circle pull-left"  src="Images/15-Mr%20Mohsin%20Soni-Cover.jpg" alt=""/>
										</div>
										<div class="pull-left user-detail-wrap">	
											<span class="block card-user-name">
												Mohsin Soni
											</span>
											<span class="block card-user-desn">
												Support Person
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body row">
										<div class="user-others-details pl-15 pr-15">
											<div class="mb-15">
												<span class="inline-block txt-dark">mohsin.soni@finideas.com</span>
											</div>
											<div class="mb-15">	
												<span class="inline-block txt-dark">9377573349</span>
											</div>
											<div class="mb-15">
												<span class="inline-block txt-dark">1002, Luxuria Business Hub, Nr. V R Mall, Gaurav Path Road, Surat - 395 007</span>
											</div>	
										</div>								
									</div>
								</div>
							</div>
						</div>
        </div>
        
        
         <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8">
							<div class="panel panel-danger contact-card card-view">
								<div class="panel-heading" style="background-color: #0066FF">
									<div class="pull-left">
										<div class="pull-left user-img-wrap mr-15">
											<img class="card-user-img img-circle pull-left" src="Images/0-Mr.%20Srinivas%20Mamindlapelly-Cover.jpg" alt=""/>
										</div>
										<div class="pull-left user-detail-wrap">	
											<span class="block card-user-name">
												Srinivas Mamindlapelly
											</span>
											<span class="block card-user-desn">
												Support Person
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body row">
										<div class="user-others-details pl-15 pr-15">
											<div class="mb-15">
												<span class="inline-block txt-dark">srinivas.Mamindlapelly@finideas.com</span>
											</div>
											<div class="mb-15">		
										<span class="inline-block txt-dark">9375204812</span>
											</div>
											<div class="mb-15">		
										<span class="inline-block txt-dark">1002, Luxuria Business Hub, Nr. V R Mall, Gaurav Path Road, Surat - 395 007</span>
											</div>
										</div>		
									</div>
								</div>
							</div>
						</div>
        

         <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8">
							<div class="panel panel-danger contact-card card-view">
								<div class="panel-heading" style="background-color: #0066FF">
									<div class="pull-left">
										<div class="pull-left user-img-wrap mr-15">
											<img class="card-user-img img-circle pull-left" src="Images/30-Mr.%20Shailesh%20Singh-Cover.jpg" alt=""/>
										</div>
										<div class="pull-left user-detail-wrap">	
											<span class="block card-user-name">
												Shailesh Singh
											</span>
											<span class="block card-user-desn">
												Support Person
											</span>
										</div>
									</div>
									
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body row">
										<div class="user-others-details pl-15 pr-15">
											<div class="mb-15">
												
												<span class="inline-block txt-dark">shailesh.singh@finideas.com</span>
											</div>
											<div class="mb-15">
												
												<span class="inline-block txt-dark">9723829924</span>
											</div>
											<div class="mb-15">
												
												<span class="inline-block txt-dark">1002, Luxuria Business Hub, Nr. V R Mall, Gaurav Path Road, Surat - 395 007</span>
											</div>
											
										</div>                                
										
									</div>
								</div>
							</div>
						</div>          
   <br />
</div>
 <hr width="100%"  style="border: medium double #0000FF"/>
<center>
<asp:Button ID="btnKnowmore"   BackColor="Highlight" ForeColor="#CC0066"  
        BorderStyle="Groove" runat="server" Text="Know more"  Font-Bold="true"  
        onclick="btnKnowmore_Click1" Height="33px" Width="489px"  Font-Size="Large" 
        BorderColor="Blue" />
</center>

</div>
 
              
</asp:Content>

