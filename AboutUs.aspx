﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AboutUs.aspx.cs" Inherits="More" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .btn
        {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            cursor: pointer;
            border-radius: 15px;
        }
        
        .btn1
        {
            background-color: white;
            color: black;
            border: 2px solid #4CAF50;
        }
        
        .btn1:hover
        {
            background-color: #4CAF50;
            color: white;
        }
        
        .txtAlign
        {
            height: 40px;
            width: 110px;
            padding-top: 9px;
            padding-left: 21px;
            padding-right: 24px;
        }
        
        .Faqcolor
        {
            background-color:Gray;
         }
            
           li {
                list-style: none;
                cursor: pointer;
            }
            li.section {
    
                margin-top:5px;
            }
            li.question {
                color: #9c2c2c;;
            }
            li.answer {
                padding: 10px;
            }
    </style>
    <div class="page-wrapper">
        <div class="container-fluid pt-25" style="background-color: #FFFFFF;">
            <center>
                <div class="row" style="background-color: #FFFFFF">
                    <asp:Button ID="btnAboutus" runat="server" Text="About Us" text-align="Center" class="btn btn1 txtAlign"
                        Height="40" Width="110" OnClick="btnAboutus_Click" />
                    <asp:Button ID="btnFaq" runat="server" Text="FAQs" Height="40" Width="110" class="btn btn1 txtAlign"
                        OnClick="btnFaq_Click" />
                </div>
                <br />
            </center>
            <asp:MultiView ID="multiviewMore" runat="server" ActiveViewIndex="0">
                <asp:View ID="ViewAboutus" runat="server">
                    <div class="row">
                    <div class="PageContent">
                     <header class="entry-header">
                     <h1 class="entry-title" style="font-family: Arial; color: #333333; font-weight: bold;">About Us
                     </h1>
                     </header>
                            <div class="pull-right">
                                <img class="fa-align-right" src="Images/F-3.jpg" height="240px" width="460px" />
                            </div>
                            <br />
                            <p class="paragraph-content1">
                                <i class=" zmdi zmdi-arrows"></i>&nbsp;<font color="black">Finideas was founded with
                                    a goal to educate people in the field of Equity & Derivatives<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Markets and provide software which would give
                                    them an edge in trading. FinIdeas<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; is a Team of young and committed having diverse
                                    education and experience backgrounds. </font>
                            </p>
                            <br />
                            <p class="paragraph-content2">
                                <i class=" zmdi zmdi-arrows"></i>&nbsp;<font color="black">We realize that we can remain
                                    competitive only through continuous updating and improvement. We<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;achieve this by focusing on constant enhancement
                                    of our knowledge base and technological edge.
                                    <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We are committed to an ambitious, forward- thinking
                                    and are always on a look out for the next big opportunity. </font>
                            </p>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="Company-Detail">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                                <div class="panel panel-danger contact-card card-view">
                                    <div class="panel-heading" style="background-color: #FF0000; border-top-width: 12px;
                                        padding-top: 12px; padding-bottom: 12px;">
                                        <div class="pull-left">
                                            <div class="pull-left user-detail-wrap">
                                                <span class="block card-user-name">Our Goal </span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body row">
                                            <div class="user-others-details pl-15 pr-15">
                                                <div class="mb-15">
                                                    <span class="inline-block txt-dark"><i class=" zmdi zmdi-arrows"></i>&nbsp;To become
                                                        a benchmark product and service &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;provider for
                                                        Equity & Derivatives markets in India.<br />
                                                        <br />
                                                        <i class=" zmdi zmdi-arrows"></i>&nbsp;To Identify Industry's trading needs, problems
                                                        and &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;provide solution related to it.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                                <div class="panel panel-danger contact-card card-view">
                                    <div class="panel-heading" style="background-color: #008000; border-top-width: 12px;
                                        padding-top: 12px; padding-bottom: 12px;">
                                        <div class="pull-left">
                                            <div class="pull-left user-detail-wrap">
                                                <span class="block card-user-name">Education </span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body row">
                                            <div class="user-others-details pl-15 pr-15">
                                                <div class="mb-15">
                                                    <span class="inline-block txt-dark"><i class=" zmdi zmdi-arrows"></i>&nbsp;We intend
                                                        to become themost preferred Training &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Institute
                                                        in the field of Equity & Derivatives markets &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;with
                                                        a focus towards practical sessions in simulated &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;market
                                                        environment using innovative technologies. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Our
                                                        goal is to make people aware of risk and return &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;prevailing
                                                        in different segments and ways to manage &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;it
                                                        effectively.
                                                        <br />
                                                        <i class=" zmdi zmdi-arrows"></i>&nbsp;Each of our leaders brings in diversified
                                                        experience &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;from various fields of the above
                                                        segments. Our &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;infrastructure and processes coupled
                                                        with our &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sincerity and passion is what makes
                                                        us confident of &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;achieving our goals and making
                                                        our vision a reality.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                                <div class="panel panel-danger contact-card card-view">
                                    <div class="panel-heading" style="background-color: #6666FF; border-top-width: 12px;
                                        padding-top: 12px; padding-bottom: 12px;">
                                        <div class="pull-left">
                                            <div class="pull-left user-detail-wrap">
                                                <span class="block card-user-name">Achivements </span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body row">
                                            <div class="user-others-details pl-15 pr-15">
                                                <div class="mb-15">
                                                    <span class="inline-block txt-dark"><i class=" zmdi zmdi-arrows"></i>&nbsp;We are the
                                                        first company in India to launch to trading &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;through
                                                        live Volatility.<br />
                                                        <i class=" zmdi zmdi-arrows"></i>&nbsp;We are dealing with more than 50 members
                                                        of NSE & &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BSE.<br />
                                                        <i class=" zmdi zmdi-arrows"></i>&nbsp;We have successfully competed the program
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;designed with BSE to promote Option Trading
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;throughout India throughout India.<br />
                                                        <i class=" zmdi zmdi-arrows"></i>&nbsp;We have trained more than 8000 Students till
                                                        date.<br />
                                                        <i class=" zmdi zmdi-arrows"></i>&nbsp;We trade Options, train people for Options
                                                        and make &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; software for Options.<br />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </asp:View>

                      <asp:View ID="ViewFaq" runat="server">
        <div class="row">
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12" style="padding-left: 20px">
                    <asp:Label ID="lblFaq" runat="server" Text="FAQ" Style="font-size: large; font-family: Arial;
                        font-weight: bold; font-style: normal; text-transform: capitalize; color: #FF0000;
                        margin-left: 10px; font-variant: normal"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel-wrapper collapse in">
                <div class="panel-body pa-15">
                    <div class="panel-group accordion-struct " style="margin-left: 20px;" role="tablist"
                        aria-multiselectable="true">

                        <div class="panel panel-default ">
                            <asp:Repeater ID="RepeaterFaq" runat="server">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingSix">
                                            <ul>
                                                <li class="section" style="height: auto; padding-top: 5px; background-color:#d2be3d;
                                                    margin-left: 0px; padding-left: 10px;">
                                                    <%# Eval("Question").ToString()%></li>
                                                <li class="question" style="height: auto; padding-top: 5px; margin-left: 0px; padding-left: 10px;">
                                                    ---) &nbsp;&nbsp;
                                                    <%# Eval("Answer").ToString()%></li>
                                            </ul>
                                            <%--<asp:Label ID="lblQue" runat="server" Text='<%# Eval("Question").ToString()%>'></asp:Label>
                                            <asp:Label ID="lblAns" runat="server" Text='<%# Eval("Answer").ToString()%>'></asp:Label>--%>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <asp:Button ID="btnAddmoreque" runat="server" Text="Add More Question" Style="margin-bottom: 10px;
            margin-left: 15px; color: Green; background-color: White;" OnClick="btnAddmoreque_Click" />
    </asp:View>

                  </asp:MultiView>
     </div> 
     </div>

    <script type="text/javascript">

        // Faq Page Hide And Show Question
        $('.question, .answer').css("display", "none");
        $('.section').click(function () {
            var $others = $('.question:visible').not(this);
            $others.next('.answer').hide();
            $others.slideToggle(500);
            $(this).next('.question').slideToggle(500);
        });
        $('.question').click(function () {
            $(this).next('.answer').slideToggle(500);
        });

        // Back Button Disable
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
      //aniket
    </script>
</asp:Content>
