﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Simulator.aspx.cs" Inherits="SimulatorNew" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <head>
        <style>
          
            .modalPopup
            {
                background: rgba(0,0,0,0.85);
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                padding-left: 350Px;
                padding-right: 350Px;
                padding-top: 480Px;
            }
            .template
            {
                height: 252Px;
                overflow-y: scroll;
            }
            .SMHeaderRow
            {
                background-color:Black;
            }
            
            .Calendar .ajax__calendar_body 
    {
      
      background-color:White;
      
    }
    .Calendar .ajax__calendar_header
    {
        background-color:#bc5656;
        width:170px;
        color:White;
    }
    
    .Dmargins
    {
        border: inset;
    border-width: 0.5px;
    font-weight: bold;
    padding: 5px;
    }
    .modalPopup1
            {
                background: rgba(0,0,0,0.85);
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                padding-left: 350Px;
                padding-right: 350Px;
                padding-top: 180Px;
            }
            
        </style>

         
        
    </head>
    <form>
     <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers>  
    <asp:PostBackTrigger ControlID="btnExpToExcel" />   
    <asp:PostBackTrigger ControlID="btnExpToExcel" />   
</Triggers> 
    <ContentTemplate>
       <div class="page-wrapper" style="min-height: 427px;">
        <div class="container-fluid">
      
            <div class="row" style = "background-color: white;">
            <div style = "float:left;" class = "divBorder Dmargins">
           <div style = "float:left;" >
           <div style="
    margin-bottom: 20px;">
                                <asp:Label ID="lblSymbol" Font-Bold="true" runat="server" Text="Symbol"></asp:Label>
                                <asp:DropDownList Height="27px" style="height:27px;font-size:14px;height:27px;width:150px;padding-top: 0px;margin-left: 9px;padding-left: 19px;padding-bottom: 0px;" ID="ddSymbol" runat="server" 
                                    class="btn btn-primary dropdown-toggle" DataSourceID="SqlDataSource1" 
                                    DataTextField="Security_Symbol" DataValueField="Security_Symbol">
                                    <asp:ListItem Selected="True" Text="Nifty"></asp:ListItem>
                                </asp:DropDownList>
                               
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:Finideas %>" 
                                    SelectCommand="SELECT * FROM [tblSymbol]"></asp:SqlDataSource>
                                    </div>
                                    <div>
                                    <asp:Label ID="lblBusinessDt" runat="server" Font-Bold="true" Text="Date"></asp:Label>
                                <asp:TextBox ID="txtBusinessDt" Width="80Px" calss="form-control" 
                                    runat="server" style="width:80px;height:27px;width:80px;width: 150px;border-left-width: 2px;padding-left: 25px;padding-top: 0px;padding-bottom: 0px;margin-left: 27px;" ReadOnly="False"></asp:TextBox>
                                    <asp:ImageButton ID ="ImageButton1" runat="server" ImageUrl="~/Images/Calendaricon-300x298.png" ImageAlign="Top" style="
    width: 27px;
    margin-bottom: 0px;
    margin-right: 3px;
"/>
                                    <cc1:CalendarExtender ID="Calendar1" PopupButtonID="ImageButton1" runat="server" TargetControlID="txtBusinessDt"
    Format="dd-MMM-yyyy" PopupPosition="BottomRight" ClientIDMode="Inherit" Animated="False" CssClass="Calendar">
</cc1:CalendarExtender> </div>
<div>
                                    </div>

           </div>

           <div style = "float:left">
                    
                    <div style= "margin-bottom: 20px;">
                                <asp:Label ID="lblStrikeprice" Font-Bold="true" runat="server" Text="Strike Price"></asp:Label>
                                <asp:TextBox ID="txtStrikePrice" Width="70Px" calss="form-control" 
                                    runat="server" style="width:70px;width: 99px;height: 27px;margin-left: 4px;" 
                                     ></asp:TextBox>
                                </div>
                                <div style="text-align: right;">
                                    <asp:RadioButton ID="rbCall" GroupName="Option" runat="server" Checked="True" Text="CALL" />
                                       <%-- <label for="radio_5">CALL</label>--%>
                                        <asp:RadioButton ID="rbPut" GroupName="Option" runat="server" Text="PUT" />
                                        <%--<label for="radio_5">PUT</label>--%>
                                <asp:Button class="btn  btn-primary" Height="27Px" style="height:27px;padding-top: 0px;padding-bottom: 0px;" ID="btnAdd"
                                    runat="server" Text="Add" OnClick="btnAdd_Click" />
                                    </div>
            </div>

            </div>
            <div style="float:left;" class = "Dmargins">
            
            <div style="margin-bottom: 20px;"><span>P&L Effects</span>
            <asp:Button class="btn  btn-success" ID="btnFixPrice" Width="60Px" Height="27Px"
                                     runat="server" style="height:27px;width: 72px;padding-bottom: 0px;padding-left: 0px;padding-top: 0px;padding-right: 0px;"
                                    Text="Fix Price" onclick="btnFixPrice_Click" />
                                     <asp:Button class="btn  btn-success" ID="btnPNL" Width="50Px" Height="27Px" style="height:27px;width: 72px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;padding-top: 0px;" runat="server" Text="PNL" onclick="btnPNL_Click"/>
                                 </div>
                                
                                <div style="text-align: right;"><span>Time</span>
            <asp:Button class="btn btn-primary" type="button"
                                            ID="btnTimerminus" runat="server" Text="Time - 1" style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;"
                                        onclick="btnTimerminus_Click" />
                                        <asp:Button class="btn btn-primary" type="button" ID="btnTimerplus"
                                        runat="server" Text="Time + 1" style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;" onclick="btnTimerplus_Click" />
            </div>


                               
                               

            </div>

            <div style="float:left; text-align: right;" class = "Dmargins">

            <div style="margin-bottom: 20px;"><span>Spot</span>
                               <asp:Button class="btn btn-primary" type="button" 
                                            ID="btnSpotminus" runat="server" style="height:27px;width: 72px;margin-bottom: 0px;padding-bottom: 0px;padding-right: 0px;padding-top: 0px;padding-left: 0px;"
                                            Text="Spot - 1" onclick="btnSpotminus_Click" />
                                    
                                        <asp:Button class="btn btn-primary" type="button" ID="btnSpotPlus"
                                        runat="server" style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;" Text="Spot + 1" onclick="btnSpotPlus_Click" />
                                        <asp:Button ID="btnResetSpot" class="btn btn-primary btn-outline btn-rounded mb-0"
                                        Width="80 Px" Height="27Px" runat="server" style="height:27px;width:80px;padding-bottom: 0px;padding-top: 0px;padding-right: 0px;padding-left: 0px;"
                                        Text="Reset Spot" onclick="btnResetSpot_Click" />
                                        </div>

                                        <div><span>Vol</span>
            <asp:Button class="btn btn-primary" type="button"
                                            ID="btnPutVolMinus" runat="server" Text="Vol - 1"
                                            style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;"
                                        onclick="btnPutVolMinus_Click" />
                                        <asp:Button class="btn btn-primary" type="button" ID="btnPutVolPlus"
                                        runat="server" Text="Vol + 1" style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;" onclick="btnPutVolPlus_Click" />
            

            
            <asp:Button ID="btnResetVol" class="btn btn-primary btn-outline btn-rounded mb-0"
                                        style="height:27px;width:80px;height:27px;width:80px;padding-bottom: 0px;padding-top: 0px;padding-right: 0px;padding-left: 0px;" Height="30Px" runat="server" Width="78Px"
                                        Text="Reset Vol" onclick="btnResetVol_Click" />
            </div>  
            </div>

            <div style="float:left;" class = "Dmargins"> 
                    
                    <div style="margin-bottom: 20px;"><span>Recording</span><asp:Label ID="lblrecStepts" runat="server" Text="Label"></asp:Label>
                <asp:Button class="btn  btn-primary" Width="50px" Height="27Px" style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;" ID="btnPrevious" runat="server" Text="< Pre" 
                                    onclick="btnPrevious_Click" />
                                <asp:Button class="btn  btn-primary" Width="50px" Height="27Px" style="height:27px;height:27px;padding-top: 0px;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;width: 72px;" ID="btnNext" runat="server" Text="Next>" 
                                    onclick="btnNext_Click1" />
                </div>       
                
                <div style = "text-align: right;"><span>Export To Excel</span>
            
            <asp:Button ID="btnExpToExcel" style="
   
    background-color: rgba(0, 197, 82, 0.86);
    border-top-width: 0px;
    border-right-width: 0px;
    border-bottom-width: 0px;
    border-left-width: 0px;
    height: 27px;
    color: white;
" runat="server"
                                            Text="Export To Excel" onclick="btnExpToExcel_Click" />

            </div>             
            </div>

           

            
            
            </div>
                                
       

      <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default card-view" style="
    height: 931px;
    background-color: white;
    float: left;
    width: 887px;
">
                                <div class="panel-wrapper collapse in">
                                        <div class="table-wrap mt-0  template" style="
    box-shadow: 1px 0px 15px 9px #B3B3B3;
    height: 887px;
">
                                            <div class="table-responsive SMTable" style="height: auto;">
                                                <asp:GridView ID="grvSimulator" runat="server" AutoGenerateColumns="False"
                                                    class="table table-striped table-bordered mb-0" style="border-collapse:collapse;text-align: right;width: auto;" 
                                                    onrowcommand="grvSimulator_RowCommand" 
                                                    onrowdatabound="grvSimulator_RowDataBound" HeaderStyle-BackColor = "#bc5656" HeaderStyle-ForeColor = "White" HeaderStyle-HorizontalAlign="Right">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Trade">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnTradePop" runat="server" Text="Trade" 
                                                                    CommandName="ShowPopUpOfTrade" />
                                                                
                                                                <%--<asp:ImageButton ID="btnEdit" ImageUrl="Images/edi.png" runat="server" CommandName="Edit"
                                                                    CausesValidation="false" ForeColor="Black" Height="25px" Width="25px" />
                                                                <br />
                                                                </span>--%>
                                                            </ItemTemplate>
                                                            <%--<EditItemTemplate>
                                                                <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update"
                                                                    ValidationGroup="evalid" CausesValidation="True" ForeColor="Black" />
                                                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel"
                                                                    CausesValidation="false" ForeColor="Black" />
                                                            </EditItemTemplate>--%>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:Button ID="Button1" runat="server" CausesValidation="false" 
                                                                    CommandName="DeleteRow" Text="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("srno") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Spot">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSpot" runat="server" Text='<%#Eval("Spot") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Strike">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStrike" runat="server"  Text='<%#Eval("Strike") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vol">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVol" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Vol")),R_Volatality).ToString("N" + R_Volatality +"") %></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="CPF">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCPF" runat="server" Text='<%#Eval("CPF") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Int">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInt" runat="server" Text='<%#Eval("Int") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Trade Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTrdDt" runat="server" Text='<%#Eval("Trade Date") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Exp Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblExpDt" runat="server" Text='<%#Eval("Exp Date") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ltp">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLtp" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Ltp")), R_Premium).ToString("N" + R_Premium + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Qty">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblQty" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Qty")), R_Qty).ToString("N" + R_Qty + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Delta">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDelta" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Delta")), R_1StGreeks).ToString("N" + R_1StGreeks + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Theta">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTheta" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Theta")), R_1StGreeks).ToString("N" + R_1StGreeks + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Gamma">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGamma" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Gamma")), R_2ndGreeks).ToString("N" + R_2ndGreeks + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Vega">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVega" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Vega")), R_1StGreeks).ToString("N" + R_1StGreeks + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Volga">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVolga" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Volga")), R_2ndGreeks).ToString("N" + R_2ndGreeks + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="Vanna">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVanna" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Vanna")), R_2ndGreeks).ToString("N" + R_2ndGreeks + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Delta Val">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeltaVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Delta Val")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Theta Val">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblThetaVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Theta Val")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="GammaVal">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGammaVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Gamma Val")), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="VegaVal">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVegaVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Vega Val")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="VolgaVal">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVolgaVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Volga Val")), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField HeaderText="VannaVal">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVannaVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Vanna Val")), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="FixVal">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFixVal" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Fix Val")), R_Premium).ToString("N" + R_Premium + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="P&L">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPL" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("P&L")), R_Pnl).ToString("N" + R_Pnl + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Insert Qty">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInsertQty" runat="server" Text='<%#Eval("InsertQty") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Insert Rate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblInsertRate" runat="server" Text='<%#Eval("InsertRate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Qty">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastQty" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("PastQty")), R_Qty).ToString("N" + R_Qty + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Spot">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastSpot" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("PastSpot")), R_Price).ToString("N" + R_Price + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Spot">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrSpot" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("CurrSpot")), R_Price).ToString("N" + R_Price + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Delta">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastDelta" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Past Delta")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Delta">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrDelta" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Curr Delta")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Delta Effect">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeltaEffect" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Delta Effect")), R_Pnl).ToString("N" + R_Pnl + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Gamma">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastGamma" runat="server" Text='<%#Eval("Past Gamma") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Gamma">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrGamma" runat="server" Text='<%#Eval("Curr Gamma") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Gamma Effect">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGammaEffect" runat="server" Text='<%#Eval("Gamma Effect") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Vol">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastVol" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Past Vol")), R_Volatality).ToString("N" + R_Volatality + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Vol">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrVol" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Curr Vol")), R_Volatality).ToString("N" + R_Volatality + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Vega">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastVega" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Past Vega")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Vega">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrVega" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Curr Vega")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vega Effect">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVegaEffect" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Vega Effect")), R_Pnl).ToString("N" + R_Pnl + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Vanna">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastVanna" runat="server" Text='<%#Eval("Past Vanna") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Vanna">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrVanna" runat="server" Text='<%#Eval("Curr Vanna") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vanna Effect">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVannaEffect" runat="server" Text='<%#Eval("Vanna Effect") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Volga">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastVolga" runat="server" Text='<%#Eval("Past Volga") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Volga">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrVolga" runat="server" Text='<%#Eval("Curr Volga") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Volga Effect">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVolgaEffect" runat="server" Text='<%#Eval("Volga Effect") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Time">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastTime" runat="server" Text='<%#Eval("Past Time") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Time">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrTime" runat="server" Text='<%#Eval("Curr Time") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Past Theta">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPastTheta" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Past Theta")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Curr Theta">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCurrTheta" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Curr Theta")), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Theta Effect">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblThetaEffect" runat="server" Text=''><%# Math.Round(Convert.ToDouble(Eval("Theta Effect")), R_Pnl).ToString("N" + R_Pnl + "")%></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#BC5656" ForeColor="White" HorizontalAlign="Right" />
                                                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                                    <SortedAscendingHeaderStyle BackColor="#848384" />
                                                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                                    <SortedDescendingHeaderStyle BackColor="#575357" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                </div>
                            </div>
                             <div class="panel panel-default card-view pb-0" style="
    float: right;
    width: 308px;
    height: 400px;
" >
                        <div class="panel-wrapper collapse in" style="height: auto;">
                          
                            <table style="width: auto;">
                            <tr>
                                    <td>
                                       
                                    </td>
                                    <td>
                                       <asp:Label Width="80Px" class="control-label " ID="Label10" runat="server" Text="Greeks" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                     <td>
                                       <asp:Label Width="80Px" class="control-label " ID="Label9" runat="server" Text="Greeks Value" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                       <asp:Label Width="80Px" class="control-label " ID="Label1" runat="server" Text="Delta" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtDeltaVal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtDeltaEffect" Height="30Px"
                                        runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label Width="80Px" class="control-label " ID="Label2" runat="server" Text="Gamma" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtGammaVal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtGammaEffect" Height="30Px"
                                        runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                       <asp:Label Width="80Px" class="control-label " ID="Label3" runat="server" Text="Vega" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtVegaVal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtVegaEffect" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                      <asp:Label Width="80Px" class="control-label " ID="Label4" runat="server" Text="Theta" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtThetaVal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtThetaEffect" Height="30Px"
                                        runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                 <tr>
                                    <td>
                                      <asp:Label Width="80Px" class="control-label " ID="Label5" runat="server" Text="Volga" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtVolgaVal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtVolgaEffect" Height="30Px"
                                        runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                 <tr>
                                    <td>
                                      <asp:Label Width="80Px" class="control-label " ID="Label6" runat="server" Text="Vanna" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtVannaVal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtVannaEffect" Height="30Px"
                                        runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                 <tr>
                                    <td>
                                     
                                    </td>
                                    <td>
                                     <asp:Label Width="80Px" class="control-label " ID="Label7" runat="server" Text="P&L Amount" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    <td>
                                    <asp:Label Width="80Px" class="control-label " ID="Label8" runat="server" Text="Total" style="display:inline-block;width:80px;display:inline-block;color: #bc5757;width:80px;"></asp:Label>
                                    </td>
                                    
                                </tr>
                                 <tr>
                                    <td>
                                       
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtpnlamount" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    <td>
                                    <asp:TextBox Width="95Px" class="form-control filled-input" ID="txtTotal" Height="30Px" runat="server" style="background-color: #f9b6b6;text-align: right;"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                            </table>
                           
                        </div>
                    </div>
                    <div style = "float:right;">
                        <asp:Button ID="btnShow" style="
   
    background-color: rgba(0, 197, 82, 0.86);
    border-top-width: 0px;
    border-right-width: 0px;
    border-bottom-width: 0px;
    border-left-width: 0px;
    height: 27px;
    color: white;
" runat="server" Text="Setting" />
                    </div>
                        </div>
                    </div>
                    
            <div class="row" style="
    margin-top: 19px;
">
                <div class="col-sm-12">
                   
                </div>
            </div>
           
            <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" BehaviorID="mpe" runat="server"
                PopupControlID="pnlPopup" TargetControlID="lnkDummy" BackgroundCssClass="modalBackground"
                CancelControlID="btnHide">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup1" Style="display: none" defaultbutton="btnAddTrade">
                <div class="header">
                    Add Trades Quantity
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pb-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="form-group">
                                        <label class="control-label mb-10">
                                            Trade</label>
                                        <%--<div class="radio-list">
                                            <div class="radio-inline pl-0">
                                                <span class="radio radio-info">
                                                    <asp:RadioButton ID="rb1Call" GroupName="Option" runat="server" />
                                                    <label for="radio_5">
                                                        CALL</label>
                                                </span>
                                            </div>
                                            <div class="radio-inline">
                                                <span class="radio radio-info">
                                                    <asp:RadioButton ID="rb1Put" GroupName="Option" runat="server" />
                                                    <label for="radio_6">
                                                        PUT</label>
                                                </span>
                                            </div>
                                            <div class="radio-inline">
                                                <span class="radio radio-info">
                                                    <asp:RadioButton ID="rb1Future" GroupName="Option" runat="server" />
                                                    <label for="radio_6">
                                                        FUTURE</label>
                                                </span>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio-list">
                                            <div class="radio-inline pl-0">
                                                <span class="radio radio-info">
                                                    <asp:RadioButton ID="rbBuy" GroupName="Type" runat="server" />
                                                    <label for="radio_5">
                                                        BUY</label>
                                                </span>
                                            </div>
                                            <div class="radio-inline">
                                                <span class="radio radio-info">
                                                    <asp:RadioButton ID="rbSell" GroupName="Type" runat="server" />
                                                    <label for="radio_6">
                                                        SELL</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblTrdQty" Font-Bold="true" runat="server" Text="Quantity"></asp:Label>
                                        <asp:TextBox ID="txtTrdQty" Width="70Px" calss="form-control" runat="server"></asp:TextBox>
                                        <asp:Button class="btn  btn-primary" Height="27Px" Style="padding-top: 1Px;" ID="btnAddTrade"
                                            runat="server" Text="Add Trade" onclick="btnAddTrade_Click1"  TabIndex="0" />
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="btnHide" runat="server" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            

           <!-- ModalPopupExtender -->
            <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnShow"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup1" align="center" style="position: fixed; left: 0px; top: 0px; z-index: 10000; width: 1263px; height: 1202px;">
                 <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">
                                            Manual Setting
                                        </h6>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row mt-00">
                                            <div class="col-sm-12">
                                                <div style="text-align: initial;">
                                                    
                                                        <p>
                                                            <asp:DropDownList ID="ddlprice" runat="server" Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : Price
                                                            </label>
                                                        
                                                        </p>
                                                        <p>
                                                            
                                                            <asp:DropDownList ID="ddlpnl" runat="server" Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : PNL
                                                            </label>
                                                        
                                                    </p>
                                                    
                                                        <p>
                                                            <asp:DropDownList ID="ddl1Greeks" runat="server" Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : 1st Greeks
                                                            </label>
                                                        </p>
                                                        <p>
                                                           
                                                            <asp:DropDownList ID="ddl1GreeksValue" runat="server" Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : 1st Greeks Value
                                                            </label>
                                                        </p>
                                                 
                                              
                                                        <p>
                                                            <asp:DropDownList ID="ddl2Greek" runat="server"  Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : 2nd Greeks
                                                            </label>
                                                        </p>
                                                        <p>
                                                            
                                                            <asp:DropDownList ID="ddl2GreekValue" runat="server"  Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : 2nd Greeks Value
                                                            </label>
                                                        </p>
                                                    
                                                    
                                                        <p>
                                                            <asp:DropDownList ID="ddlSpremium" runat="server" Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : Premium
                                                            </label>
                                                        </p>
                                                        <p>
                         
                                                            <asp:DropDownList ID="ddlSVolatality" runat="server"  Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : Volatality
                                                            </label>
                                                        </p>
                                                   
                                                    <p>
                                                        <p>
                                                            <asp:DropDownList ID="ddlSqty" runat="server"  Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : Qty
                                                            </label>
                                                        </p>
                                                        <p>
                                                            
                                                            <asp:Button ID="btnSaveManual" CausesValidation="false" runat="server" class="btn btn-success btn-anim" OnClick="btnSaveManual_Click"
                                                                Text="Save Setting " />
                                                        </p>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">
                                            Hide &amp; Unhide Setting</h6>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row mt-00">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <div style="text-align: initial;">
                                                        <p>
                                                            <p>
                                                                <asp:CheckBox ID="chkdef" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Default
                                                                </label>
                                                            </p>
                                                            <p>
                                                                
                                                                <asp:CheckBox ID="chkHide" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Hide
                                                                </label>
                                                            </p>
                                                        </p>
                                                        <p>
                                                            <p>
                                                                <asp:CheckBox ID="chkSGreek" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Greeks
                                                                </label>
                                                            </p>
                                                            <p>
                                                                
                                                                <asp:CheckBox ID="chkSGreekValue" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Greeks Value
                                                                </label>
                                                            </p>
                                                        </p>
                                                        <p>
                                                            <p>
                                                                <asp:CheckBox ID="chkpnl" runat="server" />
                                                                <label for="checkbox-9">
                                                                    PNL
                                                                </label>
                                                            </p>
                                                            <p>
                                                             
                                                            
                                                                <asp:CheckBox ID="chkDelEffct" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Delta Effect
                                                                </label>
                                                            </p>
                                                        </p>
                                                        <p>
                                                            <p>
                                                                <asp:CheckBox ID="chkVegaEffct" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Vega Effect
                                                                </label>
                                                            </p>
                                                            <p>
                                                                
                                                                <asp:CheckBox ID="chkTheEffct" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Theta Effect
                                                                </label>
                                                            </p>
                                                        </p>
                                                        <p>
                                                            <p>
                                                                <asp:Button ID="btnHUSetting" CausesValidation="false" runat="server" class="btn btn-success btn-anim" OnClick="btnHUSetting_Click"
                                                                    Text="Save Setting" />
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <asp:Button ID="btnClose" runat="server" Text="Close" />
            </asp:Panel>
<!-- ModalPopupExtender -->

<cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnDummy" PopupControlID="PnlModal" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:Button ID="btnDummy" runat="server" Text="Edit" Style="display: none;" />
    <%--The below panel will display as your confirm window--%>
    <asp:Panel ID="PnlModal" runat="server" Width="500px" CssClass="modalPopup" style="position: fixed; left: 0px; top: 0px; z-index: 10000; width: 1263px; height: 1202px;">
       <span style="font-size: xx-large;color: white;">Do You Want To Reload Previous Data?</span><br />
        <asp:Button ID="Button3" runat="server" Text="Yes" onclick="Button3_Click"  />
        <asp:Button ID="Button4" runat="server" Text="No" onclick="Button4_Click" />
    </asp:Panel>
        </div>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
 
    </form>

    
</asp:Content>

