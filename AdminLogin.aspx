﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminLogin.aspx.cs" Inherits="AdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>FinTester</title>
    <meta name="description" content="Hound is a Dashboard & Admin Site Responsive Template by hencework." />
    <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Hound Admin, Houndadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
    <meta name="author" content="hencework" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- vector map CSS -->
    <link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css"
        rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <!--Preloader-->
    <div class="preloader-it">
        <div class="la-anim-1">
        </div>
    </div>
    <!--/Preloader-->
    <div class="wrapper pa-0">
        <header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index-2.html">
						<img class="brand-img mr-10" src="Images/F-1.jpg" alt="brand"/>
					</a>
				</div>
			</header>
        <!-- Main Content -->
        <div class="page-wrapper pa-0 ma-0 auth-page">
            <div class="container-fluid">
                <!-- Row -->
                <div class="table-struct full-width full-height">
                    <div class="table-cell vertical-align-middle auth-form-wrap">
                        <div class="auth-form  ml-auto mr-auto no-float">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="mb-30">
                                        <h3 class="text-center txt-dark mb-10">
                                            Sign in to FinIdeas Admin</h3>
                                        <h6 class="text-center nonecase-font txt-grey">
                                            Enter your details below</h6>
                                    </div>
                                    <div class="form-wrap">
                                        <form action="#">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">
                                                Username
                                            </label>
                                            <asp:TextBox class="form-control" ID="txtUsername" placeholder="Enter Username" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">
                                                Password</label>
                                            <div class="clearfix">
                                            </div>
                                            <asp:TextBox class="form-control" ID="txtpassword" TextMode="Password" placeholder="Enter password"
                                                runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-group text-center">
                                            <asp:Button type="submit" class="btn btn-info btn-rounded" ID="btnSubmit" runat="server"
                                                Text="Sign in" onclick="btnSubmit_Click" />
                                        </div>
                                        <div class="form-group text-center">
                                            <asp:Label ID="lblMsg" runat="server" Text="" />
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->
            </div>
        </div>
        <!-- /Main Content -->
    </div>
    </form>
    <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>
    <!-- Init JavaScript -->
    <script src="dist/js/init.js"></script>
    
</body>
</html>
