﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AdminLogin : System.Web.UI.Page
{
    string sql;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (txtUsername.Text == "")
        {
            lblMsg.Text = "Username Can Not Be Blanck...";
            lblMsg.Visible = true;
        }
        else if (txtpassword.Text == "")
        {
            lblMsg.Text = "Password Can Not Be Blanck...";
            lblMsg.Visible = true;
        }
        else if (txtUsername.Text != "" && txtpassword.Text != "")
        {

            //ds = balobj.chklogin(boobj);
            if (cnnstr.State == ConnectionState.Closed)
            {
                cnnstr.Open();
            }
            sql = "select * from AdminLogin where Username ='" + txtUsername.Text + "' and Pass ='" + txtpassword.Text + "'";
            DataSet ds = new DataSet();
            SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
            adp.Fill(ds);
            cnnstr.Close();

            if (ds.Tables[0].Rows.Count == 0)
            {
                lblMsg.Text = "Invalid Username Or Password Try Again ...!";
            }
            else if (ds.Tables[0].Rows.Count != 0)
            {
                string Name = Convert.ToString(ds.Tables[0].Rows[0]["Username"].ToString());
                Session["IDName"] = Name;
                Response.Redirect("AdminHome.aspx");
            }
            else
            {
                Response.Redirect("AdminLogin.aspx");
            }
        }
        else
        {
            Response.Redirect("AdminLogin.aspx");
        }       
    }
}