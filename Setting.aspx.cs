﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Setting : System.Web.UI.Page
{
    string sql;
    string Email = "";
    string Module="MW";
    string SMModule = "SM";

   
    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();

    string UserId;

    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["FinIdeas"].ConnectionString);
    DataSet ds1 = new DataSet();

    protected void btnMarketwatch_Click(object sender, EventArgs e)
    {
        multiviewSetting.ActiveViewIndex = 0;
    }

    protected void btnSimulator_Click(object sender, EventArgs e)
    {
        multiviewSetting.ActiveViewIndex = 1;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();
        ds1 = objbal.ChkKey(UserId);
        objbol.CheckKey = ds1.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }  

        Email = Session["UserEmail"].ToString();

        if (!IsPostBack)
        {
            fillMWsetting();
            fillHideUnhideSetting();

            fillSMsetting();
            fillSMHideUnhideSetting();
        }
    }
    public void fillSMHideUnhideSetting()
    {
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + SMModule + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();
        
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='DEFAULT_HU'").ToString())
        {
            chkdef.Checked = true;
        }
        else
        {
            chkdef.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='HIDE_HU'").ToString())
        {
            chkHide.Checked = true;
        }
        else
        {
            chkHide.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKS_HU'").ToString())
        {
            chkSGreek.Checked = true;
        }
        else
        {
            chkSGreek.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKSVALUE_HU'").ToString())
        {
            chkSGreekValue.Checked = true;
        }
        else
        {
            chkSGreekValue.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='PNL_HU'").ToString())
        {
            chkpnl.Checked = true;
        }
        else
        {
            chkpnl.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='DELTAEFFECT_HU'").ToString())
        {
            chkDelEffct .Checked = true;
        }
        else
        {
            chkDelEffct.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VEGAEFFECT_HU'").ToString())
        {
            chkVegaEffct.Checked = true;
        }
        else
        {
            chkVegaEffct.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='THETAEFFECT_HU'").ToString())
        {
            chkTheEffct.Checked = true;
        }
        else
        {
            chkTheEffct.Checked = false;
        }
    }

    public void fillMWsetting()
    {
        sql = "select * from tblsetting where EmailID = '"+Email +"' and Module = '"+ Module + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql,cnnstr);
        adp.Fill(ds);

        cnnstr.Close();
        
        ddlOI.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='OI_ROUNDING'").ToString();
        ddl2ndgreek .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='2NDGREEKS_ROUNDING'").ToString();
        ddlbutterfly .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF_ROUNDING'").ToString();
        ddlButterfly2 .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF2_ROUNDING'").ToString();
        ddlcalander .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='CALENDER_ROUNDING'").ToString();
        ddlgreek .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKS_ROUNDING'").ToString();
        ddlPCP.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PCP_ROUNDING'").ToString();
        ddlpremium .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PREMIUM_ROUNDING'").ToString();
        ddlRatio .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='RATIO_ROUNDING'").ToString();
        ddlStraddle.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='STRADDLE_ROUNDING'").ToString();
        ddlVolatality.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLATALITY_ROUNDING'").ToString();
        ddlvolSpread .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLSPREAD_ROUNDING'").ToString();
        ddlVolume .SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLUME_ROUNDING'").ToString();
        txtQty.Text = ds.Tables[0].Compute("max(SettingValue)", "SettingName='Qty'").ToString();
        txtUpDownStrike.Text = ds.Tables[0].Compute("max(SettingValue)", "SettingName='UD_STRIKE'").ToString();
        
    }

    public void fillSMsetting()
    {
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + SMModule  + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();

        ddlprice.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PRICE_ROUNDING'").ToString();
        ddlpnl.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PNL_ROUNDING'").ToString();
        ddl1Greeks.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='1GREEKS_ROUNDING'").ToString();
        ddl1GreeksValue.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='1GREEKSVALUE_ROUNDING'").ToString();
        ddl2Greek.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='2GREEKS_ROUNDING'").ToString();
        ddl2GreekValue.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='2GREEKSVALUE_ROUNDING'").ToString();
        ddlSpremium.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PREMIUM_ROUNDING'").ToString();
        ddlSqty.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='QTY_ROUNDING'").ToString();
        ddlSVolatality.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLATALITY_ROUNDING'").ToString();
    }

    public void fillHideUnhideSetting()
    {   
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + Module + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();

        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='OI_HU'").ToString())
        {
            chkOI.Checked = true;
        }
        else
        {
            chkOI.Checked = false;
        }

        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='PCP_HU'").ToString())
        {
            chkpcp.Checked = true;
        }
        else
        {
            chkpcp .Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKS_HU'").ToString())
        {
            chkgreek.Checked = true;
        }
        else
        {
            chkgreek .Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='PREMIUM_HU'").ToString())
        {
            chkPremium.Checked = true;
        }
        else
        {
            chkPremium .Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='CALENDER_HU'").ToString())
        {
            chkcalander.Checked = true;
        }
        else
        {
            chkcalander.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='RATIO_HU'").ToString())
        {
            chkRatio.Checked = true;
        }
        else
        {
            chkRatio.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF_HU'").ToString())
        {
            chkButterfly.Checked = true;
        }
        else
        {
            chkButterfly.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF2_HU'").ToString())
        {
            chkButterfly2 .Checked = true;
        }
        else
        {
            chkButterfly2.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='STRADDLE_HU'").ToString())
        {
            chkStraddle .Checked = true;
        }
        else
        {
            chkStraddle.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='2NDGREEKS_HU'").ToString())
        {
            chk2ndGreek.Checked = true;
        }
        else
        {
            chk2ndGreek.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLSPREAD_HU'").ToString())
        {
            chkVolSpread .Checked = true;
        }
        else
        {
            chkVolSpread .Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOL_HU'").ToString())
        {
            chkVolatality .Checked = true;
        }
        else
        {
            chkVolatality.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLUME_HU'").ToString())
        {
            chkVolume .Checked = true;
        }
        else
        {
            chkVolume .Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='OTHERS_HU'").ToString())
        {
            chkOthers .Checked = true;
        }
        else
        {
            chkOthers.Checked = false;
        }

    }

    protected void btnSettingSave_Click(object sender, EventArgs e)
    {
       
        cnnstr.Open();
        SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" +Module +"','OI_ROUNDING','"+ ddlOI .SelectedValue +"'" ,cnnstr);
        OIcmd.ExecuteNonQuery();
        
        SqlCommand PCPcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_ROUNDING','" + ddlPCP .SelectedValue + "'", cnnstr);
        PCPcmd.ExecuteNonQuery();

        SqlCommand Greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_ROUNDING','" + ddlgreek.SelectedValue + "'", cnnstr);
        Greekcmd.ExecuteNonQuery();

        SqlCommand Premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_ROUNDING','" + ddlpremium .SelectedValue + "'", cnnstr);
        Premiumcmd.ExecuteNonQuery();

        SqlCommand Calendercmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','CALENDER_ROUNDING','" + ddlcalander .SelectedValue + "'", cnnstr);
        Calendercmd.ExecuteNonQuery();
        
        SqlCommand Ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_ROUNDING','" + ddlRatio .SelectedValue + "'", cnnstr);
         Ratiocmd.ExecuteNonQuery();
        
        SqlCommand Bfcmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','BF_ROUNDING','" + ddlbutterfly.SelectedValue + "'", cnnstr);
        Bfcmd.ExecuteNonQuery();
        
        SqlCommand Bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_ROUNDING','" + ddlButterfly2 .SelectedValue + "'", cnnstr);
        Bf2cmd.ExecuteNonQuery();
        
        SqlCommand Straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_ROUNDING','" + ddlStraddle .SelectedValue + "'", cnnstr);
        Straddlecmd.ExecuteNonQuery();
        
        SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_ROUNDING','" + ddl2ndgreek .SelectedValue + "'", cnnstr);
        ndGreekcmd.ExecuteNonQuery();

        SqlCommand VolSpreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_ROUNDING','" + ddlvolSpread.SelectedValue + "'", cnnstr);
        VolSpreadcmd.ExecuteNonQuery();
        
        SqlCommand Volatalitycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLATALITY_ROUNDING','" + ddlVolatality .SelectedValue + "'", cnnstr);
        Volatalitycmd.ExecuteNonQuery();

        SqlCommand Volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_ROUNDING','" + ddlVolume.SelectedValue + "'", cnnstr);
        Volumecmd.ExecuteNonQuery();

        SqlCommand Qtycmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','QTY','" + txtQty.Text + "'", cnnstr);
        Qtycmd.ExecuteNonQuery();

        SqlCommand UpDowncmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','UD_STRIKE','" + txtUpDownStrike.Text + "'", cnnstr);
        UpDowncmd.ExecuteNonQuery();

        cnnstr.Close();
    }

    //private string GetChecked(bool chked)
    //{
    //    if (chked)
    //    {
    //        return "1";
    //    }
    //    else
    //    {
    //        return "0";
    //    }
    //}

    protected void btnHideUnhide_Click(object sender, EventArgs e)
    {
        cnnstr.Open();

        if (chkOI.Checked == true)
        {
            SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_HU','1'", cnnstr);
            OIcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_HU','0'", cnnstr);
            OIcmd.ExecuteNonQuery();
        }
        if (chkpcp.Checked == true)
        {
            SqlCommand pcpcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_HU','1'", cnnstr);
            pcpcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand pcpcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_HU','0'", cnnstr);
            pcpcmd.ExecuteNonQuery();
        }
        if (chkgreek.Checked == true)
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_HU','1'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_HU','0'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        if (chkPremium.Checked == true)
        {
            SqlCommand premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_HU','1'", cnnstr);
            premiumcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_HU','0'", cnnstr);
            premiumcmd.ExecuteNonQuery();
        }
        if (chkcalander.Checked == true)
        {
            SqlCommand calandercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','CALENDER_HU','1'", cnnstr);
            calandercmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand calandercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','CALENDER_HU','0'", cnnstr);
            calandercmd.ExecuteNonQuery();
        }
        if (chkRatio.Checked == true)
        {
            SqlCommand ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_HU','1'", cnnstr);
            ratiocmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_HU','0'", cnnstr);
            ratiocmd.ExecuteNonQuery();
        }
        if (chkButterfly .Checked == true)
        {
            SqlCommand bfcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF_HU','1'", cnnstr);
            bfcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand bfcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF_HU','0'", cnnstr);
            bfcmd.ExecuteNonQuery();
        }
        if (chkStraddle .Checked == true)
        {
            SqlCommand straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_HU','1'", cnnstr);
            straddlecmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_HU','0'", cnnstr);
            straddlecmd.ExecuteNonQuery();
        }
        if (chk2ndGreek.Checked == true)
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','1'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','0'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
         if (chk2ndGreek.Checked == true)
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','1'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','0'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
         if (chkVolSpread .Checked == true)
         {
             SqlCommand volspreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_HU','1'", cnnstr);
             volspreadcmd.ExecuteNonQuery();
         }
         else
         {
             SqlCommand volspreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_HU','0'", cnnstr);
             volspreadcmd.ExecuteNonQuery();
         }
         if (chkVolatality.Checked == true)
         {
             SqlCommand volcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOL_HU','1'", cnnstr);
             volcmd.ExecuteNonQuery();
         }
         else
         {
             SqlCommand volcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOL_HU','0'", cnnstr);
             volcmd.ExecuteNonQuery();
         }
         if (chkVolume .Checked == true)
         {
             SqlCommand volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_HU','1'", cnnstr);
             volumecmd.ExecuteNonQuery();
         }
         else
         {
             SqlCommand volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_HU','0'", cnnstr);
             volumecmd.ExecuteNonQuery();
         }
         if (chkButterfly2.Checked == true)
         {
             SqlCommand bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_HU','1'", cnnstr);
             bf2cmd.ExecuteNonQuery();
         }
         else
         {
             SqlCommand bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_HU','0'", cnnstr);
             bf2cmd.ExecuteNonQuery();
         }
         if (chkOthers.Checked == true)
         {
             SqlCommand othercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OTHER_HU','1'", cnnstr);
             othercmd.ExecuteNonQuery();
         }
         else
         {
             SqlCommand othercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OTHER_HU','0'", cnnstr);
             othercmd.ExecuteNonQuery();
         }
        cnnstr.Close();

    }

    protected void btnSaveManual_Click(object sender, EventArgs e)
    {
        cnnstr.Open();
        SqlCommand pricecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PRICE_ROUNDING','" + ddlprice.SelectedValue + "'", cnnstr);
        pricecmd.ExecuteNonQuery();

        SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_ROUNDING','" + ddlpnl.SelectedValue + "'", cnnstr);
        pnlcmd.ExecuteNonQuery();

        SqlCommand FsGrkcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','1GREEKS_ROUNDING','" + ddl1Greeks.SelectedValue + "'", cnnstr);
        FsGrkcmd.ExecuteNonQuery();

        SqlCommand FsGrkValCmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','1GREEKSVALUE_ROUNDING','" + ddl1GreeksValue.SelectedValue + "'", cnnstr);
        FsGrkValCmd.ExecuteNonQuery();

        SqlCommand NdGrkcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','2GREEKS_ROUNDING','" + ddl2Greek.SelectedValue + "'", cnnstr);
        NdGrkcmd.ExecuteNonQuery();

        SqlCommand NdGrkValCmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','2GREEKSVALUE_ROUNDING','" + ddl2GreekValue.SelectedValue + "'", cnnstr);
        NdGrkValCmd.ExecuteNonQuery();

        SqlCommand Premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PREMIUM_ROUNDING','" + ddlSpremium.SelectedValue + "'", cnnstr);
        Premiumcmd.ExecuteNonQuery();

        SqlCommand sVolatalitycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VOLATALITY_ROUNDING','" + ddlSVolatality.SelectedValue + "'", cnnstr);
        sVolatalitycmd.ExecuteNonQuery();

        SqlCommand Sqtycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','QTY_ROUNDING','" + ddlSqty.SelectedValue + "'", cnnstr);
        Sqtycmd.ExecuteNonQuery();

        cnnstr.Close();

    }

    protected void btnHUSetting_Click(object sender, EventArgs e)
    {
        cnnstr.Open();

        if (chkdef.Checked == true)
        {
            SqlCommand defcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DEFAULT_HU','1'", cnnstr);
            defcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand defcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DEFAULT_HU','0'", cnnstr);
            defcmd.ExecuteNonQuery();
        }
        if (chkHide.Checked == true)
        {
            SqlCommand Hidecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','Hide_HU','1'", cnnstr);
            Hidecmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand Hidecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','Hide_HU','0'", cnnstr);
            Hidecmd.ExecuteNonQuery();
        }

        if (chkSGreek.Checked == true)
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKS_HU','1'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand greekcmd= new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKS_HU','0'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        if (chkSGreekValue.Checked == true)
        {
            SqlCommand Sgreekvalcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKSVALUE_HU','1'", cnnstr);
            Sgreekvalcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand Sgreekvalcmd= new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKSVALUE_HU','0'", cnnstr);
            Sgreekvalcmd.ExecuteNonQuery();    
        }
        if (chkpnl.Checked == true)
        {
            SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_HU','1'", cnnstr);
            pnlcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand pnlcmd= new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_HU','0'", cnnstr);
            pnlcmd.ExecuteNonQuery();
        }
        if (chkDelEffct.Checked == true)
        {
            SqlCommand deltacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DELTAEFFECT_HU','1'", cnnstr);
            deltacmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand deltacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DELTAEFFECT_HU','0'", cnnstr);
            deltacmd.ExecuteNonQuery();
        }
        if (chkVegaEffct.Checked == true)
        {
            SqlCommand vegacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VEGAEFFECT_HU','1'", cnnstr);
            vegacmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand vegacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VEGAEFFECT_HU','0'", cnnstr);
            vegacmd.ExecuteNonQuery();
        }
        if (chkTheEffct.Checked == true)
        {
            SqlCommand Thetacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','THETAEFFECT_HU','1'", cnnstr);
            Thetacmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand Thetacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','THETAEFFECT_HU','0'", cnnstr);
            Thetacmd.ExecuteNonQuery();
        }
        
        cnnstr.Close();
    }
}