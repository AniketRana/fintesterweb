﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MarketWatch.aspx.cs" Inherits="MarketWatch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <Triggers>  
    <asp:PostBackTrigger ControlID="btnHideUnhide" />   
</Triggers> 
   <ContentTemplate>

   <link href="dist/css/styleContext.css" rel="stylesheet" type="text/css" />
    <script src="dist/js/Script.js" type="text/javascript" language="javascript"></script>
    <script src="dist/js/jscolor.js" type="text/javascript" language="javascript"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js" type = "text/javascript" language="javascript"></script>

       
    <style type="text/css">
    .Calendar .ajax__calendar_body 
    {
      
      background-color:White;
      
    }
    .Calendar .ajax__calendar_header
    {
        background-color:#bc5656;
        width:170px;
        color:White;
    }
    
    .dataTable
    {
        width:auto;
    } 
    
    .btnhover:hover
    {
        background-color:#f9b6b6 !important;
        -webkit-text-fill-color: black !important;
    }
    .rdo
    {
        color:#bc5656;
        margin-left: 20px;
        font-weight:bold;
    }
    .dataTables tbody tr {
min-height: 35px; /* or whatever height you need to make them all consistent */
}


.important
{
    /*-webkit-text-fill-color:Red !important;*/
    background-color:#ea3a93 !important;
   font-size:medium;
   font-weight:bold;
}

.call
{
    background-color:#dcbfda7a;
}
.put
{
    background-color:#c2da947a;
}

td.selected {
    background-color: #FFCF8B
}


   </style>

<div class="page-wrapper">
<div class="container-fluid">
				
				<!-- Title -->
				<%--<div class="row heading-bg" style="padding: unset;">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Trading Day</h5>
    
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="Dashboard.aspx">Dashboard</a></li>
						<li><a href="#"><span>table</span></a></li>
						<li class="active"><span>MarketWatch</span></li>
					  </ol>
					</div>
					
				</div>--%>
				<!-- /Title -->
				
				<div class="row">
    <div class="col-lg-12">
						<div class="panel panel-default card-view">
							<div class="MWpanel-heading">
								
        
        
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body" visible="False">
									<p class="text-muted"><code></code><code> </code></p>
                                    
                                   
									<div class="table-wrap mt-40">
                                    <div style="color: #000000;width: 1071px;""><span class="custom-dropdown">Symbol
                                    <asp:DropDownList runat="server" 
                                            style="padding-bottom: 4px;padding-top: 4px;padding-left: 10px;margin-left: 5px;" 
                                            id="dropdown" DataSourceID="LinqDataSource1" DataTextField="Security_Symbol" 
                                            DataValueField="Security_Symbol" EnableViewState="True" 
                                            AutoPostBack="True" onselectedindexchanged="dropdown_SelectedIndexChanged">
                                    </asp:DropDownList>
                                                    
                                                <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
                                            ContextTypeName="Linq.SettingDataLinqDataContext" EntityTypeName="" 
                                            GroupBy="Security_Symbol" OrderBy="Security_Symbol" 
                                            Select="new (key as Security_Symbol, it as tblSymbols)" TableName="tblSymbols">
                                        </asp:LinqDataSource>
                                                    
                                                </span>
                                    <p  id="rect"style="float:right"><asp:Button
                                                    ID="Button3" runat="server" Text="<<" 
                                                class="btn btn-default  btn-xs btn btn-warning btnhover" style="
    background-color: #bc5656;
    border-color: #bc5656;
    /* color: white; */
    -webkit-text-fill-color: white;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
    padding-left: 0px;
    border-top-width: 0px;
    border-bottom-width: 0px;
    border-left-width: 0px;
    border-right-width: 0px;
    height: 28px;
    margin-bottom: 3px;
    width: 71px;
" onclick="Button3_Click" />
                                            <asp:TextBox ID="txtDate" style="
    margin-top: 5px;
    height: 28px;
    padding-left: 25px;
    width: 134px;
    background-color: #f9b6b6;
    color: black;
    font-weight: 500;
    border-top-width: 0px;
    border-left-width: 0px;
    border-bottom-width: 0px;
    border-right-width: 0px;
    margin-right: 3px;
" runat="server" AutoPostBack="True" ontextchanged="txtDate_TextChanged" class="readonly"></asp:TextBox><asp:ImageButton
    ID="ImageButton1" runat="server" ImageUrl="~/Images/Calendaricon-300x298.png" ImageAlign="AbsMiddle" style="
    width: 27px;
    margin-bottom: 5px;
    margin-right: 3px;
"/><asp:Button
                                                    ID="Button2" runat="server" Text=">>" 
                                                class="btn btn-default  btn-xs btn btn-warning btnhover " style="
    background-color: #bc5656;
    border-color: #bc5656;
    /* color: white; */
    -webkit-text-fill-color: white;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
    padding-left: 0px;
    border-top-width: 0px;
    border-bottom-width: 0px;
    border-right-width: 0px;
    border-left-width: 0px;
    height: 28px;
    margin-bottom: 3px;
    width: 71px;
" onclick="Button2_Click" />
<cc1:CalendarExtender ID="Calendar1" PopupButtonID="ImageButton1" runat="server" TargetControlID="txtDate"
    Format="dd-MMM-yyyy" PopupPosition="TopRight" ClientIDMode="Inherit" Animated="True" CssClass="Calendar">
</cc1:CalendarExtender> 
                                        <asp:TextBox ID="txtSkip" runat="server" style="
    width: 34px;
    height: 27px;
    margin-left: 20px;
    background-color: #f9b6b6;
    /* border-bottom-style: initial; */
    border-color: #bc5656;
    padding-left: 6px;
" Text="1" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                       <panel class="custom-dropdown" style="margin-bottom: 7px;height: 29px;";"> 
                                           <asp:Label runat="server" style="margin-left: 111px;" Text="Expiry Dates : "></asp:Label>
                                           <asp:DropDownList runat="server"  ID="ddExpiry" AutoPostBack="True" 

                                               onselectedindexchanged="ddExpiry_SelectedIndexChanged" style="height: 29px;padding-top: 0px;padding-bottom: 0px;"></asp:DropDownList>  </panel>  </p>
                                               <p style="
    width: 1163px;
">
                                                  <asp:Label ID="Label1" class="btn btn-default  btn-xs btn btn-warning" style="background-color: #8a2121;border-color: #8a2121;-webkit-text-fill-color: white;font-weight: 600;white-space:nowrap;cursor: text;font-size: inherit;" runat="server" Text="Label">Expiry Date  -  </asp:Label>    <asp:Label ID="lblExpiry" class="btn btn-default  btn-xs btn btn-warning" runat="server" style="background-color: #8a2121;border-color: #8a2121;-webkit-text-fill-color: white;font-weight: 600;white-space:nowrap;cursor: text;font-size: inherit;
" Text="Expiry Date"></asp:Label><asp:Label ID="Label2" class="btn btn-default  btn-xs btn btn-warning" style="background-color: #8a2121;border-color: #8a2121;-webkit-text-fill-color: white;font-weight: 600;white-space:nowrap; margin-left:20px;cursor: text;font-size: inherit;" runat="server" Text="Label">Future Price  -  </asp:Label><asp:Label ID="lblFuture" class="btn btn-default  btn-xs btn btn-warning" runat="server" Text="Label" style="background-color: #8a2121;border-color: #8a2121;-webkit-text-fill-color: white;font-weight: 600;white-space:nowrap;margin-left:3px;cursor: text;font-size: inherit;
"></asp:Label><asp:Label ID="Label3" class="btn btn-default  btn-xs btn btn-warning" style="background-color: #8a2121;border-color: #8a2121;-webkit-text-fill-color: white;font-weight: 600;white-space:nowrap; margin-left:20px;cursor: text;font-size: inherit;" runat="server" Text="Label">Change  -  </asp:Label><asp:Label ID="lblchange" class="btn btn-default  btn-xs btn btn-warning" runat="server" Text="Label" style="background-color: #8a2121;border-color: #8a2121;-webkit-text-fill-color: white;font-weight: 600;white-space:nowrap;margin-left:3px;cursor: text;font-size: inherit;
"></asp:Label><a id="btnExport" class="btn btn-default  btn-xs btn btn-warning btnhove" tabindex="0" style="margin-left: 20px;" onclick="tableToExcel('tableID', 'MarketWatch')" href="#"><span>Export To Excel</span></a><a id="btn" class="btn btn-default  btn-xs btn btn-warning btnhove" style="margin-left: 20px;" href="javascript: void(0);">Setting</a><asp:Label ID="lblAtm" runat="server" style = "display:none" Text="Label"></asp:Label>
                                            <%--<input class="jscolor {onFineChange:'update(this)'}" value="cc66ff" style="background-image: none;background-color: rgb(204, 102, 255);color: rgb(0, 0, 0);width: 24px;height: 25px;float: right; margin-left: 5px;"> <input class="jscolor {onFineChange:'update1(this)'}" value="cc66ff" style="background-image: none;background-color: rgb(204, 102, 255);color: rgb(0, 0, 0);width: 24px;height: 25px;float: right;">--%> </p>
                                               
                                </div>
                               
                                
										    <div class="MWtable-responsive">
                                              
											<table id="tableID" class="table mb-0 tableclass cell-border">
												<thead class="MWtbl-header">
												  <tr>
													<th>Strike</th>
													<th>C-Volga</th>
													<th>C-Vanna</th>
													<th>C-Gamma</th>
													<th>C-BF</th>
													<th>C-BF3</th>
                                                    <th>C-BF2</th>
													<th>C-Cal</th>
													<th>C-Theta</th>
													<th>C-Vega</th>
													<th>C-Delta</th>
													<th>C-OI</th>
                                                    <th>&Delta;C-OI</th>
													<th>PCP</th>
													<th>&Delta;PCP</th>
													<th>&Delta;C-LTP</th>
													<th>C-LTP</th>
													<th>C-VR</th>
                                                    <th>C-DR</th>
													<th>C-PR</th>
													<th>STRADDLE</th>
													<th>&Delta;C-IV%</th>
													<th>C-IV%</th>
													<th>C2C</th>
                                                    <th>&Delta;C2C</th>
													<th>C-Vol Diff</th>
													<th>C-Vol RR</th>
													<th>C-Vegga RR</th>
													<th>C-NO OF CONT</th>
                                                    <th style="width: 54px;display: none;">Strike 2</th>
													<th style="width: 54px;display: none;">Strike 3</th>
													<th>P-Gamma</th>
													<th>P-Vanna</th>
                                                    <th>P-Volga</th>
													<th>P-BF</th>
													<th>P-BF2</th>
													<th>P-BF3</th>
													<th>P-Cal</th>
                                                    <th>P-Delta</th>
													<th>P-Vega</th>
													<th>P-Theta</th>
													<th>&Delta;P-OI</th>
                                                    <th>P-OI</th>
                                                    <th>&Delta;T-OI</th>
													<th>T-OI</th>
													<th>P-LTP</th>
													<th>&Delta;P-LTP</th>
													<th>P-PR</th>
                                                    <th>P-DR</th>
													<th>P-VR</th>
													<th>P-IV %</th>
													<th>&Delta;P-IV %</th>
                                                    <th>&Delta;P2P</th>
													<th>C2P</th>
													<th>&Delta;C2P</th>
													<th>P2P</th>
													<th>P Vol Diff</th>
                                                    <th>P Vol RR</th>
													<th>P Vegga RR</th>
													<th>P No of Cont</th>
													
								                    
												  </tr>
												</thead>
												<tbody class="MWtbl-content">
                                                <asp:Repeater ID="RepeaterInner" runat="server">
                    <ItemTemplate>
												  <tr class="trhover tblbody">
													<td  style="background-color:Black;color: white;"><%# Convert.ToDouble(Eval("Strike")).ToString("N2")%></td>
													<td class = "call Context"><%# Math.Round(Convert.ToDouble(Eval("C-Volga")), R_2ndgreeks).ToString("N" + R_2ndgreeks + "")%></td>
													<td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-Vanna")), R_2ndgreeks).ToString("N" + R_2ndgreeks + "")%> </td>
													<td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-Gamma")), R_2ndgreeks).ToString("N" + R_2ndgreeks + "")%></td>
													<td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-BF")), R_Butterfly).ToString("N" + R_Butterfly + "")%></td>
													<td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-BF3")), R_Butterfly2).ToString("N" + R_Butterfly2 + "")%></td>
                                                    <td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-BF2")), R_Butterfly2).ToString("N" + R_Butterfly2 + "")%></td>
													<td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-Cal")), R_Calendar).ToString("N" + R_Calendar + "")%></td>
													<td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-Theta")), R_Greeks).ToString("N" + R_Greeks + "")%> </td>
													<td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-Vega")), R_Greeks).ToString("N" + R_Greeks + "")%></td>
													<td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-Delta")), R_Greeks).ToString("N" + R_Greeks + "")%></td>
													<td class = "call"><%# (Math.Round((Convert.ToDouble(Eval("C-OI"))/R_OI),0) * R_OI)%></td>
                                                    <td class = "call"><%# (Math.Round((Convert.ToDouble(Eval("? C-OI")) / R_OI), 0) * R_OI)%></td>                                                   
													<td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("PCP")), R_PCP).ToString("N" + R_PCP + "")%> </td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("?PCP")), R_PCP).ToString("N" + R_PCP + "")%> </td>
													<td class = "call"><%# Math.Round(Convert.ToDouble(Eval("? C-LTP")), R_Premium).ToString("N" + R_Premium + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-LTP")),R_Premium).ToString("N" + R_Premium + "")%> </td>
													<td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-VR")), R_Ratio).ToString("N" + R_Ratio + "")%></td>
													<td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-DR")), R_Ratio).ToString("N" + R_Ratio + "")%></td>
                                                    <td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("C-PR")), R_Ratio).ToString("N" + R_Ratio + "")%></td>
                                                    <td class = "context-menu-one call"><%# Math.Round(Convert.ToDouble(Eval("STRADDLE")), R_Straddle).ToString("N" + R_Straddle + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("? C-IV %")), R_Volatility).ToString("N" + R_Volatility + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-IV %")), R_Volatility).ToString("N" + R_Volatility + "")%></td>                                            
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C2C")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("?C2C ")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("CVol Diff")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("CVol RR")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("CVega RR")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td> 
                                                    <td class = "call"><%# Math.Round(Convert.ToDouble(Eval("C-No of Cont")), R_Volume).ToString("N" + R_Volume + "")%></td>                                                                                                                                       
                                                    <td style="background-color: rgba(0, 0, 0, 0.65);color: white;display: none;"><%# Convert.ToDouble(Eval("Strike2")).ToString("N2")%></td>
                                                    <td style="background-color: rgba(0, 0, 0, 0.65);color: white;display: none;"><%# Convert.ToDouble(Eval("Strike3")).ToString("N2")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-Gamma")), R_2ndgreeks).ToString("N" + R_2ndgreeks + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-Vanna")), R_2ndgreeks).ToString("N" + R_2ndgreeks + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-Volga")), R_2ndgreeks).ToString("N" + R_2ndgreeks + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-BF")), R_Butterfly).ToString("N" + R_Butterfly + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-BF2")), R_Butterfly2).ToString("N" + R_Butterfly + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-BF3")), R_Butterfly2).ToString("N" + R_Butterfly + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-Cal")), R_Calendar).ToString("N" + R_Calendar + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-Delta")), R_Greeks).ToString("N" + R_Greeks + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-Vega")), R_Greeks).ToString("N" + R_Greeks + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-Theta")), R_Greeks).ToString("N" + R_Greeks + "")%></td>
                                                    <td class = "put"><%# (Math.Round((Convert.ToDouble(Eval("? P-OI")) / R_OI), 0) * R_OI)%></td>
                                                    <td class = "put"><%# (Math.Round((Convert.ToDouble(Eval("P-OI")) / R_OI), 0) * R_OI)%></td>
                                                    <td class = "put"><%# (Math.Round((Convert.ToDouble(Eval("? T-OI")) / R_OI), 0) * R_OI)%></td>
                                                    <td class = "put"><%# (Math.Round((Convert.ToDouble(Eval("T-OI")) / R_OI), 0) * R_OI)%></td>    
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-LTP")), R_Premium).ToString("N" + R_Premium + "")%></td>                                           
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("? P-LTP")), R_Premium).ToString("N" + R_Premium + "")%></td>                                                     
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-PR")), R_Ratio).ToString("N" + R_Ratio + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-DR")), R_Ratio).ToString("N" + R_Ratio + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-VR")), R_Ratio).ToString("N" + R_Ratio + "")%></td>
                                                    <td class = "context-menu-one put"><%# Math.Round(Convert.ToDouble(Eval("P-IV %")), R_Volatility).ToString("N" + R_Volatility + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("? P-IV %")), R_Volatility).ToString("N" + R_Volatility + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("?P2P")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("C2P")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("?C2P")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>                                                    
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P2P")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("PVol Diff")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("PVol RR")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("PVega RR")), R_VolSpreads).ToString("N" + R_VolSpreads + "")%></td>
                                                    <td class = "put"><%# Math.Round(Convert.ToDouble(Eval("P-No Of Cont")), R_Volume).ToString("N" + R_Volume + "")%></td>
                                                    
                                                    
												  </tr>
                                                   </ItemTemplate>
                </asp:Repeater>
												  
												</tbody>
											</table>
										</div>
                                        
									</div>
                                   
								</div>
							</div>
						</div>


                        <div class="modal fade" id="modelWindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">Setting
                        
                         <div class="row" style="margin-left: 51px;margin-right: 7px;">
                        <div class="col-md-6">
                             <div class="panel panel-default card-view" style="height: 493px;float: right;">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">
                                            Manual Setting
                                        </h6>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row mt-00">
                                            <div class="col-sm-12">
                                                <div style = "border:2px;">
                                                    <p>
                                                        <p>
                                                            <asp:DropDownList ID="ddlOI" runat="server" align="right" Width="50px">
                                                                <asp:ListItem>-3</asp:ListItem>
                                                                <asp:ListItem>-2</asp:ListItem>
                                                                <asp:ListItem>-1</asp:ListItem>
                                                                <asp:ListItem Selected="True">0</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : Open Interest
                                                            </label>
                                                        </p>
                                                        <p>
                                                               <asp:DropDownList ID="ddlPCP" runat="server" align="right" Width="50px">
                                                                <asp:ListItem>0</asp:ListItem>
                                                                <asp:ListItem>1</asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>3</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label>
                                                                : PCP
                                                            </label>
                                                        </p>
                                                        <p>
                                                        </p>
                                                        <p>
                                                            <p>
                                                                <asp:DropDownList ID="ddlgreek" runat="server" Width="50px">
                                                                    <asp:ListItem>0</asp:ListItem>
                                                                    <asp:ListItem>1</asp:ListItem>
                                                                    <asp:ListItem>2</asp:ListItem>
                                                                    <asp:ListItem>3</asp:ListItem>
                                                                    <asp:ListItem>4</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <label>
                                                                : Greeks
                                                                </label>
                                                            </p>
                                                            <p>
                                                                <asp:DropDownList ID="ddlpremium" runat="server" align="right" Width="50px">
                                                                    <asp:ListItem>0</asp:ListItem>
                                                                    <asp:ListItem>1</asp:ListItem>
                                                                    <asp:ListItem>2</asp:ListItem>
                                                                    <asp:ListItem>3</asp:ListItem>
                                                                    <asp:ListItem>4</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <label>
                                                                : Premium
                                                                </label>
                                                            </p>
                                                            <p>
                                                            </p>
                                                            <p>
                                                                <p>
                                                                    <asp:DropDownList ID="ddlcalander" runat="server" align="right" Width="50px">
                                                                        <asp:ListItem>0</asp:ListItem>
                                                                        <asp:ListItem>1</asp:ListItem>
                                                                        <asp:ListItem>2</asp:ListItem>
                                                                        <asp:ListItem>3</asp:ListItem>
                                                                        <asp:ListItem>4</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <label>
                                                                    : Calander
                                                                    </label>
                                                                </p>
                                                                <p>
                                                                    <asp:DropDownList ID="ddlRatio" runat="server" align="right" Width="50px">
                                                                        <asp:ListItem>0</asp:ListItem>
                                                                        <asp:ListItem>1</asp:ListItem>
                                                                        <asp:ListItem>2</asp:ListItem>
                                                                        <asp:ListItem>3</asp:ListItem>
                                                                        <asp:ListItem>4</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <label>
                                                                    : Ratio
                                                                    </label>
                                                                </p>
                                                                <p>
                                                                </p>
                                                                <p>
                                                                    <p>
                                                                        <asp:DropDownList ID="ddlbutterfly" runat="server" Width="50px">
                                                                            <asp:ListItem>0</asp:ListItem>
                                                                            <asp:ListItem>1</asp:ListItem>
                                                                            <asp:ListItem>2</asp:ListItem>
                                                                            <asp:ListItem>3</asp:ListItem>
                                                                            <asp:ListItem>4</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <label>
                                                                        : Butterfly
                                                                        </label>
                                                                    </p>
                                                                    <p>
                                                                        <asp:DropDownList ID="ddlStraddle" runat="server" align="right" Width="50px">
                                                                            <asp:ListItem>0</asp:ListItem>
                                                                            <asp:ListItem>1</asp:ListItem>
                                                                            <asp:ListItem>2</asp:ListItem>
                                                                            <asp:ListItem>3</asp:ListItem>
                                                                            <asp:ListItem>4</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <label>
                                                                        : Straddle
                                                                        </label>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                        <p>
                                                                            <asp:DropDownList ID="ddl2ndgreek" runat="server" align="right" Width="50px">
                                                                                <asp:ListItem>0</asp:ListItem>
                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                <asp:ListItem>4</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <label>
                                                                            : 2nd Greeks
                                                                            </label>
                                                                        </p>
                                                                        <p>
                                                                            <asp:DropDownList ID="ddlvolSpread" runat="server" align="right" Width="50px">
                                                                                <asp:ListItem>0</asp:ListItem>
                                                                                <asp:ListItem>1</asp:ListItem>
                                                                                <asp:ListItem>2</asp:ListItem>
                                                                                <asp:ListItem>3</asp:ListItem>
                                                                                <asp:ListItem>4</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <label>
                                                                            : Vol Spread
                                                                            </label>
                                                                        </p>
                                                                        <p>
                                                                        </p>
                                                                        <p>
                                                                            <p>
                                                                                <asp:DropDownList ID="ddlVolatality" runat="server" align="right" Width="50px">
                                                                                    <asp:ListItem>0</asp:ListItem>
                                                                                    <asp:ListItem>1</asp:ListItem>
                                                                                    <asp:ListItem>2</asp:ListItem>
                                                                                    <asp:ListItem>3</asp:ListItem>
                                                                                    <asp:ListItem>4</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <label>
                                                                                : Volatality
                                                                                </label>
                                                                            </p>
                                                                            <p>
                                                                                <asp:DropDownList ID="ddlVolume" runat="server" align="right" Width="50px">
                                                                                    <asp:ListItem>0</asp:ListItem>
                                                                                    <asp:ListItem>1</asp:ListItem>
                                                                                    <asp:ListItem>2</asp:ListItem>
                                                                                    <asp:ListItem>3</asp:ListItem>
                                                                                    <asp:ListItem>4</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <label>
                                                                                : Volume
                                                                                </label>
                                                                            </p>
                                                                            <p>
                                                                            </p>
                                                                            <p>
                                                                                <p>
                                                                                    <asp:DropDownList ID="ddlButterfly2" runat="server" align="right" Width="50px">
                                                                                        <asp:ListItem>0</asp:ListItem>
                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <label>
                                                                                    : Butterfly 2
                                                                                    </label>
                                                                                </p>
                                                                                <p>
                                                                                    <asp:TextBox ID="txtQty" runat="server" Height="20px" Width="80px">0</asp:TextBox>
                                                                                    <label>
                                                                                    : Qty
                                                                                    </label>
                                                                                </p>
                                                                                <p>
                                                                                </p>
                                                                                <p>
                                                                                    <p>
                                                                                        <asp:TextBox ID="txtUpDownStrike" runat="server" Height="20px" Width="80px">0</asp:TextBox>
                                                                                        <label for="checkbox-9">
                                                                                        : Up-Down Strike
                                                                                        </label>
                                                                                    </p>
                                                                                    <p>
                                                                                        <asp:Button ID="btnSettingSave" runat="server" class="btn btn-success btn-anim" 
                                                                                            OnClick="btnSettingSave_Click" Text="Save Setting " />
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                </p>
                                                                            </p>
                                                                        </p>
                                                                    </p>
                                                                </p>
                                                            </p>
                                                        </p>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default card-view" style="float: left;">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">
                                            Hide &amp; Unhide Setting</h6>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row mt-00">
                                            <div class="col-sm-12">
                                                <div class="checkbox checkbox-primary checkbox-circle">
                                                     <div style = "border:2px;">
                                                        <p>
                                                            <p>
                                                                <asp:CheckBox ID="chkOI" runat="server" />
                                                                <label for="checkbox-9">
                                                                    Open Interest
                                                                </label>
                                                            </p>
                                                            <p>
                                                               
                                                                <asp:CheckBox ID="chkpcp" runat="server" />
                                                                <label for="checkbox-9">
                                                                    PCP
                                                                </label>
                                                            </p>
                                                            <p>
                                                            </p>
                                                            <p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkgreek" runat="server" />
                                                                    <label for="checkbox-9">
                                                                    Greeks
                                                                    </label>
                                                                </p>
                                                                <p>
                                                                    <asp:CheckBox ID="chkPremium" runat="server" />
                                                                    <label for="checkbox-9">
                                                                    Premium
                                                                    </label>
                                                                </p>
                                                                <p>
                                                                </p>
                                                                <p>
                                                                    <p>
                                                                        <asp:CheckBox ID="chkcalander" runat="server" />
                                                                        <label for="checkbox-9">
                                                                        Calender
                                                                        </label>
                                                                    </p>
                                                                    <p>
                                                                        <asp:CheckBox ID="chkRatio" runat="server" />
                                                                        <label for="checkbox-9">
                                                                        Ratio
                                                                        </label>
                                                                    </p>
                                                                    <p>
                                                                    </p>
                                                                    <p>
                                                                        <p>
                                                                            <asp:CheckBox ID="chkButterfly" runat="server" />
                                                                            <label for="checkbox-9">
                                                                            Butterfly
                                                                            </label>
                                                                        </p>
                                                                        <p>
                                                                            <asp:CheckBox ID="chkStraddle" runat="server" />
                                                                            <label for="checkbox-9">
                                                                            Straddle
                                                                            </label>
                                                                        </p>
                                                                        <p>
                                                                        </p>
                                                                        <p>
                                                                            <p>
                                                                                <asp:CheckBox ID="chk2ndGreek" runat="server" />
                                                                                <label for="checkbox-9">
                                                                                2nd Greeks
                                                                                </label>
                                                                            </p>
                                                                            <p>
                                                                                <asp:CheckBox ID="chkVolSpread" runat="server" />
                                                                                <label for="checkbox-9">
                                                                                Vol Spread
                                                                                </label>
                                                                            </p>
                                                                            <p>
                                                                            </p>
                                                                            <p>
                                                                                <p>
                                                                                    <asp:CheckBox ID="chkVolatality" runat="server" />
                                                                                    <label for="checkbox-9">
                                                                                    Volatility
                                                                                    </label>
                                                                                </p>
                                                                                <p>
                                                                                    <asp:CheckBox ID="chkVolume" runat="server" />
                                                                                    <label for="checkbox-9">
                                                                                    Volume
                                                                                    </label>
                                                                                </p>
                                                                                <p>
                                                                                </p>
                                                                                <p>
                                                                                    <p>
                                                                                        <asp:CheckBox ID="chkButterfly2" runat="server" />
                                                                                        <label for="checkbox-9">
                                                                                        Butterfly 2
                                                                                        </label>
                                                                                    </p>
                                                                                    <p>
                                                                                        <asp:CheckBox ID="chkOthers" runat="server" />
                                                                                        <label for="checkbox-9">
                                                                                        Others
                                                                                        </label>
                                                                                    </p>
                                                                                    <p>
                                                                                    </p>
                                                                                    <p>
                                                                                        <p>
                                                                                            <asp:Button ID="btnHideUnhide" runat="server" class="btn btn-success btn-anim" 
                                                                                                OnClick="btnHideUnhide_Click" Text="Save Setting " />
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                    </p>
                                                                                </p>
                                                                            </p>
                                                                        </p>
                                                                    </p>
                                                                </p>
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        </div>
					</div>
                    </div>
                </div>
            </div>
            <div class="menu">
                <ul id="top4list">
                    <a id="linkLong" target="_blank">
                        <li>Long</li></a> <a id="linkShort" target="_blank">
                            <li>Short</li></a>
                </ul>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
      

Sys.Application.add_load(function () {
    var table = $('#tableID').dataTable({
        "scrollY": "650px",
        "scrollX": true,
        "paging": false,
        "ordering": false,
        searching: false,
        "info": false,
//       stateSave: true,
        colReorder:true,
        fixedColumns:   {
            leftColumns: 1
        },

       "createdRow": function( row, data, dataIndex ) {
    if ( data[0] == $("[id$=lblAtm]").text()) { 
      $(row).addClass( 'important' );
    }
  }
         
    });

    $("#tableID,td").contextmenu(function(){
    $(this).addClass("selected").siblings().removeClass("selected");
});

$("#tableID,td").dblclick(function(){
    $(this).addClass("selected").siblings().removeClass("selected");
});

 $("#tableID,td").click(function(){
    $(this).removeClass("selected");
});

        
   

//    $('a.toggle-vis').on('click', function (e) {
//        e.preventDefault();

//        // Get the column API object
//        var column = table.column($(this).attr('data-column'));

//        // Toggle the visibility
//        column.visible(!column.visible());
//    });

   $('table').on('contextmenu', 'td', function (e) {
             var row = e.delegateTarget.tHead.rows[0].cells[this.cellIndex],
        column = this.parentNode.cells[0];
       
         var trdDate = $("input[id$=txtDate]").val(); //get date text box value using jQuery
         var expDate = $("[id$=lblExpiry]").text();   //get lable value using jquery
         var symbol = $("[id$=dropdown]").val();
         var spotprice = $("[id$=lblFuture]").text(); 
             
             var LinkLong = "Simulator.aspx?Long=" + this.innerHTML + "&Strategy=" +$(row).text() +"&Strike=" + $(column).text() + "&TrdDate=" +trdDate + "&ExpDate=" +expDate + "&symbol=" +symbol +"&LS=Long" + "&Spot=" +spotprice;
             var LinkShort = "Simulator.aspx?Short=" + this.innerHTML + "&Strategy=" +$(row).text() + "&Strike=" + $(column).text() + "&TrdDate=" +trdDate + "&ExpDate=" +expDate + "&symbol=" +symbol +"&LS=Short" + "&Spot=" +spotprice;
             document.getElementById("linkLong").setAttribute("href", LinkLong);
             document.getElementById("linkShort").setAttribute("href", LinkShort);
         
           
         });

    $(".readonly").keydown(function(e){
        e.preventDefault();
    });

     $("#panel").hide();  

     $('#click').click(function()
{   
    $("#panel").toggle();     
});

$('#btn').click(function() {
   $('#modelWindow').modal('show');
});


$("#btnExport").click(function () {
        var symbol = $("[id$=dropdown]").val();
    var todaysDate = moment().format('DD-MM-YYYY HH:mm:ss');
    var blobURL = tableToExcel('tableID', 'test_table');
    $(this).attr('download','MarketWatch_'+symbol+'_'+todaysDate+'.xls')
    $(this).attr('href',blobURL);
});

var tableToExcel = (function() {
        var symbol = $("[id$=dropdown]").val();
      var trdDate = $("input[id$=txtDate]").val();
      var symdt = symbol+'_'+trdDate;
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: symdt || 'Worksheet', table: table.innerHTML}
    var blob = new Blob([format(template, ctx)]);
  var blobURL = window.URL.createObjectURL(blob);
    return blobURL;
  }
})()


});

$(document).ready(function(){

        $("#txtSkip").keydown(function (event) {
            if (event.shiftKey) {
                event.preventDefault();
            }

            if (event.keyCode == 46 || event.keyCode == 8) {
            }
            else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57) {
                        event.preventDefault();
                    }
                }
                else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            }
        });
});


//$(document).ready(function(){
//  $('.Context').contextmenu(function(){
//	$('.Context').css("background-color","black");
//	//var id  = $(this).attr('id');
//	  //$('#'+id+' .btnEdit').toggle();
//	  //return false;
//  });
//  $(document).click(function(){
//	  $('.Context').css("background-color","none")
//  });
//});



    
        
        Sys.Application.add_load(function () {
        var HU_2ndgreeks=<%=this.HU_2ndgreeks%>
        var HU_BF=<%=this.HU_BF%>
        var HU_BF2=<%=this.HU_BF2%>
        var HU_Calender=<%=this.HU_Calender%>
        var HU_Greeks=<%=this.HU_Greeks%>
        var HU_OI=<%=this.HU_OI%>
        var HU_Other=<%=this.HU_Other%>
        var HU_PCP=<%=this.HU_PCP%>
        var HU_Premium=<%=this.HU_Premium%>
        var HU_Ratio=<%=this.HU_Ratio%>
        var HU_Straddle=<%=this.HU_Straddle%>
        var HU_Vol=<%=this.HU_Vol%>
        var HU_Volspread=<%=this.HU_Volspread%>
        var HU_Volume=<%=this.HU_Volume%>
        
        if(HU_2ndgreeks == 0)
        {
        $('td:nth-child(2),th:nth-child(2)').hide();
        $('td:nth-child(3),th:nth-child(3)').hide();
        $('td:nth-child(4),th:nth-child(4)').hide();
        $('td:nth-child(32),th:nth-child(32)').hide();
        $('td:nth-child(33),th:nth-child(33)').hide();
        $('td:nth-child(34),th:nth-child(34)').hide();
        }
        else
        {
        $('td:nth-child(2),th:nth-child(2)').show();
        $('td:nth-child(3),th:nth-child(3)').show();
        $('td:nth-child(4),th:nth-child(4)').show();
        $('td:nth-child(32),th:nth-child(32)').show();
        $('td:nth-child(33),th:nth-child(33)').show();
        $('td:nth-child(34),th:nth-child(34)').show();
        }
        if(HU_BF == 0)
        {
        $('td:nth-child(5),th:nth-child(5)').hide();
        $('td:nth-child(35),th:nth-child(35)').hide();
        }
        else
        {
        $('td:nth-child(35),th:nth-child(35)').show();
        $('td:nth-child(5),th:nth-child(5)').show();
        }

        if(HU_BF2 == 0)
        {
        $('td:nth-child(6),th:nth-child(6)').hide();
        $('td:nth-child(7),th:nth-child(7)').hide();
        $('td:nth-child(36),th:nth-child(36)').hide();
        $('td:nth-child(37),th:nth-child(37)').hide();
        }
        else
        {
        $('td:nth-child(6),th:nth-child(6)').show();
        $('td:nth-child(7),th:nth-child(7)').show();
        $('td:nth-child(36),th:nth-child(36)').show();
        $('td:nth-child(37),th:nth-child(37)').show();
        }

        if(HU_Calender == 0)
        {
        $('td:nth-child(8),th:nth-child(8)').hide();
        $('td:nth-child(38),th:nth-child(38)').hide();
        }
        else
        {
        $('td:nth-child(8),th:nth-child(8)').show();
        $('td:nth-child(38),th:nth-child(38)').show();
        }

        if(HU_Greeks == 0)
        {
        $('td:nth-child(9),th:nth-child(9)').hide();
        $('td:nth-child(10),th:nth-child(10)').hide();
        $('td:nth-child(11),th:nth-child(11)').hide();
        $('td:nth-child(39),th:nth-child(39)').hide();
        $('td:nth-child(40),th:nth-child(40)').hide();
        $('td:nth-child(41),th:nth-child(41)').hide();
        }
        else
        {
        $('td:nth-child(9),th:nth-child(9)').show();
        $('td:nth-child(10),th:nth-child(10)').show();
        $('td:nth-child(11),th:nth-child(11)').show();
        $('td:nth-child(39),th:nth-child(39)').show();
        $('td:nth-child(40),th:nth-child(40)').show();
        $('td:nth-child(41),th:nth-child(41)').show();
        }

        if(HU_OI == 0)
        {
        $('td:nth-child(12),th:nth-child(12)').hide();
        $('td:nth-child(13),th:nth-child(13)').hide();
        $('td:nth-child(42),th:nth-child(42)').hide();
        $('td:nth-child(43),th:nth-child(43)').hide();
        $('td:nth-child(44),th:nth-child(44)').hide();
        $('td:nth-child(45),th:nth-child(45)').hide();
        }
        else
        {
        $('td:nth-child(12),th:nth-child(12)').show();
        $('td:nth-child(13),th:nth-child(13)').show();
        $('td:nth-child(42),th:nth-child(42)').show();
        $('td:nth-child(43),th:nth-child(43)').show();
        $('td:nth-child(44),th:nth-child(44)').show();
        $('td:nth-child(45),th:nth-child(45)').show();
        }

        if(HU_PCP == 0)
        {
        $('td:nth-child(14),th:nth-child(14)').hide();
        $('td:nth-child(15),th:nth-child(15)').hide();
        }
        else
        {
        $('td:nth-child(14),th:nth-child(14)').show();
        $('td:nth-child(15),th:nth-child(15)').show();
        }

        if(HU_Premium == 0)
        {
        $('td:nth-child(16),th:nth-child(16)').hide();
        $('td:nth-child(17),th:nth-child(17)').hide();
        $('td:nth-child(46),th:nth-child(46)').hide();
        $('td:nth-child(47),th:nth-child(47)').hide();
        }
        else
        {
        $('td:nth-child(16),th:nth-child(16)').show();
        $('td:nth-child(17),th:nth-child(17)').show();
        $('td:nth-child(46),th:nth-child(46)').show();
        $('td:nth-child(47),th:nth-child(47)').show();
        }

        if(HU_Ratio == 0)
        {
        $('td:nth-child(18),th:nth-child(18)').hide();
        $('td:nth-child(19),th:nth-child(19)').hide();
        $('td:nth-child(20),th:nth-child(20)').hide();
        $('td:nth-child(48),th:nth-child(48)').hide();
        $('td:nth-child(49),th:nth-child(49)').hide();
        $('td:nth-child(50),th:nth-child(50)').hide();
        }
        else
        {
        $('td:nth-child(18),th:nth-child(18)').show();
        $('td:nth-child(19),th:nth-child(19)').show();
        $('td:nth-child(20),th:nth-child(20)').show();
        $('td:nth-child(48),th:nth-child(48)').show();
        $('td:nth-child(49),th:nth-child(49)').show();
        $('td:nth-child(50),th:nth-child(50)').show();
        }

        if(HU_Straddle == 0)
        {
        $('td:nth-child(21),th:nth-child(21)').hide();
        }
        else
        {
        $('td:nth-child(21),th:nth-child(21)').show();
        }

        if(HU_Volspread == 0)
        {
        $('td:nth-child(24),th:nth-child(24)').hide();
        $('td:nth-child(25),th:nth-child(25)').hide();
        $('td:nth-child(26),th:nth-child(26)').hide();
        $('td:nth-child(27),th:nth-child(27)').hide();
        $('td:nth-child(28),th:nth-child(28)').hide();
        $('td:nth-child(53),th:nth-child(53)').hide();
        $('td:nth-child(54),th:nth-child(54)').hide();
        $('td:nth-child(55),th:nth-child(55)').hide();
        $('td:nth-child(56),th:nth-child(56)').hide();
        $('td:nth-child(57),th:nth-child(57)').hide();
        $('td:nth-child(58),th:nth-child(58)').hide();
        $('td:nth-child(59),th:nth-child(59)').hide();
        }
        else
        {
        $('td:nth-child(24),th:nth-child(24)').show();
        $('td:nth-child(25),th:nth-child(25)').show();
        $('td:nth-child(26),th:nth-child(26)').show();
        $('td:nth-child(27),th:nth-child(27)').show();
        $('td:nth-child(28),th:nth-child(28)').show();
        $('td:nth-child(53),th:nth-child(53)').show();
        $('td:nth-child(54),th:nth-child(54)').show();
        $('td:nth-child(55),th:nth-child(55)').show();
        $('td:nth-child(56),th:nth-child(56)').show();
        $('td:nth-child(57),th:nth-child(57)').show();
        $('td:nth-child(58),th:nth-child(58)').show();
        $('td:nth-child(59),th:nth-child(59)').show();
        }

        if(HU_Vol == 0)
        {
        $('td:nth-child(22),th:nth-child(22)').hide();
        $('td:nth-child(23),th:nth-child(23)').hide();
        $('td:nth-child(51),th:nth-child(51)').hide();
        $('td:nth-child(52),th:nth-child(52)').hide();
        }

        else
        {
        $('td:nth-child(22),th:nth-child(22)').show();
        $('td:nth-child(23),th:nth-child(23)').show();
        $('td:nth-child(51),th:nth-child(51)').show();
        $('td:nth-child(52),th:nth-child(52)').show();
        }

        if(HU_Volume == 0)
        {
        $('td:nth-child(29),th:nth-child(29)').hide();
        $('td:nth-child(60),th:nth-child(60)').hide();
        }
        else
        {
        $('td:nth-child(29),th:nth-child(29)').show();
        $('td:nth-child(60),th:nth-child(60)').show();
        }
       
        });

        function update(jscolor) {
        // 'jscolor' instance can be used as a string
        $('#tableID').css("-webkit-text-fill-color", "#" + jscolor);
    }
    function update1(jscolor) {
        // 'jscolor' instance can be used as a string
        //document.getElementById('tableID').style.backgroundColor = '#' + jscolor
        $('#tableID').css("backgroundColor", "#" + jscolor);
    }
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : evt.keyCode;
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;    
         return true;
      }

     </script>
</asp:Content>
