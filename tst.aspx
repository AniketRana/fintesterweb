﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="tst.aspx.cs" Inherits="tst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
   <meta charset="UTF-8" />
		
       		<!--alerts CSS -->
		<link href="vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
		
		
    <div class="page-wrapper" style="min-height: 664px; padding-top: 30px; padding-bottom: 0px;">
        <div class="container-fluid pt-25">
            
             <img src="" alt="alert" class="img-responsive model_img" id="sa-warning" />               
      
        </div>
    </div>
    		
		<!-- Sweet-Alert  -->
		<script src="vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
        <%--<script src="dist/js/sweetalert-data.js"></script>--%>
		
        <script>
    //Warning Message
    $('#sa-warning,.sa-warning').on('click',function(e){
	    swal({   
            title: "Are you sure ?",   
            text: "You will not be able to recover this Record !",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#fec107",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "Your file has been deleted.", "success"); 
        });
		return false;
    });
        </script>

</asp:Content>
