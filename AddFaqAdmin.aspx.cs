﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AddFaqAdmin : System.Web.UI.Page
{

    string UserId = "";
    string sql;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["IDName"] == null)
        {                                   
            Response.Redirect("AdminLogin.aspx");
        }

        UserId = Session["IDName"].ToString();
        if(!IsPostBack)
        {
            fillgrid();
        }
    }
    private void fillgrid()
    {
        DataSet ds1 = new DataSet();
        cnnstr.Open();
        sql = "select * from tblFaq order by id desc";
        SqlDataAdapter da = new SqlDataAdapter(sql, cnnstr);
        da.Fill(ds1);
        grvfaq.DataSource = ds1;
        grvfaq.DataBind();
        cnnstr.Close();
    }
    protected void grvfaq_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grvfaq.EditIndex = -1;
        fillgrid();
    }
    protected void grvfaq_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grvfaq.EditIndex = e.NewEditIndex;
        fillgrid();
    }
    protected void grvfaq_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Label lblId = (Label)grvfaq .Rows[e.RowIndex].FindControl("lblid");
        TextBox gtxtQuestion= (TextBox)grvfaq.Rows[e.RowIndex].FindControl("gtxtQuestion");
        TextBox gtxtAnswer = (TextBox)grvfaq.Rows[e.RowIndex].FindControl("gtxtAnswer");
      
        cnnstr.Open();
        //sql = "Update Login set FirstName = '" + gtxtFname.Text + "',Password = '" + gtxtPassword.Text + "',MobileNo = '" + gtxtmno.Text + "',FromDate = '" + gtxtFromDt.Text + "',ToDate = '" + gtxtToDt.Text + "',ExpDate = '" + gtxtExpDt.Text + "',RegisterDate = '" + gtxtRegisterDt.Text + "' where Email = '" + lblEmail.Text + "'";
        sql = "update tblFaq set Question = '" + gtxtQuestion.Text + "',Answer = '" + gtxtAnswer.Text + "' where id = " + lblId.Text + "";
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        grvfaq.EditIndex = -1;
        fillgrid();
    }
    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        string id; 
        Button btndel = (Button)sender;
        id = btndel.CommandArgument;
        cnnstr.Open();
        sql = "Delete From tblFaq Where id = " + id +"";
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        fillgrid();
    }
    protected void AddFaqimgbtn_Click(object sender, ImageClickEventArgs e)
    {
        txtAnswer.Text = "";
        txtQuestion.Text = "";
        pnlForm.Visible = true;
        pnlgrid.Visible = false;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        cnnstr.Open();
        sql = "insert into tblFaq values('" + txtQuestion.Text + "','" + txtAnswer.Text + "')";
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        fillgrid();
        pnlForm.Visible = false;
        pnlgrid.Visible = true;
    }
}