﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;


public partial class SimulatorNew : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
    SqlCommand cmdd = new SqlCommand();
    public static string ACTIVE_OPTION = "";
    DataTable dtExpiryDate = new DataTable();
    double dblFutPrice1 = 0;
    double dblFutPrice2 = 0;
    double dblFutPrice3 = 0;
    DateTime EXP_DATE1 = DateTime.Parse("1/1/1900");
    DateTime EXP_DATE2 = DateTime.Parse("1/1/1900");
    DateTime EXP_DATE3 = DateTime.Parse("1/1/1900");
    DateTime ACTIVE_EXPIRY_DATE = DateTime.Parse("1/1/1900");
    double SPOT_PRICE_ACTIVE = 0;
    DataTable ACTIVE_MARKET_WATCH_DATA = new DataTable();
    DataTable dt_main = new DataTable();
    string Action = "";
    string CPF = "";
    double dblWeightedFuturePrize = 0;
    int TradeQty = 0;
    string myIdVal;
    string SelectdCPF;
    string SelectedStrike;

    EntityStrategy objEntity;
    Repostrategy objStrat;
    EntitySummary objsum;
   
    string UserId;

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();

    RepoSetting obj;
    EntitySetting objsetting;
    public int R_Price;
    public int R_Volatality;
    public int R_Pnl;
    public int R_1StGreeks;
    public int R_2ndGreeks;
    public int R_1stGreeksValue;
    public int R_2ndGreeksValue;
    public int R_Premium;
    public int R_Qty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");

        }

        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }

        grvSimulator.Columns[2].Visible = false;
        grvSimulator.Columns[8].Visible = false;
        rbBuy.Checked = true;
        
        txtBusinessDt.Attributes.Add("readonly", "readonly");

        if (!IsPostBack)
        {
            dt_main = null;
            objStrat = new Repostrategy();
            ddSymbol.SelectedValue = "NIFTY100";

            if (Request.QueryString.Keys.Count > 0)
            {                                                    //code for request comming from marketwatch along with Long and Short
                objEntity = new EntityStrategy();
                objStrat = new Repostrategy();
                objEntity.Strategy = objStrat.getValues(Request.QueryString["Strategy"].ToString(), Request.QueryString["LS"].ToString());
                objEntity.Midstrike = Convert.ToDouble(Request.QueryString["Strike"].ToString());
                objEntity.Bdate = Request.QueryString["TrdDate"].ToString();
                objEntity.Edate = Request.QueryString["ExpDate"].ToString();
                objEntity.Symbol = Request.QueryString["symbol"].ToString();
                objEntity.Userid = Session["UserEmail"].ToString();
                objEntity.Spotprice = Request.QueryString["Spot"].ToString();
                DataTable dt_Trade = new DataTable();
                if (objStrat.getSimPosition(objEntity).Rows.Count > 0)
                {
                    ModalPopupExtender2.Show();
                }
                else
                {
                    RepoSetting obj = new RepoSetting();
                    EntitySetting objsetting = obj.getSettingByEmail(objEntity.Userid);
                    objEntity.Qty = objsetting.R_Quantity.ToString();
                    objStrat.AddMWPosition(objEntity);

                    DataTable dt_Add = new DataTable();
                    dt_Add = objStrat.ActionAdd(objEntity);


                    dt_Trade = objStrat.ActionTrade(objEntity);

                    updateSetting(Session["UserEmail"].ToString());
                    HideUnhide(Session["UserEmail"].ToString());

                    grvSimulator.DataSource = dt_Trade;
                    grvSimulator.DataBind();
                    setSummary(objStrat.getSummary(dt_Trade));

                    ddSymbol.SelectedValue = objEntity.Symbol;
                    txtBusinessDt.Text = objEntity.Bdate;
                    ddSymbol.Enabled = false;
                    txtBusinessDt.Enabled = false;
                    ImageButton1.Enabled = false;
                    objStrat.set_Recording(objEntity);
                    lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
                }
            }
            else
            {
                objEntity = new EntityStrategy();
                objEntity.Userid = Session["UserEmail"].ToString();
                if (objStrat.getSimPosition(objEntity).Rows.Count > 0)
                {
                    ModalPopupExtender2.Show();
                }
                else
                {
                    objStrat.delete_MW_Entry_By_Email(Session["UserEmail"].ToString());
                }
            }

            fillSMsetting();
            fillSMHideUnhideSetting();

        }
        
    }

    private void HideUnhide(string EmailId)
    {
        obj = new RepoSetting();
        objsetting = obj.getSettingByEmail(EmailId);

        if (objsetting.Hu_Sm_Default == 0)
        {
            grvSimulator.Columns[3].Visible = false;
            grvSimulator.Columns[4].Visible = false;
            grvSimulator.Columns[5].Visible = false;
            grvSimulator.Columns[6].Visible = false;
           // grvSimulator.Columns[8].Visible = false;
            grvSimulator.Columns[9].Visible = false;
            grvSimulator.Columns[10].Visible = false;
            grvSimulator.Columns[11].Visible = false;
            grvSimulator.Columns[48].Visible = false;
            grvSimulator.Columns[49].Visible = false;
            grvSimulator.Columns[0].Visible = false;
            grvSimulator.Columns[1].Visible = false;
        }
        else
        {
            grvSimulator.Columns[3].Visible = true;
            grvSimulator.Columns[4].Visible = true;
            grvSimulator.Columns[5].Visible = true;
            grvSimulator.Columns[6].Visible = true;
           // grvSimulator.Columns[8].Visible = true;
            grvSimulator.Columns[9].Visible = true;
            grvSimulator.Columns[10].Visible = true;
            grvSimulator.Columns[11].Visible = true;
            grvSimulator.Columns[48].Visible = true;
            grvSimulator.Columns[49].Visible = true;
            grvSimulator.Columns[0].Visible = true;
            grvSimulator.Columns[1].Visible = true;
        }
        if (objsetting.Hu_Sm_Hide == 0)
        {
            grvSimulator.Columns[7].Visible = false;
            grvSimulator.Columns[26].Visible = false;
            grvSimulator.Columns[27].Visible = false;
            grvSimulator.Columns[34].Visible = false;
            grvSimulator.Columns[35].Visible = false;
            grvSimulator.Columns[36].Visible = false;
            grvSimulator.Columns[42].Visible = false;
            grvSimulator.Columns[43].Visible = false;
            grvSimulator.Columns[44].Visible = false;
            grvSimulator.Columns[45].Visible = false;
            grvSimulator.Columns[46].Visible = false;
            grvSimulator.Columns[47].Visible = false;
        }
        else
        {
            grvSimulator.Columns[7].Visible = true;
            grvSimulator.Columns[26].Visible = true;
            grvSimulator.Columns[27].Visible = true;
            grvSimulator.Columns[34].Visible = true;
            grvSimulator.Columns[35].Visible = true;
            grvSimulator.Columns[36].Visible = true;
            grvSimulator.Columns[42].Visible = true;
            grvSimulator.Columns[43].Visible = true;
            grvSimulator.Columns[44].Visible = true;
            grvSimulator.Columns[45].Visible = true;
            grvSimulator.Columns[46].Visible = true;
            grvSimulator.Columns[47].Visible = true;
        }
        if (objsetting.Hu_Sm_Greeks == 0)
        {
            grvSimulator.Columns[12].Visible = false;
            grvSimulator.Columns[13].Visible = false;
            grvSimulator.Columns[14].Visible = false;
            grvSimulator.Columns[15].Visible = false;
            grvSimulator.Columns[16].Visible = false;
            grvSimulator.Columns[17].Visible = false;

        }

        else
        {
            grvSimulator.Columns[12].Visible = true;
            grvSimulator.Columns[13].Visible = true;
            grvSimulator.Columns[14].Visible = true;
            grvSimulator.Columns[15].Visible = true;
            grvSimulator.Columns[16].Visible = true;
            grvSimulator.Columns[17].Visible = true;
        }


        if (objsetting.Hu_Sm_GreeksValue == 0)
        {
            grvSimulator.Columns[18].Visible = false;
            grvSimulator.Columns[19].Visible = false;
            grvSimulator.Columns[20].Visible = false;
            grvSimulator.Columns[21].Visible = false;
            grvSimulator.Columns[22].Visible = false;
            grvSimulator.Columns[23].Visible = false;
        }
        else
        {
            grvSimulator.Columns[18].Visible = true;
            grvSimulator.Columns[19].Visible = true;
            grvSimulator.Columns[20].Visible = true;
            grvSimulator.Columns[21].Visible = true;
            grvSimulator.Columns[22].Visible = true;
            grvSimulator.Columns[23].Visible = true;
        }

        if (objsetting.Hu_Sm_Pnl == 0)
        {
            grvSimulator.Columns[24].Visible = false;
            grvSimulator.Columns[25].Visible = false;
        }
        else
        {
            grvSimulator.Columns[24].Visible = true;
            grvSimulator.Columns[25].Visible = true;
        }

        if (objsetting.Hu_Sm_DeltaEffect == 0)
        {
            grvSimulator.Columns[28].Visible = false;
            grvSimulator.Columns[29].Visible = false;
            grvSimulator.Columns[30].Visible = false;
            grvSimulator.Columns[31].Visible = false;
            grvSimulator.Columns[32].Visible = false;
            grvSimulator.Columns[33].Visible = false;

        }
        else
        {
            grvSimulator.Columns[28].Visible = true;
            grvSimulator.Columns[29].Visible = true;
            grvSimulator.Columns[30].Visible = true;
            grvSimulator.Columns[31].Visible = true;
            grvSimulator.Columns[32].Visible = true;
            grvSimulator.Columns[33].Visible = true;
        }

        if (objsetting.Hu_Sm_VegaEffect == 0)
        {
            grvSimulator.Columns[37].Visible = false;
            grvSimulator.Columns[38].Visible = false;
            grvSimulator.Columns[39].Visible = false;
            grvSimulator.Columns[40].Visible = false;
            grvSimulator.Columns[41].Visible = false;

        }
        else
        {
            grvSimulator.Columns[37].Visible = true;
            grvSimulator.Columns[38].Visible = true;
            grvSimulator.Columns[39].Visible = true;
            grvSimulator.Columns[40].Visible = true;
            grvSimulator.Columns[41].Visible = true;
        }

        if (objsetting.Hu_Sm_ThetaEffect == 0)
        {
            grvSimulator.Columns[50].Visible = false;
            grvSimulator.Columns[51].Visible = false;
            grvSimulator.Columns[52].Visible = false;
        }
        else
        {
            grvSimulator.Columns[50].Visible = true;
            grvSimulator.Columns[51].Visible = true;
            grvSimulator.Columns[52].Visible = true;
        }
    }

    private void updateSetting(string EmailId)
    {
        obj = new RepoSetting();
        objsetting = obj.getSettingByEmail(EmailId);
        R_Price = objsetting.R_Price;
        R_Volatality = objsetting.R_Volatality;
        R_1StGreeks = objsetting.R_1stGreeks;
        R_1stGreeksValue = objsetting.R_1stGreeksValue;
        R_2ndGreeks = objsetting.R_2ndGreeks;
        R_2ndGreeksValue = objsetting.R_2ndGreeksvalue;
        R_Pnl = objsetting.R_pnl;
        R_Premium = objsetting.R_smPremium;
        R_Qty = objsetting.R_Qty;
    
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {

        try
        {
            if (!String.IsNullOrEmpty(txtStrikePrice.Text) && txtStrikePrice.Text !="0")
            {

                objEntity = new EntityStrategy();
                objStrat = new Repostrategy();
                if (rbCall.Checked == true)
                {
                    objEntity.Cpf = "CE";
                }
                else if (rbPut.Checked == true)
                {
                    objEntity.Cpf = "PE";
                }
                else
                {
                    objEntity.Cpf = "CE";
                }
                objEntity.Action = "AddOne";
                objEntity.Symbol = ddSymbol.SelectedValue;
                objEntity.Userid = Session["UserEmail"].ToString();
                objEntity.Midstrike = Convert.ToDouble(txtStrikePrice.Text);
                objEntity.Bdate = txtBusinessDt.Text;
                RepoSetting obj = new RepoSetting();
                //EntitySetting objsetting = obj.getSettingByEmail(objEntity.Userid);
                objEntity.Qty = "0";
                object RecordCount = 0;
                con.Open();
                cmdd = new SqlCommand("select count(*) Cnt from tblSimulatorPosition where strike='" + txtStrikePrice.Text + "' and  [Trade Date]='" + DateTime.Parse(txtBusinessDt.Text).ToString("dd-MMM-yyyy") + "' and [Exp Date]='" + ACTIVE_EXPIRY_DATE.ToString("dd-MMM-yyyy") + "' and cpf='" + CPF + "'", con);
                RecordCount = cmdd.ExecuteScalar();
                con.Close();

                if (Convert.ToInt16(RecordCount) == 0)
                {
                    updateSetting(Session["UserEmail"].ToString());
                    HideUnhide(Session["UserEmail"].ToString());
                    DataTable dt_Trade = new DataTable();
                    dt_Trade = objStrat.AddTrade(objEntity);

                    if (dt_Trade != null)
                    {
                        grvSimulator.DataSource = dt_Trade;
                        grvSimulator.DataBind();
                        setSummary(objStrat.getSummary(dt_Trade));
                        ddSymbol.Enabled = false;
                        txtBusinessDt.Enabled = false;
                        ImageButton1.Enabled = false;
                        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
                    }
                }

                //objEntity.Cpf = "FUT";
            }

            else if(String.IsNullOrEmpty(txtStrikePrice.Text) || txtStrikePrice.Text =="0")
            {
                objEntity = new EntityStrategy();
                objStrat = new Repostrategy();
                
                    objEntity.Cpf = "FUT";
  
                objEntity.Action = "Add";
                objEntity.Symbol = ddSymbol.SelectedValue;
                objEntity.Userid = Session["UserEmail"].ToString();
                objEntity.Midstrike = 0.00;
                objEntity.Bdate = txtBusinessDt.Text;
                RepoSetting obj = new RepoSetting();
                objEntity.Qty = "0";
                object RecordCount = 0;
                con.Open();
                cmdd = new SqlCommand("select count(*) Cnt from tblSimulatorPosition where strike='" + objEntity.Midstrike + "' and  [Trade Date]='" + DateTime.Parse(txtBusinessDt.Text).ToString("dd-MMM-yyyy") + "' and [Exp Date]='" + ACTIVE_EXPIRY_DATE.ToString("dd-MMM-yyyy") + "' and cpf='" + CPF + "'", con);
                RecordCount = cmdd.ExecuteScalar();
                con.Close();

                if (Convert.ToInt16(RecordCount) == 0)
                {
                    updateSetting(Session["UserEmail"].ToString());
                    HideUnhide(Session["UserEmail"].ToString());
                    DataTable dt_Trade = new DataTable();
                    dt_Trade = objStrat.Add_Fut_Buy(objEntity);

                    if (dt_Trade != null)
                    {
                        grvSimulator.DataSource = dt_Trade;
                        grvSimulator.DataBind();
                        setSummary(objStrat.getSummary(dt_Trade));
                        ddSymbol.Enabled = false;
                        txtBusinessDt.Enabled = false;
                        ImageButton1.Enabled = false;
                        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
                    }
                }
            }
           
        }
        catch (Exception ex)
        {

        }
        
    }
  
    //protected void ddSymbol_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    var selectedsymbol = ddSymbol.SelectedItem.Value.ToString();
    //    con.Open();
    //    cmdd = new SqlCommand("SELECT MAX(BUSINESS_DATE) AS MAX_DATE FROM OptionCE_" + selectedsymbol + "", con);
    //    SqlDataAdapter sdas = new SqlDataAdapter(cmdd);
    //    DataTable dt = new DataTable();
    //    sdas.Fill(dt);
    //    con.Close();
    //    txtBusinessDt.Text = Convert.ToDateTime(dt.Rows[0][0].ToString()).ToString("dd-MMM-yyyy");
    //    ACTIVE_OPTION = "FUTURE";

    //}
    
    
    
    protected void RepeaterInner_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (Action == "Trade")
        {
            switch (e.Item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:
                    dynamic dr = (DataRowView)e.Item.DataItem;
                    if (rbBuy.Checked == true)
                    {
                        dynamic Button = (Button)e.Item.FindControl("btnTradePopup");
                        Button.BackColor = System.Drawing.Color.Green;
                    }
                    if (rbSell.Checked == true)
                    {
                        dynamic Button = (Button)e.Item.FindControl("btnTradePopup");
                        Button.BackColor = System.Drawing.Color.Red;
                    }
                    break;
            }
        }
    }
    public void FillGrid()
    {
        
    }
    protected void btnAddTrade_Click1(object sender, EventArgs e)
    {
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        Action = "TradeQty";
        //TradeQty = Convert.ToInt16(txtTrdQty.Text);
        int Qty = Convert.ToInt16(txtTrdQty.Text);
       

        DataTable dt = new DataTable();
        SelectdCPF = Session["SelectdCPF"].ToString();
            if (rbBuy.Checked)
            {
                objEntity.Cpf = SelectdCPF;
                objEntity.Qty = Qty.ToString();
            }
            if (rbSell.Checked)
            {
                objEntity.Cpf = SelectdCPF;
                objEntity.Qty = Convert.ToString(Convert.ToInt16(Qty) * (-1));
            }

     
        objEntity.Id = Session["TradeId"].ToString();
        objEntity.Userid = Session["UserEmail"].ToString();
        objEntity.Symbol = ddSymbol.SelectedValue;
        updateSetting(Session["UserEmail"].ToString());
        HideUnhide(Session["UserEmail"].ToString());
        DataTable dt_Trade = new DataTable();
        dt_Trade = objStrat.updateTrade(objEntity);
        grvSimulator.DataSource = dt_Trade;
        grvSimulator.DataBind();
        //setSummary(objStrat.getSummary(dt_Trade));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";

        //int rowid = Convert.ToInt16(Session["rowid"].ToString());
        //string CurrStrike = Session["SelectedStrike"].ToString();

        //con.Open();
        //cmdd = new SqlCommand("select MIN(srno) srno from tblSimulatorPosition", con);
        //object num = cmdd.ExecuteScalar();
        //con.Close();

        //con.Open();
        //cmdd = new SqlCommand("select Spot from tblSimulatorPosition where CPF='XX'", con);
        //object FutSpot = cmdd.ExecuteScalar();
        //con.Close();
        //dblWeightedFuturePrize = (dblWeightedFuturePrize + (Convert.ToDouble(txtTrdQty.Text) * Convert.ToDouble(FutSpot)));

        //if (myIdVal == Convert.ToString(num).ToString())
        //{
        //    Action = "FutBuy";
        //    CPF="XX";
        //    con.Open();
        //    cmdd = new SqlCommand("exec sp_Proc_Sim '" + Action + "','" + ddSymbol.SelectedItem.Value.ToString().Trim() + "','" + Session["UserEmail"] + "','" + SPOT_PRICE_ACTIVE + "','" + CurrStrike + "','" + CPF + "','" + DateTime.Parse(txtBusinessDt.Text).ToString("dd-MMM-yyyy") + "','" + ACTIVE_EXPIRY_DATE.ToString("dd-MMM-yyyy") + "','" + Qty + "','" + myIdVal + "','"+dblWeightedFuturePrize+"'", con);
        //    SqlDataAdapter sdas = new SqlDataAdapter(cmdd);
        //    DataTable dt_main = new DataTable();
        //    sdas.Fill(dt_main);
        //    con.Close();
        //    grvSimulator.DataSource = dt_main;
        //    grvSimulator.DataBind();
        //}
        //else
        //{
        //    con.Open();
        //    cmdd = new SqlCommand("exec sp_Proc_Sim '" + Action + "','" + ddSymbol.SelectedItem.Value.ToString().Trim() + "','" + Session["UserEmail"] + "','" + SPOT_PRICE_ACTIVE + "','" + CurrStrike + "','" + CPF + "','" + DateTime.Parse(txtBusinessDt.Text).ToString("dd-MMM-yyyy") + "','" + ACTIVE_EXPIRY_DATE.ToString("dd-MMM-yyyy") + "','" + Qty + "','" + myIdVal + "'", con);
        //    SqlDataAdapter sdas = new SqlDataAdapter(cmdd);
        //    DataTable dt_main = new DataTable();
        //    sdas.Fill(dt_main);
        //    con.Close();
        //    grvSimulator.DataSource = dt_main;
        //    grvSimulator.DataBind();
        //}

        
    }
    protected void grvSimulator_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ShowPopUpOfTrade")
        {
            GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            int rowid = row.RowIndex;
            Session["rowid"] = rowid;
            txtTrdQty.Text = null;
            Label TradeId = (Label)grvSimulator.Rows[rowid].FindControl("lblID");
            myIdVal = TradeId.Text;
            Session["TradeId"] = myIdVal;
            Label TradeCPF = (Label)grvSimulator.Rows[rowid].FindControl("lblCPF");
            SelectdCPF = TradeCPF.Text;
            Session["SelectdCPF"] = SelectdCPF;
            Label CurStrike = (Label)grvSimulator.Rows[rowid].FindControl("lblStrike");
            SelectedStrike = CurStrike.Text;
            Session["SelectedStrike"] = SelectedStrike;
            ModalPopupExtender1.Show();
            txtTrdQty.Focus();
            
        }

        if (e.CommandName == "DeleteRow")
        {
            objStrat = new Repostrategy();
            GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            int rowid = row.RowIndex;
            Label TradeId = (Label)grvSimulator.Rows[rowid].FindControl("lblID");
            myIdVal = TradeId.Text;
            DataTable dt_Trade = new DataTable();
            dt_Trade = objStrat.Delete_Trade(Convert.ToInt32(myIdVal), Session["UserEmail"].ToString());
            updateSetting(Session["UserEmail"].ToString());
            grvSimulator.DataSource = dt_Trade;
            grvSimulator.DataBind();
            setSummary(objStrat.getSummary(dt_Trade));
            lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
            

        }
    }


    protected void grvSimulator_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Action == "Trade")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // determine the value of the UnitsInStock field
                int ID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Qty"));
                if (ID > 0)
                {
                    e.Row.Cells[0].BackColor = System.Drawing.Color.Blue;
                }
                if (ID < 0)
                {
                    e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
                }
                // color the background of the row yellow
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string s = e.Row.Cells[9].Text;
            double Qty = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Qty"));
            if (Qty > 0)
            {

                e.Row.BackColor = ColorTranslator.FromHtml("#a9ba8b");
            }
            else
            {

                e.Row.BackColor = ColorTranslator.FromHtml("#f75d5d");
                
            }
        }
    }

    protected void txtBusinessDt_TextChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT EXPIRY_DATE, FUTURE_PRICE FROM OPTIONPE_" + ddSymbol.SelectedItem.Value.ToString().Trim() + " WHERE  Security_Symbol='" + ddSymbol.SelectedItem.Value.ToString().Trim() + "' And  BUSINESS_DATE='" + DateTime.Parse(txtBusinessDt.Text).ToString("dd-MMM-yyyy") + "'  ORDER BY EXPIRY_DATE ASC", con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        sda.Fill(dtExpiryDate);

        if (dtExpiryDate.Rows.Count == 0)
        {
            //    dateTimePicker.Text = DateTime.Parse(dateTimePicker.Text.ToString()).AddDays(intAddDay).ToString();
            //    setHeaderData(dateTimePicker);
        }
        else
        {
            EXP_DATE1 = DateTime.Parse(dtExpiryDate.Rows[0].ItemArray.GetValue(0).ToString());
            EXP_DATE2 = DateTime.Parse(dtExpiryDate.Rows[1].ItemArray.GetValue(0).ToString());
            EXP_DATE3 = DateTime.Parse(dtExpiryDate.Rows[2].ItemArray.GetValue(0).ToString());
            dblFutPrice1 = Convert.ToDouble(dtExpiryDate.Rows[0].ItemArray.GetValue(1).ToString());
            dblFutPrice2 = Convert.ToDouble(dtExpiryDate.Rows[1].ItemArray.GetValue(1).ToString());
            dblFutPrice3 = Convert.ToDouble(dtExpiryDate.Rows[2].ItemArray.GetValue(1).ToString());
        }
        SPOT_PRICE_ACTIVE = dblFutPrice1;
        Session["SpotPriceActive"] = SPOT_PRICE_ACTIVE.ToString();
        ACTIVE_EXPIRY_DATE = EXP_DATE1;

        Session["ActiveExpiryDate"] = EXP_DATE1;
    }



    protected void btnFixPrice_Click(object sender, EventArgs e)
    {
        //grvSimulator.DataSource = null;
        //grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getFixVal(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
    protected void btnPNL_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getPNL(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }

    protected void btnTimerplus_Click(object sender, EventArgs e)
    {
        int ch;
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        objEntity.Symbol = Request.QueryString["symbol"].ToString();
        dt_Sim = objStrat.getTimePlus(objEntity, out ch);
        if (dt_Sim != null)
        {
            
                updateSetting(Session["UserEmail"].ToString());
                grvSimulator.DataSource = dt_Sim;
                grvSimulator.DataBind();
                txtBusinessDt.Text = dt_Sim.Rows[0][7].ToString();
                setSummary(objStrat.getSummary(dt_Sim));
                lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
                if (ch == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No data available after this date...');", true);
                }
           
        }
        else 
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
        }
        
    }


    private void setSummary(EntitySummary objsum)
    {
        txtDeltaVal.Text = Math.Round(Convert.ToDouble(objsum.DeltaVal), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtGammaVal.Text = Math.Round(Convert.ToDouble(objsum.GammaVal), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtVegaVal.Text = Math.Round(Convert.ToDouble(objsum.VegaVal), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtVolgaVal.Text = Math.Round(Convert.ToDouble(objsum.VolgaVal), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "");
        txtThetaVal.Text = Math.Round(Convert.ToDouble(objsum.ThetaVal), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtVannaVal.Text = Math.Round(Convert.ToDouble(objsum.VannaVal), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "");
        txtpnlamount.Text = Math.Round(Convert.ToDouble(objsum.PnlAmt), R_Pnl).ToString("N" + R_Pnl + "");
        txtDeltaEffect.Text = Math.Round(Convert.ToDouble(objsum.DeltaEffect), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtGammaEffect.Text = Math.Round(Convert.ToDouble(objsum.GammaEffect), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtVegaEffect.Text = Math.Round(Convert.ToDouble(objsum.VegaEffect), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtVolgaEffect.Text = Math.Round(Convert.ToDouble(objsum.VolgaEffect), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "");
        txtThetaEffect.Text = Math.Round(Convert.ToDouble(objsum.ThetaEffect), R_1stGreeksValue).ToString("N" + R_1stGreeksValue + "");
        txtVannaEffect.Text = Math.Round(Convert.ToDouble(objsum.VannaEffect), R_2ndGreeksValue).ToString("N" + R_2ndGreeksValue + "");
        txtTotal.Text = Math.Round(Convert.ToDouble(objsum.Total), R_Pnl).ToString("N" + R_Pnl + "");
        
    }

    private void txtcolorScheme()
    {
        if (Convert.ToDouble(txtDeltaVal.Text) <= 0)
            txtDeltaVal.BackColor = ColorTranslator.FromHtml("#a9ba8b");
        else
            txtDeltaVal.BackColor = ColorTranslator.FromHtml("#f75d5d");
        
    }
    protected void btnTimerminus_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getTimeMinus(objEntity);
        if (dt_Sim != null)
        {
            updateSetting(Session["UserEmail"].ToString());
            grvSimulator.DataSource = dt_Sim;
            grvSimulator.DataBind();
            setSummary(objStrat.getSummary(dt_Sim));
            txtBusinessDt.Text = dt_Sim.Rows[0][7].ToString();
            lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
        }

    }
    protected void btnSpotPlus_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getSpotPlus(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";

    }
    protected void btnSpotminus_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getSpotMinus(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
    protected void btnPutVolPlus_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getVolPlus(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
    protected void btnResetSpot_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getResetSpot(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
    protected void btnPutVolMinus_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getVolMinus(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
    protected void btnResetVol_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getResetVol(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
   
   
    protected void btnNext_Click1(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getStepNext(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        txtBusinessDt.Text = dt_Sim.Rows[0][7].ToString();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Sim = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Sim = objStrat.getStepPrev(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        grvSimulator.DataSource = dt_Sim;
        grvSimulator.DataBind();
        txtBusinessDt.Text = dt_Sim.Rows[0][7].ToString();
        setSummary(objStrat.getSummary(dt_Sim));
        lblrecStepts.Text = "["+objStrat.get_Recording_Step(Session["UserEmail"].ToString())+"]";
    }
    protected void btnExpToExcel_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }

    private void ExportGridToExcel()
    {
        objStrat = new Repostrategy();

        DataGrid grid = new DataGrid();

        DataTable dt_Rec = new DataTable();

        dt_Rec = objStrat.get_Recordings_By_Email(Session["UserEmail"].ToString());

        grid.DataSource = dt_Rec;
        grid.DataBind();


        //grid.Columns[1].Visible = false;
        //grid.Columns[2].Visible = false;
        //grid.Columns[3].Visible = false;
        string symbol = ddSymbol.SelectedValue;
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string FileName = symbol+"-" + txtBusinessDt.Text + ".xls";
        StringWriter strwritter = new StringWriter();
        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + "Simulator-"+FileName);
        //string headerTable = @"<Table><tr><td><img src=""~\\Images\\F-3.jpg"" \></td></tr></Table>";
        //Response.Write(headerTable);
        Response.Write("FinIdeas");
        grid.GridLines = GridLines.Both;
        grid.HeaderStyle.Font.Bold = true;
        grid.RenderControl(htmltextwrtter);
        Response.Write(strwritter.ToString());
        Response.End();  

    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        /* Verifies that the control is rendered */
    }


    protected void btnSaveManual_Click(object sender, EventArgs e)
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

        string Email = Session["UserEmail"].ToString();
        string SMModule = "SM";
        cnnstr.Open();
        SqlCommand pricecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PRICE_ROUNDING','" + ddlprice.SelectedValue + "'", cnnstr);
        pricecmd.ExecuteNonQuery();

        SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_ROUNDING','" + ddlpnl.SelectedValue + "'", cnnstr);
        pnlcmd.ExecuteNonQuery();

        SqlCommand FsGrkcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','1GREEKS_ROUNDING','" + ddl1Greeks.SelectedValue + "'", cnnstr);
        FsGrkcmd.ExecuteNonQuery();

        SqlCommand FsGrkValCmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','1GREEKSVALUE_ROUNDING','" + ddl1GreeksValue.SelectedValue + "'", cnnstr);
        FsGrkValCmd.ExecuteNonQuery();

        SqlCommand NdGrkcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','2GREEKS_ROUNDING','" + ddl2Greek.SelectedValue + "'", cnnstr);
        NdGrkcmd.ExecuteNonQuery();

        SqlCommand NdGrkValCmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','2GREEKSVALUE_ROUNDING','" + ddl2GreekValue.SelectedValue + "'", cnnstr);
        NdGrkValCmd.ExecuteNonQuery();

        SqlCommand Premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PREMIUM_ROUNDING','" + ddlSpremium.SelectedValue + "'", cnnstr);
        Premiumcmd.ExecuteNonQuery();

        SqlCommand sVolatalitycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VOLATALITY_ROUNDING','" + ddlSVolatality.SelectedValue + "'", cnnstr);
        sVolatalitycmd.ExecuteNonQuery();

        SqlCommand Sqtycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','QTY_ROUNDING','" + ddlSqty.SelectedValue + "'", cnnstr);
        Sqtycmd.ExecuteNonQuery();

        cnnstr.Close();

        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Trade = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Trade = objStrat.getSimPosition(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        HideUnhide(Session["UserEmail"].ToString());
        fillSMsetting();
        fillSMHideUnhideSetting();
        grvSimulator.DataSource = dt_Trade;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Trade));
        
       

    }

    protected void btnHUSetting_Click(object sender, EventArgs e)
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

        string Email = Session["UserEmail"].ToString();
        string SMModule = "SM";
        cnnstr.Open();

        if (chkdef.Checked == true)
        {
            SqlCommand defcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DEFAULT_HU','1'", cnnstr);
            defcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand defcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DEFAULT_HU','0'", cnnstr);
            defcmd.ExecuteNonQuery();
        }
        if (chkHide.Checked == true)
        {
            SqlCommand Hidecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','Hide_HU','1'", cnnstr);
            Hidecmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand Hidecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','Hide_HU','0'", cnnstr);
            Hidecmd.ExecuteNonQuery();
        }

        if (chkSGreek.Checked == true)
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKS_HU','1'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKS_HU','0'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        if (chkSGreekValue.Checked == true)
        {
            SqlCommand Sgreekvalcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKSVALUE_HU','1'", cnnstr);
            Sgreekvalcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand Sgreekvalcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','GREEKSVALUE_HU','0'", cnnstr);
            Sgreekvalcmd.ExecuteNonQuery();
        }
        if (chkpnl.Checked == true)
        {
            SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_HU','1'", cnnstr);
            pnlcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand pnlcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','PNL_HU','0'", cnnstr);
            pnlcmd.ExecuteNonQuery();
        }
        if (chkDelEffct.Checked == true)
        {
            SqlCommand deltacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DELTAEFFECT_HU','1'", cnnstr);
            deltacmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand deltacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','DELTAEFFECT_HU','0'", cnnstr);
            deltacmd.ExecuteNonQuery();
        }
        if (chkVegaEffct.Checked == true)
        {
            SqlCommand vegacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VEGAEFFECT_HU','1'", cnnstr);
            vegacmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand vegacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','VEGAEFFECT_HU','0'", cnnstr);
            vegacmd.ExecuteNonQuery();
        }
        if (chkTheEffct.Checked == true)
        {
            SqlCommand Thetacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','THETAEFFECT_HU','1'", cnnstr);
            Thetacmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand Thetacmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + SMModule + "','THETAEFFECT_HU','0'", cnnstr);
            Thetacmd.ExecuteNonQuery();
        }

        cnnstr.Close();

        grvSimulator.DataSource = null;
        grvSimulator.DataBind();
        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        DataTable dt_Trade = new DataTable();
        objEntity.Userid = Session["UserEmail"].ToString();
        dt_Trade = objStrat.getSimPosition(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        HideUnhide(Session["UserEmail"].ToString());
        fillSMsetting();
        fillSMHideUnhideSetting();
        grvSimulator.DataSource = dt_Trade;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Trade));

        
        
    }

    public void fillSMHideUnhideSetting()
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
        string Email = Session["UserEmail"].ToString();
        string SMModule = "SM";
        string sql = "";
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + SMModule + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();

        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='DEFAULT_HU'").ToString())
        {
            chkdef.Checked = true;
        }
        else
        {
            chkdef.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='HIDE_HU'").ToString())
        {
            chkHide.Checked = true;
        }
        else
        {
            chkHide.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKS_HU'").ToString())
        {
            chkSGreek.Checked = true;
        }
        else
        {
            chkSGreek.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKSVALUE_HU'").ToString())
        {
            chkSGreekValue.Checked = true;
        }
        else
        {
            chkSGreekValue.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='PNL_HU'").ToString())
        {
            chkpnl.Checked = true;
        }
        else
        {
            chkpnl.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='DELTAEFFECT_HU'").ToString())
        {
            chkDelEffct.Checked = true;
        }
        else
        {
            chkDelEffct.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VEGAEFFECT_HU'").ToString())
        {
            chkVegaEffct.Checked = true;
        }
        else
        {
            chkVegaEffct.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='THETAEFFECT_HU'").ToString())
        {
            chkTheEffct.Checked = true;
        }
        else
        {
            chkTheEffct.Checked = false;
        }
    }

    public void fillSMsetting()
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
        string Email = Session["UserEmail"].ToString();
        string SMModule = "SM";
        string sql = "";
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + SMModule + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();

        ddlprice.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PRICE_ROUNDING'").ToString();
        ddlpnl.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PNL_ROUNDING'").ToString();
        ddl1Greeks.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='1GREEKS_ROUNDING'").ToString();
        ddl1GreeksValue.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='1GREEKSVALUE_ROUNDING'").ToString();
        ddl2Greek.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='2GREEKS_ROUNDING'").ToString();
        ddl2GreekValue.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='2GREEKSVALUE_ROUNDING'").ToString();
        ddlSpremium.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PREMIUM_ROUNDING'").ToString();
        ddlSqty.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='QTY_ROUNDING'").ToString();
        ddlSVolatality.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLATALITY_ROUNDING'").ToString();
    }

   


    protected void Button4_Click(object sender, EventArgs e)
    {

        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
        if (Request.QueryString.Keys.Count > 0)
        {


            objEntity.Strategy = objStrat.getValues(Request.QueryString["Strategy"].ToString(), Request.QueryString["LS"].ToString());
            objEntity.Midstrike = Convert.ToDouble(Request.QueryString["Strike"].ToString());
            objEntity.Bdate = Request.QueryString["TrdDate"].ToString();
            objEntity.Edate = Request.QueryString["ExpDate"].ToString();
            objEntity.Symbol = Request.QueryString["symbol"].ToString();
            objEntity.Userid = Session["UserEmail"].ToString();
            objEntity.Spotprice = Request.QueryString["Spot"].ToString();
            DataTable dt_Trade = new DataTable();

            RepoSetting obj = new RepoSetting();
            EntitySetting objsetting = obj.getSettingByEmail(objEntity.Userid);
            objEntity.Qty = objsetting.R_Quantity.ToString();
            objStrat.AddMWPosition(objEntity);

            DataTable dt_Add = new DataTable();
            dt_Add = objStrat.ActionAdd(objEntity);

            dt_Trade = objStrat.ActionTrade(objEntity);

            updateSetting(Session["UserEmail"].ToString());
            HideUnhide(Session["UserEmail"].ToString());

            grvSimulator.DataSource = dt_Trade;
            grvSimulator.DataBind();
            setSummary(objStrat.getSummary(dt_Trade));

            ddSymbol.SelectedValue = objEntity.Symbol;
            txtBusinessDt.Text = objEntity.Bdate;
            ddSymbol.Enabled = false;
            txtBusinessDt.Enabled = false;
            ImageButton1.Enabled = false;
            objStrat.set_Recording(objEntity);
            lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
            ModalPopupExtender2.Hide();
            //call Your Function if you select Cancel option
        }
        else
        {
            objStrat.delete_MW_Entry_By_Email(Session["UserEmail"].ToString());
        }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {

        objEntity = new EntityStrategy();
        objStrat = new Repostrategy();
       
        objEntity.Userid = Session["UserEmail"].ToString();
        
        DataTable dt_Trade = new DataTable();
        dt_Trade = new DataTable();
        dt_Trade = objStrat.getSimPosition(objEntity);
        updateSetting(Session["UserEmail"].ToString());
        HideUnhide(Session["UserEmail"].ToString());

        grvSimulator.DataSource = dt_Trade;
        grvSimulator.DataBind();
        setSummary(objStrat.getSummary(dt_Trade));

        ddSymbol.SelectedValue = objEntity.Symbol;
        txtBusinessDt.Text = dt_Trade.Rows[0][7].ToString();
        ddSymbol.Enabled = false;
        txtBusinessDt.Enabled = false;
        ImageButton1.Enabled = false;
        lblrecStepts.Text = "[" + objStrat.get_Recording_Step(Session["UserEmail"].ToString()) + "]";
        ModalPopupExtender2.Hide();
        //call Your Function if you select OK option
    }
}



