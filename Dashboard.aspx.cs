﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Timers;

public partial class Dashboard : System.Web.UI.Page
{

    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();

    string UserId;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }
        
        getNoOfUser();
        lblNoOfHit.Text = Application["NoOfVisitors"].ToString();
        //Chart1.Series[0].ToolTip = "Y Value: #YVAL";
        if (!IsPostBack)
        {
            //Chart1.Series["Series1"].ToolTip = "Value of X:#VALX   Value of Y:#VALY";
            //Chart2.Series["Series2"].ToolTip = "Value of X:#VALX   Value of Y:#VALY";

            //Chart1.Titles.Add("NewTitle");
            //Chart1.Titles["NewTitle"].Text = "My Chart";
            //Chart1.Titles["NewTitle"].DockedToChartArea = null;
        }
    }
    public void getNoOfUser()
    {
        cnnstr.Open();
        string sql = "select count(Email) from login";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        lblNoofUser.Text = ds.Tables[0].Rows[0][0].ToString();
        cnnstr.Close();
    }
    protected void btnMarketwatch_Click(object sender, EventArgs e)
    {
        Response.Redirect("Marketwatch.aspx");
    }
    protected void btnsimulator_Click(object sender, EventArgs e)
    {
        Response.Redirect("Simulator.aspx");
    }
    protected void btnTrend_Click(object sender, EventArgs e)
    {
        Response.Redirect("Trends.aspx");
    }
    protected void btnSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect("Setting.aspx");
    }
}