﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AdminHome.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .heght
        {
            height: 120px;
        }
        .Calendar .ajax__calendar_body
        {
            width: 162px;
            height: 135px;
            background-color: #4c7393;
            color: white;
            z-index: 10;
        }
        .Calendar .ajax__calendar_header
        {
            background-color: gray;
            color: white;
        }
        .desing
        {
            /*background-color :Teal;    */
        }
    </style>
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    
    <h2 align=center> User Details </h2>
    <br>

   <%-- AllowPaging="false"
    OnPageIndexChanging="grvregister_PageIndexChanging"--%>

    <center>
    <div style="width: 90%; height: 600px; overflow: scroll ">

    <asp:GridView ID="grvregister" runat="server" AutoGenerateColumns="False"
        align="center"  
        Width="100%" OnRowCancelingEdit="grvregister_RowCancelingEdit" OnRowEditing="grvregister_RowEditing"
        OnRowUpdating="grvregister_RowUpdating" CellPadding="4" ForeColor="#333333" GridLines="None">

        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <%--  <asp:TemplateField HeaderText="ID">
                <ItemTemplate>
                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>


            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:Button ID="btnEdit" CommandName="Edit" runat="server" Text="Edit" />
                </ItemTemplate>
                <EditItemTemplate>
                <asp:LinkButton ID="lbtnUpd" Text="Update" CommandName="Update" runat="server"></asp:LinkButton>
                <asp:LinkButton ID="lbtnDel" Text="Cancel" CommandName="Cancel"  runat="server"></asp:LinkButton>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                </ItemTemplate>
                <%--<EditItemTemplate>
                    <asp:TextBox ID="gtxtEmail" Width="200px" Text='<%# Eval("Email") %>' runat="server"></asp:TextBox>
                </EditItemTemplate>--%>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <asp:Label ID="lblFname" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtFname" Width="70px" Text='<%# Eval("FirstName") %>' runat="server"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Password">
                <ItemTemplate>
                    <asp:Label ID="lblPassword" runat="server" Text='<%# Eval("Password") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtPassword" Width="100px" Text='<%# Eval("Password") %>' runat="server"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile No">
                <ItemTemplate>
                    <asp:Label ID="lblmno" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtmno" Width="100px" Text='<%# Eval("MobileNo") %>' runat="server"></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="From Date">
                <ItemTemplate>
                    <asp:Label ID="lblFromDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Text='<%# Eval("FromDate","{0:dd-MMM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtFromDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Width="100px"
                        Text='<%#Eval("FromDate","{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                    <cc1:CalendarExtender ID="calenderFromDt" PopupButtonID="gtxtFromDt" runat="server"
                        TargetControlID="gtxtFromDt" Format="dd-MMM-yyyy" PopupPosition="Right" ClientIDMode="Inherit"
                        Animated="False" CssClass="Calendar">
                    </cc1:CalendarExtender>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="To Date">
                <ItemTemplate>
                    <asp:Label ID="lblToDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Text='<%# Eval("ToDate","{0:dd-MMM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtToDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Width="100px"
                        Text='<%#Eval("ToDate","{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                    <cc1:CalendarExtender ID="calenderToDt" PopupButtonID="gtxtToDt" runat="server" TargetControlID="gtxtToDt"
                        Format="dd-MMM-yyyy" PopupPosition="Right" ClientIDMode="Inherit" Animated="False"
                        CssClass="Calendar">
                    </cc1:CalendarExtender>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Expiry Date">
                <ItemTemplate>
                    <asp:Label ID="lblExpiryDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Text='<%# Eval("ExpDate","{0:dd-MMM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtExpDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Width="100px"
                        Text='<%#Eval("ExpDate","{0:dd-MMM-yyyy}") %>'></asp:TextBox>
                    <cc1:CalendarExtender ID="calenderExpDt" PopupButtonID="gtxtExpDt" runat="server"
                        TargetControlID="gtxtExpDt" Format="dd-MMM-yyyy" PopupPosition="Right" ClientIDMode="Inherit"
                        Animated="False" CssClass="Calendar">
                    </cc1:CalendarExtender>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Register Date">
                <ItemTemplate>
                    <asp:Label ID="lblRegisterDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}" Text='<%# Eval("RegisterDate","{0:dd-MMM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="gtxtRegisterDt" runat="server" DataFormatString="{0:dd-MMM-yyyy}"
                        Width="100px" Text='<%#Eval("RegisterDate","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                    <cc1:CalendarExtender ID="calenderRegisterDt" PopupButtonID="gtxtRegisterDt" runat="server"
                        TargetControlID="gtxtRegisterDt" Format="dd/MMM/yyyy" PopupPosition="Right" ClientIDMode="Inherit"
                        Animated="False" CssClass="Calendar">
                    </cc1:CalendarExtender>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" CommandArgument='<%# Eval("Email")%>' runat="server" Text="Delete"
                        OnCommand="btnDelete_Command" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
        <EditRowStyle BackColor="#7C6F57" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#E3EAEB" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F8FAFA" />
        <SortedAscendingHeaderStyle BackColor="#246B61" />
        <SortedDescendingCellStyle BackColor="#D4DFE1" />
        <SortedDescendingHeaderStyle BackColor="#15524A" />

    </asp:GridView>
    </div> 
    </center>
</asp:Content>
