﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Profile.aspx.cs" Inherits="ProfileFinal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-wrapper">
                <div class="container-fluid pt-25">
                    <center>
                        <asp:Button ID="btnprofile" class="btn  btn-info" runat="server" Text="Profile" OnClick="btnprofile_Click" />
                        <asp:Button ID="btnUpdateProfile" class="btn  btn-primary" runat="server" Text="UpdateProfile"
                            OnClick="btnUpdateProfile_Click" />
                        <asp:Button ID="btnChangePasswd" class="btn  btn-success" runat="server" Text="ChangePassword"
                            OnClick="btnChangePasswd_Click" />
                        <br>
                        <br>
                    </center>
                    <font size="3" color="red">
                        <asp:Label runat="server" ID="lblmsg" Text="" />
                    </font>

                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    
                        <asp:View ID="ViewProfile" runat="server">
                            <h3>
                                <center>
                                    Profile</center>
                                </h3>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12 col-xs-12">
                                        <div class="panel panel-default card-view pa-0">
                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body pb-0">
                                                    <div class="tab-struct custom-tab-1">
                                                        <div id="Div2" class="tab-pane fade active in" role="tabpanel">

                                                            <div class="col-md-12">
                                                                <div class="pt-20">
                                                                    <div class="streamline user-activity">
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Firts Name : </span><span>
                                                                                            <asp:Label ID="lblFname" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Last Name : </span><span>
                                                                                            <asp:Label ID="lblLname" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Email : </span><span>
                                                                                            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Mobile No : </span><span>
                                                                                            <asp:Label ID="lblMno" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Expiry Date : </span><span>
                                                                                            <asp:Label ID="lblExpDt" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Registration Date : </span>
                                                                                        <span>
                                                                                            <asp:Label ID="lblRegistration" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">Historically Data Limit :
                                                                                        </span><span>
                                                                                            <asp:Label ID="lblDataLimit" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                        <div class="sl-item">
                                                                            <a href="javascript:void(0)">
                                                                                <div class="sl-avatar avatar avatar-sm avatar-circle">
                                                                                    <img class="img-responsive img-circle" src="Images/UserLogo.png" alt="avatar" />
                                                                                </div>
                                                                                <div class="sl-content">
                                                                                    <p class="inline-block">
                                                                                        <span class="capitalize-font txt-success mr-5 weight-500">How Many Days Left : </span>
                                                                                        <span>
                                                                                            <asp:Label ID="lblDayLeft" runat="server" Text=""></asp:Label>
                                                                                        </span>
                                                                                    </p>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </asp:View>

                        <asp:View ID="ViewUpdateProfile" runat="server">
                            <h3>
                                <center>
                                    Update Profile</center>
                              
                                </h3>
                                <br>
                                    <!-- Row -->
                                    <div class="row">
                                        <div class="col-lg-12 col-xs-12">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pb-0">
                                                        <div class="tab-struct custom-tab-1">
                                                            <div id="settings_8" class="tab-pane fade active in" role="tabpanel">
                                                                <!-- Row -->
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="">
                                                                            <div class="panel-wrapper collapse in">
                                                                                <div class="panel-body pa-0">
                                                                                    <div class="col-sm-12 col-xs-12">
                                                                                        <div class="form-wrap">
                                                                                            <form action="#">
                                                                                            <div class="form-body overflow-hide">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label mb-10" for="exampleInputuname_01">
                                                                                                        First Name</label>
                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-addon">
                                                                                                            <i class="icon-user"></i>
                                                                                                        </div>
                                                                                                        <asp:TextBox ID="txtFname" runat="server" class="form-control" placeholder="First Name"
                                                                                                            Style="width: 300px;"></asp:TextBox>
                                                                                                        <%--<input type="text" class="form-control" id="exampleInputuname_01" placeholder="willard bryant">--%>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label mb-10" for="exampleInputuname_01">
                                                                                                        Last Name</label>
                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-addon">
                                                                                                            <i class="icon-user"></i>
                                                                                                        </div>
                                                                                                        <asp:TextBox ID="txtLname" runat="server" class="form-control" placeholder="Last Name"
                                                                                                            Style="width: 300px;"></asp:TextBox>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label mb-10" for="exampleInputContact_01">
                                                                                                        Mobile number</label>
                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-addon" dir="ltr">
                                                                                                            <i class="icon-phone"></i>
                                                                                                        </div>
                                                                                                        <asp:TextBox ID="txtmno" runat="server" class="form-control" placeholder="+91 7894561230"
                                                                                                            Style="width: 300px;"></asp:TextBox>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-actions mt-10">
                                                                                                <asp:Button ID="btnprofileUpdate" runat="server" class="btn btn-success mr-10 mb-30"
                                                                                                    OnClick="btnprofileUpdate_Click" Text="Update Profile" />
                                                                                            </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Row -->
                                
                        </asp:View>
                       
                        <asp:View ID="ViewChnagePasswd" runat="server">
                            <h3>
                                <center>
                                    Change Password</center>
                                </h3>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12 col-xs-12">
                                        <div class="panel panel-default card-view pa-0">
                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body pb-0">
                                                    <div class="tab-struct custom-tab-1">
                                                        <div id="Div1" class="tab-pane fade active in" role="tabpanel">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="">
                                                                        <div class="panel-wrapper collapse in">
                                                                            <div class="panel-body pa-0">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <div class="form-wrap">
                                                                                        <form action="#">
                                                                                        <div class="form-body overflow-hide">
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputpwd_01">
                                                                                                    Old Password</label>
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon">
                                                                                                        <i class="icon-lock"></i>
                                                                                                    </div>
                                                                                                    <asp:TextBox ID="txtOldPasswd" runat="server" class="form-control" placeholder="Old Password"
                                                                                                        Style="width: 300px;"></asp:TextBox>
                                                                                                    <%--<input type="password" class="form-control" id="Password1" placeholder="Enter pwd" value="password">--%>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputpwd_01">
                                                                                                    New Password</label>
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon">
                                                                                                        <i class="icon-lock"></i>
                                                                                                    </div>
                                                                                                    <asp:TextBox ID="txtNewPasswd" runat="server" class="form-control" placeholder="New Password"
                                                                                                        Style="width: 300px;"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputpwd_01">
                                                                                                    Confirm New Password</label>
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon">
                                                                                                        <i class="icon-lock"></i>
                                                                                                    </div>
                                                                                                    <asp:TextBox ID="txtConfirmPasswd" runat="server" class="form-control" placeholder="Confirm New Password"
                                                                                                        Style="width: 300px;"></asp:TextBox>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-actions mt-10">
                                                                                                <%--<button type="submit" class="btn btn-success mr-10 mb-30">Update profile</button>--%>
                                                                                                <asp:Button ID="btnpasswdChange" runat="server" class="btn btn-success mr-10 mb-30"
                                                                                                    OnClick="btnpasswdChange_Click" Text="Change Password" />
                                                                                            </div>
                                                                                        </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </asp:View>

                    </asp:MultiView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
