﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Net.Mail;

public partial class Sign_Up : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["AuthUser"] = null;
    }
    SqlConnection sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
    
    protected void btnSignup_Click(object sender, EventArgs e)
    {
        if (txtEmail.Text.Trim().Length <= 0)
        {
            return;
        }
        else if (txtPassword.Text.Trim().Length <= 0)
        {
            return;
        }
        else if (txtPassword.Text != txtCpwd.Text)
        {
           Response.Write(@"<script>alert('Password Not matched')</script>");
            return;
        }
        else
        {
            ExistUser();
            if (Session["UserCount"].ToString() != "0")
            {
                Response.Write(@"<script>alert('You are already registered')</script>");
                return;
            }
            else
            {
                try
                {
                    sqlcon.Open();
                    SqlCommand cmd = new SqlCommand("insert into Login (Firstname,Lastname,Password,Email,MobileNo,Fromdate,Todate,Expdate) values('" + txtFirstName.Text + "','" + txtLastName.Text + "','" + txtPassword.Text + "','" + txtEmail.Text + "','" + txtMobNo.Text + "','" + DateTime.Today.AddDays(-30).ToString("dd/MMM/yyyy") + "','" + DateTime.Today.ToString("dd/MMM/yyyy") + "','" + DateTime.Today.AddDays(7).ToString("dd/MMM/yyyy") + "')");
                    cmd.Connection = sqlcon;
                    cmd.ExecuteNonQuery();
                    sqlcon.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Welcome " + txtFirstName.Text + "');", true);
                    Session["AuthUser"] = txtFirstName.Text;

                    string Str = null;
                    Str = "<!DOCTYPE HTML>" + "\n";
                    Str = Str + "<html lang='en-US'>" + "\n";
                    Str = Str + "<head>" + "\n";
                    Str = Str + "<meta charset='UTF-8'>" + "\n";
                    Str = Str + "<title></title>" + "\n";
                    Str = Str + "</head>" + "\n";
                    Str = Str + "<body>" + "\n";
                    Str = Str + "<p>Dear Sir, </p>" + "\n";
                    Str = Str + "<p>Greetings from FinIdeas !!!</p>" + "\n";
                    Str = Str + "<p>We welcome you to the world of FinIdeas Softwares.'</p>" + "\n";
                    Str = Str + "<p>Thanks for showing your keen interest in FinTester Software.</p>" + "\n";
                    Str = Str + "<p>You are eligible to use 7 Days Free Demo.</p>" + "\n";
                    Str = Str + "<p>Please feel free to contact our Support Team.</p>" + "\n";
                    Str = Str + "<p>Mohsin Soni : 91+ 9377573349.</p>" + "\n";
                    Str = Str + "<p>Srinivas : 91+ 9375204812 .</p>" + "\n";
                    Str = Str + "<p>Shailesh Sing : 91+ 9723829924.</p>" + "\n";
                    Str = Str + "</body>" + "\n";
                    Str = Str + "</html>" + "\n";

                    //DemoDays

                    send_email("Software@finideas.com", "Finideas123", txtEmail.Text, "FinTester Registration Confirmation", Str);

                    RepoSetting obj = new RepoSetting();
                    obj.SaveDefaultSetting(txtEmail.Text, "MW");
                    Session["UserEmail"] = txtEmail.Text;
                    Response.Redirect("Dashboard.aspx");
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('SignUp Failed Please Try Agail....!');", true);
                }
            }
            
        }
    }
    public void send_email(string senderemail, string senderpassword, string receiveremail, string subject, string message)
    {
        //  If MessageBox.Show((Convert.ToString("This will send an email to ") & receiveremail) + " are you sure ?", "Confirm", MessageBoxButtons.YesNo) = DialogResult.Yes Then
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress(senderemail);
            mail.To.Add(receiveremail);
            mail.Subject = subject;
            mail.IsBodyHtml = true;
            mail.Body = message;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(senderemail, senderpassword);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }
        catch (Exception ex)
        {
            //MessageBox.Show(ex.ToString());
            //MessageBox.Show("Email Is Invalid..");
        }
        //  End If
    }
    public object ExistUser()
    {
        object count = 0;
        sqlcon.Open();
        SqlCommand cmd = new SqlCommand("select count(*) Cnt from Login where Email='" + txtEmail.Text + "'");
        cmd.Connection = sqlcon;
        count = cmd.ExecuteScalar();
        Session["UserCount"] = count;
        sqlcon.Close();

        if (Convert.ToInt16(count) > 0)
        {
            return 1;
        }
        else 
        {
            return 0;
        }
        
    }
}