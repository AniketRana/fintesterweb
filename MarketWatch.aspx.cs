﻿using System;
using System.Linq;
using System.Web.UI;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class MarketWatch : Page
{
    public int HU_2ndgreeks;
    public int HU_BF;
    public int HU_BF2;
    public int HU_Calender;
    public int HU_Greeks;
    public int HU_OI;
    public int HU_Other;
    public int HU_PCP;
    public int HU_Premium;
    public int HU_Ratio;
    public int HU_Straddle;
    public int HU_Vol;
    public int HU_Volspread;
    public int HU_Volume;


    //declaration of runding variable 
    public int R_2ndgreeks;

    public int R_Butterfly;
    public int R_Butterfly2;
    public int R_Calendar;
    public int R_Greeks;
    public int R_OI;
    public int R_OpenInt;
    public int R_PCP;
    public int R_Premium;
    public int R_Ratio;
    public int R_Straddle;
    public int R_Volatility;
    public int R_VolSpreads;
    public int R_Volume;
    public int R_Quantity;
    public int red;
    public int green;
    public int blue;
    public string em = "";

    int settFlag = 0;

    EntitySymbol EntityObj;
    EntityConstants objConst;

    protected void Page_Load(object sender, EventArgs e)
    {

        string sql;
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

        DataSet ds = new DataSet();
        ClsBOL objbol = new ClsBOL();
        ClsBAL objbal = new ClsBAL();

        string UserId;

        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }  

        em = Session["UserEmail"].ToString(); //Request.QueryString["email"].ToString();

        if (!IsPostBack)
        {
           // radioExp1.Checked = true;
             
            objConst = new EntityConstants(); //class for define Constants
            EntityObj = new EntitySymbol();   //
            var obj = new RepoSymbolData();
            
            dropdown.SelectedValue = objConst.Symbol;

           
            EntityObj.User = em;
            EntityObj.Symbol = objConst.Symbol;
            EntityObj.Bdate = obj.getBdate(objConst.Symbol);
            EntityObj.Edate = obj.getAllExpiry(EntityObj,0);
            // By Defualt Symbol NIFTY

            ddExpiry.DataSource = obj.getExpiry(EntityObj);
            ddExpiry.DataTextField = "Expiry Date";
            ddExpiry.DataValueField = "Expiry Date";
            ddExpiry.DataBind();
            
            DataTable dt_mw = new DataTable();
            dt_mw = obj.GetDataBySymbol(EntityObj);

            RepeaterInner.DataSource = dt_mw;
            RepeaterInner.DataBind();
            
            Calendar1.EndDate = Convert.ToDateTime(EntityObj.Bdate);
            lblExpiry.Text = obj.getAllExpiry(EntityObj,0);
            string FutPrice = obj.getFuturePrice(EntityObj);
            lblFuture.Text = FutPrice;
            txtDate.Text = EntityObj.Bdate;
            lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();

            fillMWsetting();
            fillHideUnhideSetting();

            setAtm(dt_mw, Convert.ToDouble(FutPrice));

        }
        updatesetting(em);
       
    }

    private void updatesetting(string EmailId)
    {
        RepoSetting obj = new RepoSetting();
        EntitySetting objsetting = obj.getSettingByEmail(EmailId);


        HU_2ndgreeks = objsetting.Hu_2ndgreeks;
        HU_BF = objsetting.Hu_BF;
        HU_BF2 = objsetting.Hu_BF2;
        HU_Calender = objsetting.Hu_Calender;
        HU_Greeks = objsetting.Hu_Greeks;
        HU_OI = objsetting.Hu_OI;
        HU_Other = objsetting.Hu_Other;
        HU_PCP = objsetting.Hu_PCP;
        HU_Premium = objsetting.Hu_Premium;
        HU_Ratio = objsetting.Hu_Ratio;
        HU_Straddle = objsetting.Hu_Straddle;
        HU_Vol = objsetting.Hu_Vol;
        HU_Volspread = objsetting.Hu_Volspread;
        HU_Volume = objsetting.Hu_Volume;

        R_2ndgreeks = objsetting.R_2ndgreeks;

        R_Butterfly = objsetting.R_Butterfly;
        R_Butterfly2 = objsetting.R_Butterfly2;
        R_Calendar = objsetting.R_Calendar;
        R_Greeks = objsetting.R_Greeks;
        R_OI = objsetting.R_OI;
        R_OpenInt = objsetting.R_OpenInt;
        R_PCP = objsetting.R_PCP;
        R_Premium = objsetting.R_Premium;
        R_Ratio = objsetting.R_Ratio;
        R_Straddle = objsetting.R_Straddle;
        R_Volatility = objsetting.R_Volatility;
        R_VolSpreads = objsetting.R_VolSpreads;
        R_Volume = objsetting.R_Volume;
        R_Quantity = objsetting.R_Quantity;

        RepeaterInner.DataBind();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            EntityObj = new EntitySymbol();
            var obj = new RepoSymbolData();
            objConst = new EntityConstants();
            EntityObj.User = em;
            EntityObj.Bdate = txtDate.Text;
            EntityObj.Symbol = dropdown.SelectedValue;
            int noofdayskip = Convert.ToInt32(txtSkip.Text);
            DateTime businessDate = DateTime.ParseExact(EntityObj.Bdate, "dd-MMM-yyyy", null);
            txtDate.Text = Convert.ToDateTime(businessDate).AddDays(noofdayskip * 1).ToString("dd-MMM-yyyy");
            EntityObj.Bdate = txtDate.Text;


             DataTable dt_exp = new DataTable();
            dt_exp = obj.getExp(EntityObj);


            ddExpiry.DataSource = dt_exp;
            ddExpiry.DataTextField = "Expiry Date";
            ddExpiry.DataValueField = "Expiry Date";
            ddExpiry.DataBind();

            ddExpiry.SelectedIndex = Convert.ToInt32(ViewState["ExpIndex"]);
            EntityObj.Edate = obj.getAllExpiry(EntityObj, Convert.ToInt32(ViewState["ExpIndex"]));


            if (EntityObj.Edate != "")
            {
                DataTable dt_mw = new DataTable();
                dt_mw = obj.GetDataBySymbol(EntityObj);
                RepeaterInner.DataSource = dt_mw;
                RepeaterInner.DataBind();
                txtDate.Text = EntityObj.Bdate;
                string FutPrice = obj.getFuturePrice(EntityObj);
                lblFuture.Text = FutPrice;
                lblExpiry.Text = EntityObj.Edate;
                lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
                setAtm(dt_mw, Convert.ToDouble(FutPrice));
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
        }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        try
        {
            EntityObj = new EntitySymbol();
            var obj = new RepoSymbolData();
            objConst = new EntityConstants();
            EntityObj.User = em;
            EntityObj.Bdate = txtDate.Text;
            EntityObj.Symbol = dropdown.SelectedValue;
            EntityObj.Edate = lblExpiry.Text;
            int noofdayskip = Convert.ToInt32(txtSkip.Text);
            DateTime businessDate = DateTime.ParseExact(txtDate.Text, "dd-MMM-yyyy", null);
            txtDate.Text = Convert.ToDateTime(businessDate).AddDays(-noofdayskip * 1).ToString("dd-MMM-yyyy");
            string changedate = txtDate.Text;
            EntityObj.Bdate = txtDate.Text;
            if (obj.GetDataBySymbol(EntityObj) == null)
            {
                int skipdays = 0;
                while (obj.GetDataBySymbol(EntityObj) == null)
                {
                    skipdays -= 1;
                    txtDate.Text = Convert.ToDateTime(changedate).AddDays(skipdays * 1).ToString("dd-MMM-yyyy");
                    EntityObj.Bdate = txtDate.Text;
                    if (skipdays == -10)
                    {
                        EntityObj.Bdate = "";
                        break;
                    }
                }
            }

            ddExpiry.DataSource = obj.getExpiry(EntityObj);
            ddExpiry.DataTextField = "Expiry Date";
            ddExpiry.DataValueField = "Expiry Date";
            ddExpiry.DataBind();

            ddExpiry.SelectedIndex = Convert.ToInt32(ViewState["ExpIndex"]);

            EntityObj.Edate = obj.getAllExpiry(EntityObj, Convert.ToInt32(ViewState["ExpIndex"]));
            DataTable dt_mw = new DataTable();
            dt_mw = obj.GetDataBySymbol(EntityObj);

           RepeaterInner.DataSource = dt_mw;
            RepeaterInner.DataBind();
            string FutPrice = obj.getFuturePrice(EntityObj);
            lblFuture.Text = FutPrice;
            lblExpiry.Text = EntityObj.Edate;
            lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
            setAtm(dt_mw, Convert.ToDouble(FutPrice));
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
        }

       
    }

    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        try
        {
            objConst = new EntityConstants();
            EntityObj = new EntitySymbol();
            var obj = new RepoSymbolData();
            EntityObj.User = em;
            EntityObj.Bdate = txtDate.Text;
            EntityObj.Symbol = dropdown.SelectedValue;

            ddExpiry.DataSource = obj.getExp(EntityObj);
            ddExpiry.DataTextField = "Expiry Date";
            ddExpiry.DataValueField = "Expiry Date";
            ddExpiry.DataBind();

            ddExpiry.SelectedIndex = Convert.ToInt32(ViewState["ExpIndex"]);

            EntityObj.Edate = obj.getAllExpiry(EntityObj, Convert.ToInt32(ViewState["ExpIndex"]));

            DataTable dt_mw = new DataTable();
            dt_mw = obj.GetDataBySymbol(EntityObj);
            RepeaterInner.DataSource = dt_mw;

            RepeaterInner.DataBind();
            txtDate.Text = EntityObj.Bdate;
            string FutPrice = obj.getFuturePrice(EntityObj);
            lblFuture.Text = FutPrice;
            lblExpiry.Text = EntityObj.Edate;
            lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
            setAtm(dt_mw, Convert.ToDouble(FutPrice));
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Not Available...!');", true);
            RepeaterInner.DataSource = null;
            RepeaterInner.DataBind();
        }
    }

    protected void dropdown_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            objConst = new EntityConstants();
            EntityObj = new EntitySymbol();
            var obj = new RepoSymbolData();
            EntityObj.User = Session["UserEmail"].ToString();
            EntityObj.Bdate = txtDate.Text;
            EntityObj.Symbol = dropdown.SelectedItem.Text;
            DataTable dt_mw = new DataTable();
            DataTable dt_Exp = new DataTable();
            dt_Exp = obj.getSymAllExp(EntityObj);

            ddExpiry.DataSource = dt_Exp;
            ddExpiry.DataTextField = "Expiry Date";
            ddExpiry.DataValueField = "Expiry Date";
            ddExpiry.DataBind();
            int Exp_Index = Convert.ToInt32(ViewState["ExpIndex"]);

            if (dt_Exp.Rows.Count <= Exp_Index)
            {
                Exp_Index = 2;
            }

            ddExpiry.SelectedIndex = Exp_Index;

            EntityObj.Edate = obj.getSymExpiry(EntityObj, Exp_Index);
            
            

            dt_mw = obj.GetDataBySymbol(EntityObj);
                RepeaterInner.DataSource = dt_mw;
                RepeaterInner.DataBind();
                txtDate.Text = EntityObj.Bdate;
                string FutPrice = obj.getFuturePrice(EntityObj);
                lblFuture.Text = FutPrice;
                lblExpiry.Text = EntityObj.Edate;
                lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
                setAtm(dt_mw, Convert.ToDouble(FutPrice));
            
           
        }
        catch (Exception ex)
        {
            RepeaterInner.DataSource = null;
            RepeaterInner.DataBind();
        }
    }



    ////protected void radioExp1_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (radioExp1.Checked == true)
    //    {
    //        objConst = new EntityConstants();
    //        EntityObj = new EntitySymbol();
    //        var obj = new RepoSymbolData();
    //        EntityObj.User = em;
    //        EntityObj.Symbol = dropdown.SelectedValue;
    //        EntityObj.Bdate = txtDate.Text;
    //        EntityObj.Edate = obj.get1stExpiry(EntityObj);
    //        RepeaterInner.DataSource = obj.GetDataBySymbol(EntityObj);
    //        RepeaterInner.DataBind();
    //        lblExpiry.Text = EntityObj.Edate;
    //        lblFuture.Text = obj.getFuturePrice(EntityObj);
    //        lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
    //    }
    //    else
    //    {
    //        RepeaterInner.DataSource = null;
    //        RepeaterInner.DataBind();
    //    }
    //}
    //protected void radioExp2_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (radioExp2.Checked == true)
    //    {
    //        objConst = new EntityConstants();
    //        EntityObj = new EntitySymbol();
    //        var obj = new RepoSymbolData();
    //        EntityObj.User = em;
    //        EntityObj.Symbol = dropdown.SelectedValue;
    //        EntityObj.Bdate = txtDate.Text;
    //        EntityObj.Edate = obj.get2ndExpiry(EntityObj);
    //        RepeaterInner.DataSource = obj.GetDataBySymbol(EntityObj);
    //        RepeaterInner.DataBind();
    //        lblExpiry.Text = EntityObj.Edate;
    //        lblFuture.Text = obj.getFuturePrice(EntityObj);
    //        lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();

    //    }
    //    else
    //    {
    //        RepeaterInner.DataSource = null;
    //        RepeaterInner.DataBind();
    //    }
    //}

    //protected void radioExp3_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (radioExp3.Checked == true)
    //    {
    //        objConst = new EntityConstants();
    //        EntityObj = new EntitySymbol();
    //        var obj = new RepoSymbolData();
    //        EntityObj.User = em;
    //        EntityObj.Symbol = dropdown.SelectedValue;
    //        EntityObj.Bdate = txtDate.Text;
    //        EntityObj.Edate = obj.get3rdExpiry(EntityObj);
    //        RepeaterInner.DataSource = obj.GetDataBySymbol(EntityObj);
    //        RepeaterInner.DataBind();
    //        lblExpiry.Text = EntityObj.Edate;
    //        lblFuture.Text = obj.getFuturePrice(EntityObj);
    //        lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();}


    protected void btnSettingSave_Click(object sender, EventArgs e)
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

        string Email = Session["UserEmail"].ToString();
        string Module = "MW";
        cnnstr.Open();
        SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_ROUNDING','" + ddlOI.SelectedValue + "'", cnnstr);
        OIcmd.ExecuteNonQuery();

        SqlCommand PCPcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_ROUNDING','" + ddlPCP.SelectedValue + "'", cnnstr);
        PCPcmd.ExecuteNonQuery();

        SqlCommand Greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_ROUNDING','" + ddlgreek.SelectedValue + "'", cnnstr);
        Greekcmd.ExecuteNonQuery();

        SqlCommand Premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_ROUNDING','" + ddlpremium.SelectedValue + "'", cnnstr);
        Premiumcmd.ExecuteNonQuery();

        SqlCommand Calendercmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','CALENDER_ROUNDING','" + ddlcalander.SelectedValue + "'", cnnstr);
        Calendercmd.ExecuteNonQuery();

        SqlCommand Ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_ROUNDING','" + ddlRatio.SelectedValue + "'", cnnstr);
        Ratiocmd.ExecuteNonQuery();

        SqlCommand Bfcmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','BF_ROUNDING','" + ddlbutterfly.SelectedValue + "'", cnnstr);
        Bfcmd.ExecuteNonQuery();

        SqlCommand Bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_ROUNDING','" + ddlButterfly2.SelectedValue + "'", cnnstr);
        Bf2cmd.ExecuteNonQuery();

        SqlCommand Straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_ROUNDING','" + ddlStraddle.SelectedValue + "'", cnnstr);
        Straddlecmd.ExecuteNonQuery();

        SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_ROUNDING','" + ddl2ndgreek.SelectedValue + "'", cnnstr);
        ndGreekcmd.ExecuteNonQuery();

        SqlCommand VolSpreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_ROUNDING','" + ddlvolSpread.SelectedValue + "'", cnnstr);
        VolSpreadcmd.ExecuteNonQuery();

        SqlCommand Volatalitycmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLATALITY_ROUNDING','" + ddlVolatality.SelectedValue + "'", cnnstr);
        Volatalitycmd.ExecuteNonQuery();

        SqlCommand Volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_ROUNDING','" + ddlVolume.SelectedValue + "'", cnnstr);
        Volumecmd.ExecuteNonQuery();

        SqlCommand Qtycmd = new SqlCommand("Sp_InsUpd_Setting'" + Email + "','" + Module + "','QTY','" + txtQty.Text + "'", cnnstr);
        Qtycmd.ExecuteNonQuery();

        SqlCommand UpDowncmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','UD_STRIKE','" + txtUpDownStrike.Text + "'", cnnstr);
        UpDowncmd.ExecuteNonQuery();

        cnnstr.Close();

        settFlag = 1;
        updatesetting(Email);
        getGrid();
       


    }

    protected void btnHideUnhide_Click(object sender, EventArgs e)
    {
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

        string Email = Session["UserEmail"].ToString();
        string Module = "MW";

        cnnstr.Open();

        if (chkOI.Checked == true)
        {
            SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_HU','1'", cnnstr);
            OIcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand OIcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OI_HU','0'", cnnstr);
            OIcmd.ExecuteNonQuery();
        }
        if (chkpcp.Checked == true)
        {
            SqlCommand pcpcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_HU','1'", cnnstr);
            pcpcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand pcpcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PCP_HU','0'", cnnstr);
            pcpcmd.ExecuteNonQuery();
        }
        if (chkgreek.Checked == true)
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_HU','1'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand greekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','GREEKS_HU','0'", cnnstr);
            greekcmd.ExecuteNonQuery();
        }
        if (chkPremium.Checked == true)
        {
            SqlCommand premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_HU','1'", cnnstr);
            premiumcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand premiumcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','PREMIUM_HU','0'", cnnstr);
            premiumcmd.ExecuteNonQuery();
        }
        if (chkcalander.Checked == true)
        {
            SqlCommand calandercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','CALENDER_HU','1'", cnnstr);
            calandercmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand calandercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','CALENDER_HU','0'", cnnstr);
            calandercmd.ExecuteNonQuery();
        }
        if (chkRatio.Checked == true)
        {
            SqlCommand ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_HU','1'", cnnstr);
            ratiocmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand ratiocmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','RATIO_HU','0'", cnnstr);
            ratiocmd.ExecuteNonQuery();
        }
        if (chkButterfly.Checked == true)
        {
            SqlCommand bfcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF_HU','1'", cnnstr);
            bfcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand bfcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF_HU','0'", cnnstr);
            bfcmd.ExecuteNonQuery();
        }
        if (chkStraddle.Checked == true)
        {
            SqlCommand straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_HU','1'", cnnstr);
            straddlecmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand straddlecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','STRADDLE_HU','0'", cnnstr);
            straddlecmd.ExecuteNonQuery();
        }
        if (chk2ndGreek.Checked == true)
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','1'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','0'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
        if (chk2ndGreek.Checked == true)
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','1'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand ndGreekcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','2NDGREEKS_HU','0'", cnnstr);
            ndGreekcmd.ExecuteNonQuery();
        }
        if (chkVolSpread.Checked == true)
        {
            SqlCommand volspreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_HU','1'", cnnstr);
            volspreadcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand volspreadcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLSPREAD_HU','0'", cnnstr);
            volspreadcmd.ExecuteNonQuery();
        }
        if (chkVolatality.Checked == true)
        {
            SqlCommand volcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOL_HU','1'", cnnstr);
            volcmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand volcmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOL_HU','0'", cnnstr);
            volcmd.ExecuteNonQuery();
        }
        if (chkVolume.Checked == true)
        {
            SqlCommand volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_HU','1'", cnnstr);
            volumecmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand volumecmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','VOLUME_HU','0'", cnnstr);
            volumecmd.ExecuteNonQuery();
        }
        if (chkButterfly2.Checked == true)
        {
            SqlCommand bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_HU','1'", cnnstr);
            bf2cmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand bf2cmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','BF2_HU','0'", cnnstr);
            bf2cmd.ExecuteNonQuery();
        }
        if (chkOthers.Checked == true)
        {
            SqlCommand othercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OTHER_HU','1'", cnnstr);
            othercmd.ExecuteNonQuery();
        }
        else
        {
            SqlCommand othercmd = new SqlCommand("Sp_InsUpd_Setting '" + Email + "','" + Module + "','OTHER_HU','0'", cnnstr);
            othercmd.ExecuteNonQuery();
        }
        cnnstr.Close();


        RepeaterInner.DataSource = null;
        RepeaterInner.DataBind();
        getGrid();
        updatesetting(Email);  
        

    }

    public void fillMWsetting()
    {
        string Email = Session["UserEmail"].ToString();
        string Module = "MW";
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
        string sql = "";
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + Module + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();

        ddlOI.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='OI_ROUNDING'").ToString();
        ddl2ndgreek.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='2NDGREEKS_ROUNDING'").ToString();
        ddlbutterfly.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF_ROUNDING'").ToString();
        ddlButterfly2.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF2_ROUNDING'").ToString();
        ddlcalander.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='CALENDER_ROUNDING'").ToString();
        ddlgreek.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKS_ROUNDING'").ToString();
        ddlPCP.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PCP_ROUNDING'").ToString();
        ddlpremium.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='PREMIUM_ROUNDING'").ToString();
        ddlRatio.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='RATIO_ROUNDING'").ToString();
        ddlStraddle.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='STRADDLE_ROUNDING'").ToString();
        ddlVolatality.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLATALITY_ROUNDING'").ToString();
        ddlvolSpread.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLSPREAD_ROUNDING'").ToString();
        ddlVolume.SelectedValue = ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLUME_ROUNDING'").ToString();
        txtQty.Text = ds.Tables[0].Compute("max(SettingValue)", "SettingName='Qty'").ToString();
        txtUpDownStrike.Text = ds.Tables[0].Compute("max(SettingValue)", "SettingName='UD_STRIKE'").ToString();

    }

    public void fillHideUnhideSetting()
    {
        string Email = Session["UserEmail"].ToString();
        string Module = "MW";
        SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
        string sql = "";
        sql = "select * from tblsetting where EmailID = '" + Email + "' and Module = '" + Module + "' ";
        DataSet ds = new DataSet();
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);

        cnnstr.Close();

        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='OI_HU'").ToString())
        {
            chkOI.Checked = true;
        }
        else
        {
            chkOI.Checked = false;
        }

        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='PCP_HU'").ToString())
        {
            chkpcp.Checked = true;
        }
        else
        {
            chkpcp.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='GREEKS_HU'").ToString())
        {
            chkgreek.Checked = true;
        }
        else
        {
            chkgreek.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='PREMIUM_HU'").ToString())
        {
            chkPremium.Checked = true;
        }
        else
        {
            chkPremium.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='CALENDER_HU'").ToString())
        {
            chkcalander.Checked = true;
        }
        else
        {
            chkcalander.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='RATIO_HU'").ToString())
        {
            chkRatio.Checked = true;
        }
        else
        {
            chkRatio.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF_HU'").ToString())
        {
            chkButterfly.Checked = true;
        }
        else
        {
            chkButterfly.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='BF2_HU'").ToString())
        {
            chkButterfly2.Checked = true;
        }
        else
        {
            chkButterfly2.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='STRADDLE_HU'").ToString())
        {
            chkStraddle.Checked = true;
        }
        else
        {
            chkStraddle.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='2NDGREEKS_HU'").ToString())
        {
            chk2ndGreek.Checked = true;
        }
        else
        {
            chk2ndGreek.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLSPREAD_HU'").ToString())
        {
            chkVolSpread.Checked = true;
        }
        else
        {
            chkVolSpread.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOL_HU'").ToString())
        {
            chkVolatality.Checked = true;
        }
        else
        {
            chkVolatality.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='VOLUME_HU'").ToString())
        {
            chkVolume.Checked = true;
        }
        else
        {
            chkVolume.Checked = false;
        }
        if ("1" == ds.Tables[0].Compute("max(SettingValue)", "SettingName='OTHERS_HU'").ToString())
        {
            chkOthers.Checked = true;
        }
        else
        {
            chkOthers.Checked = false;
        }

    }

    private void getGrid()
    {
        objConst = new EntityConstants();
        EntityObj = new EntitySymbol();
        var obj = new RepoSymbolData();
        EntityObj.User = em;
        EntityObj.Symbol = dropdown.SelectedValue;
        EntityObj.Bdate = txtDate.Text;
                

        ddExpiry.DataSource = obj.getExpiry(EntityObj);
        ddExpiry.DataTextField = "Expiry Date";
        ddExpiry.DataValueField = "Expiry Date";
        ddExpiry.DataBind();

        ddExpiry.SelectedIndex = Convert.ToInt32(ViewState["ExpIndex"]);

        //if (ddExpiry.SelectedIndex == 0)
        //{
        //    EntityObj.Edate = obj.getSym1stExpiry(EntityObj);
        //}
        //if (ddExpiry.SelectedIndex == 1)
        //{
        //    EntityObj.Edate = obj.getSym2ndExpiry(EntityObj);
        //}
        //if (ddExpiry.SelectedIndex == 2)
        //{
        //    EntityObj.Edate = obj.getSym3rdExpiry(EntityObj);
        //}

        EntityObj.Edate = obj.getAllExpiry(EntityObj, Convert.ToInt32(ViewState["ExpIndex"]));
        DataTable dt_mw = new DataTable();
        dt_mw = obj.GetDataBySymbol(EntityObj);
        RepeaterInner.DataSource = dt_mw;
        RepeaterInner.DataBind();
        lblExpiry.Text = EntityObj.Edate;
        string FutPrice = obj.getFuturePrice(EntityObj);
        lblFuture.Text = FutPrice;
        lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
        setAtm(dt_mw, Convert.ToDouble(FutPrice));
    }

    protected void ddExpiry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["ExpIndex"] = ddExpiry.SelectedIndex.ToString();
        objConst = new EntityConstants();
        EntityObj = new EntitySymbol();
        var obj = new RepoSymbolData();
        EntityObj.User = em;
        EntityObj.Symbol = dropdown.SelectedValue;
        EntityObj.Bdate = txtDate.Text;
        EntityObj.Edate = Convert.ToDateTime(DateTime.ParseExact(ddExpiry.SelectedValue, "dd MMM yyyy", null)).ToString("dd-MMM-yyyy");
        DataTable dt_mw = new DataTable();
        dt_mw = obj.GetDataBySymbol(EntityObj);
        RepeaterInner.DataSource = dt_mw;
        RepeaterInner.DataBind();
        lblExpiry.Text = EntityObj.Edate;
        string FutPrice = obj.getFuturePrice(EntityObj);
        lblFuture.Text = FutPrice;
        lblchange.Text = obj.getChangeFuturePrice(EntityObj).ToString();
        setAtm(dt_mw, Convert.ToDouble(FutPrice));

   
    }


    private void setAtm(DataTable dt, double spot)
    {
        RepoSymbolData obj = new RepoSymbolData();

        double atm = obj.getAtm(dt, spot);
        lblAtm.Text = string.Format("{0:n0}", atm) + ".00";
    }

}