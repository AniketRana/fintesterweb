﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Home : System.Web.UI.Page
{
    string UserId = "";
    string sql;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["IDName"] == null)
        {
            Response.Redirect("AdminLogin.aspx");
        }
        
        UserId = Session["IDName"].ToString();
  
        if (!IsPostBack)
        {
            Fillgrid();
        }
    }
    public void Fillgrid()
    {
        cnnstr.Open();
        sql = "select * from Login";
        SqlDataAdapter adp = new SqlDataAdapter(sql, cnnstr);
        adp.Fill(ds);
        grvregister.DataSource = ds;
        grvregister.DataBind();
        cnnstr.Close();
    }

    //protected void grvregister_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    grvregister.PageIndex = e.NewPageIndex;
    //    Fillgrid();
    //}

    protected void grvregister_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grvregister.EditIndex = e.NewEditIndex;
        Fillgrid();
    }
    protected void grvregister_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grvregister.EditIndex = -1;
        Fillgrid();
    }
    protected void grvregister_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //Label lblEmail = (Label)grvregister.Rows[e.RowIndex].FindControl("lblEmail");
        Label lblEmail= (Label)grvregister.Rows[e.RowIndex].FindControl("lblEmail");
        TextBox gtxtFname = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtFname");
        TextBox gtxtPassword = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtPassword");
        TextBox gtxtmno = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtmno");
        TextBox gtxtFromDt = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtFromDt");
        TextBox gtxtToDt = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtToDt");
        TextBox gtxtExpDt = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtExpDt");
        TextBox gtxtRegisterDt = (TextBox)grvregister.Rows[e.RowIndex].FindControl("gtxtRegisterDt");
        
        cnnstr.Open();
        sql = "Update Login set FirstName = '"+gtxtFname .Text +"',Password = '"+ gtxtPassword.Text +"',MobileNo = '"+ gtxtmno.Text +"',FromDate = '"+gtxtFromDt .Text+"',ToDate = '"+gtxtToDt.Text +"',ExpDate = '"+gtxtExpDt.Text +"',RegisterDate = '"+gtxtRegisterDt.Text +"' where Email = '"+lblEmail.Text+"'"; 
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        grvregister .EditIndex = -1;
        Fillgrid();
    }
    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        string Email;
        Button btndel = (Button)sender;
        Email = btndel.CommandArgument;

        cnnstr.Open();
        sql = "Delete From Login Where Email = '" + Email + "'";
        SqlCommand cmd = new SqlCommand(sql, cnnstr);
        cmd.ExecuteNonQuery();
        cnnstr.Close();
        Fillgrid();
       
    }
}