﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddFaqAdmin.aspx.cs" Inherits="AddFaqAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
<style>
.button {
  padding: 10px 15px;
  font-size: 15px;
  text-align: center;
  cursor: pointer;
  outline: none;
  color: #fff;
  background-color: #4CAF50;
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <div>
        <div width="70%" style="height: 0px;">
            <h2 align="center" style="margin-top: 0px; margin-bottom: 0px; padding-top: 10px;">
                Faq Details
            </h2>
        </div>
        <div>
            <asp:ImageButton ID="AddFaqimgbtn" Height="70px" Width="100" ImageUrl="~/Images/add.png"
                runat="server" OnClick="AddFaqimgbtn_Click" /></div>
    </div>
    <asp:Panel ID="pnlForm" runat="server" Visible="false">
        <table border="0" align="center">
            <tr>
                <td><b> Question :</b>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtQuestion" TextMode="MultiLine"  Width="400px" required runat="server" Rows="3"></asp:TextBox>                   
                </td>
            </tr>
           
            <tr>
                <td><b>Answer :</b> 
                </td >
                <td><asp:TextBox ID="txtAnswer" style="margin-top: 20px;" TextMode="MultiLine" Width="400px" required runat="server" Rows="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <asp:Button ID="btnSubmit" class="button" runat="server" Text="Submit" 
                        onclick="btnSubmit_Click" />
                </td>
            </tr>

        </table>
    </asp:Panel>
    <asp:Panel ID="pnlgrid" runat="server" Visible="true">
        <center>
            <div style="width: 90%; height: 600px; overflow: scroll">
                <asp:GridView ID="grvfaq" runat="server" AutoGenerateColumns="False" align="center"
                    Width="100%" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="grvfaq_RowCancelingEdit"
                    OnRowEditing="grvfaq_RowEditing" OnRowUpdating="grvfaq_RowUpdating">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <center>
                                    <asp:Button ID="btnEdit" CommandName="Edit" runat="server" Text="Edit" /></center>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
                                    <asp:LinkButton ID="lbtnUpd" Text="Update" CommandName="Update" runat="server"></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnDel" Text="Cancel" CommandName="Cancel" runat="server"></asp:LinkButton></center>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID">
                            <ItemTemplate>
                                <center>
                                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("id") %>'></asp:Label></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question">
                            <ItemTemplate>
                                <center>
                                    <asp:Label ID="lblQuestion" Width="300px" runat="server" Text='<%# Eval("Question") %>'></asp:Label></center>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
                                    <asp:TextBox ID="gtxtQuestion" Width="300px" TextMode="MultiLine" Text='<%# Eval("Question") %>'
                                        runat="server" Rows="4"></asp:TextBox></center>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Answer">
                            <ItemTemplate>
                                <center>
                                    <asp:Label ID="lblAnswer" Width="650px" runat="server" Text='<%# Eval("Answer") %>'></asp:Label></center>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
                                    <asp:TextBox ID="gtxtAnswer" Width="650px" TextMode="MultiLine" Text='<%# Eval("Answer") %>'
                                        runat="server" Rows="4"></asp:TextBox></center>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <center>
                                    <asp:Button ID="btnDelete" CommandArgument='<%# Eval("id")%>' runat="server" Text="Delete"
                                        OnCommand="btnDelete_Command" />
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
            </div>
        </center>
    </asp:Panel>
</asp:Content>
