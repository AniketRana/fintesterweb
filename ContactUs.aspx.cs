﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ContactUs : System.Web.UI.Page
{
    string sql;
    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();
    string UserId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();

        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        } 
     
    }
    
    protected void btnKnowmore_Click1(object sender, EventArgs e)
    {
        Response.Redirect("https://docs.google.com/forms/d/e/1FAIpQLSdXTY_GnSVpEd530crLJWpgb7qU5q1zw2fp1yeMut8pFxmFkQ/viewform?c=0&w=1&includes_info_params=true");
    }
}