﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class MasterPage : System.Web.UI.MasterPage
{
    string Email;
    string sql;

    SqlConnection cnnstr = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Finideas"].ConnectionString);

    DataSet ds = new DataSet();
    ClsBOL objbol = new ClsBOL();
    ClsBAL objbal = new ClsBAL();

    string UserId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["AuthUser"] == null)
        {
             Response.Redirect("Login.aspx");
        }
        lblUser.Text = " Welcome " + Session["AuthUser"].ToString();
        Email = Session["UserEmail"].ToString();
       
        if (Session["AuthUser"] == null || Session["Key"] == null || Session["UserEmail"] == null)
        {
            Response.Redirect("Login.aspx");
        }
       
        UserId = Session["UserEmail"].ToString();
        ds = objbal.ChkKey(UserId);
        objbol.CheckKey = ds.Tables[0].Rows[0]["KeyID"].ToString().Trim();
        objbol.GenRateKey = Session["Key"].ToString();
       
        if (objbol.GenRateKey != objbol.CheckKey)
        {
            Response.Redirect("Login.aspx");
        }  
    }  
    protected void btnLogout_Click(object sender, EventArgs e)
    {
        //sql = "Update Login set Active = 0 where Email = '" + Email + "'";
        //cnnstr.Open();
        //SqlCommand cmd = new SqlCommand(sql, cnnstr);
        //cmd.ExecuteNonQuery();
        //cnnstr.Close();

        Session.RemoveAll();
        Response.Redirect("Login.aspx");
    }
}
