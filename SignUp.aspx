﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="Sign_Up" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   	<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>FinTester</title>
		<meta name="description" content="Hound is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Hound Admin, Houndadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Custom CSS -->
		<link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form runat="server">
    	<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="Login.aspx">
						<img class="brand-img mr-10" src="Images/F-1.jpg" alt="brand"/>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Already have an account?</span>
					<a class="inline-block btn btn-info btn-rounded btn-outline" href="Login.aspx">Sign In</a>
				</div>
				<div class="clearfix"></div>
			</header>
			
			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign up to FinIdeas</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>	
										<div class="form-wrap">
											<form action="#">
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">Your First Name *</label>
                                                    <asp:TextBox class="form-control"  ID="txtFirstName" required=""  
                                                        placeholder="Enter First Name" runat="server" 
                                                         ></asp:TextBox>
                                                         <%--<asp:RegularExpressionValidator runat="server" ControlToValidate="txtFirstName" ErrorMessage="Can't Add Numeric value" ValidationExpression="[a-zA-Z]" >
                                                         </asp:RegularExpressionValidator>--%>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputName_3">Your Last Name *</label>
													<asp:TextBox class="form-control" ID="txtLastName" required="" 
                                                        placeholder="Enter Last Name" runat="server"></asp:TextBox>
												<%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLastName" ErrorMessage="Can't Add Numeric value" ValidationExpression="[a-zA-Z]" >
                                                         </asp:RegularExpressionValidator>--%>
                                                </div>
                                                <div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password *</label>
													<asp:TextBox class="form-control"  ID="txtPassword" TextMode="Password" required="" placeholder="Enter Password" runat="server"></asp:TextBox>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_3">Confirm Password *</label>
													<asp:TextBox class="form-control"  ID="txtCpwd" TextMode="Password" required="" placeholder="Enter Confirm Password" runat="server"></asp:TextBox>
												</div>
												
                                                <div class="form-group">
													<label class="control-label mb-10" for="exampleInputEmail_2">Email address *</label>
													<asp:TextBox class="form-control"  ID="txtEmail" placeholder="Enter email" runat="server"></asp:TextBox>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputName_4">Mobile Number *</label>
                                                    <asp:TextBox  class="form-control" ID="txtMobNo" placeholder="Enter Mobile No." runat="server" CausesValidation="True"></asp:TextBox>
                                                </div>
                                                <%--<div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
                                                        <asp:CheckBox ID="chkbox1" runat="server" />
														<label for="checkbox_2"> I agree to all <span class="txt-primary">Terms</span></label>
													</div>
													<div class="clearfix"></div>
												</div>--%>
												<div class="form-group text-center">
                                                <asp:Button class="btn btn-info btn-rounded" ID="btnSignup" runat="server" 
                                                        Text="Sign Up" onclick="btnSignup_Click" href="Defualt.aspx"/>
												</div>
											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="dist/js/init.js"></script>
        <script>

            // Back Button Disable
            window.history.forward();
            function noBack() {
                window.history.forward();
            }
        
        </Script>
    </form>
</body>
</html>
